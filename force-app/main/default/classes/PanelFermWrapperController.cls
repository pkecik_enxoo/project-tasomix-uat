/*
* @author Jarosław Kołodziejczyk 2017-04-04
* 1. Kontroler obsługujacy działanie komponentów PanelFerm oraz PanelFermWrapper
* 2. Metody wywoływane z poziomu kontrolera javascriptowego komponentu
* 3. Metoda getFarms wykorzystywana jest zarówno w jsowym kontrolerze jak i helperze, oraz przy wywołaniu funkji doInit
* 4. Metoda getAccount (zdefiniowana w klasie ClientUtils) pobiera inicjalną listę dostępnych kont dla danego usera
* 5. Metoda selectFarm generuję listę powiązanych ze sobą obiektów (ferma,kurnik,flock,flock staty) w formie listy instancji customowej klasy (w ten sposób jestem w stanie iterować po liście powiązanych obiektów w aurze)
*/


public class PanelFermWrapperController {
    
    @AuraEnabled
    public static Farm__c[] getFarms(string accId){
        // Lista pobierająca fermy dla konta które jest określone jako farm owner account lub master record kontaktu "Manager"
        return ClientUtils.getFarms(accId);

    }
    
    @AuraEnabled
    public static Account[] getAccount()
    {
        return ClientUtils.getAccount();
    }
    
    @AuraEnabled
    public static PanelFermDataClass[] selectFarm(Id farmId)
    {
        // sprawdzam czy przekazany został poprawny id fermy
        system.debug('PanelFermWrapperController.selectFarm : farmId:'+farmId);    
        
        List<PanelFermDataClass> houseFlock = new List<PanelFermDataClass>();
        
        List<House__c> houseList = ClientUtils.getHouses(farmId); //new List<House__c>([select id,name,House_Name__c from House__c where Farm__c =: farmId order by  House_Name__c asc]);//,SortOrderNumber__c,
        
        // sprawdzam czy pobrane zostały kurniki
        system.debug('PanelFermWrapperController.selectFarm : houseList:'+houseList);
        
        // Lista flocków i ich flock statów które sortowane są po Date__c, jest to potrzebne do wybierania najnowszych danych do obiektu z ich parametrami
        transient List<Flock__c> flockList = new List<Flock__c>([select id,name,House__c,Flock_number__c,Total_dead__c,Total_dead_percent__c,Day__c,Feed_Left_in_Silo__c,
                                                                 (select id,name,Daily_dead__c,Daily_selection__c,Avg_body_weight__c,Daily_Dead_Total__c,Daily_Dead_Percent__c,
                                                                  Daily_Feed_Consumed__c,Daily_water_consumption__c,Day__c,Date__c,Weight_Standard_Chick__c,
                                                                  Water_consumption_Standard_Chick__c,Weight_Standard_table__c,Water_consumption_Standard_Table__c,
                                                                  Feed_consumption_Standard_Table__c,Feed_consumption_Standard_Chick__c,Water_Feed__c,Density__c,
                                                                  Avg_feed_consumed__c, Feed_consumption_in_Chick_stndard__c, Avg_water_consumption__c, Water_consumption_in_Chick_Standard__c, 
                                                                  
                                                                  Standard_Line_Id__r.Avg_body_weight__c, Standard_Line_Id__r.Chick_weight_factor__c,Flock__r.Chick_weight_at_arrival__c, 
                                                                  Standard_Line_Id__r.Daily_feed_consumed__c, Standard_Line_Id__r.Daily_water_consumption__c
                                                                  from Flock_stats__r order by Date__c desc) 
                                                                 from Flock__c where House__c in: houseList and Status__c =: 'Active']);
        
        // sprawdzam czy pobrane zostały flocki
        system.debug('PanelFermWrapperController.selectFarm : flockList:'+flockList);
        
        // Dla każdego kurnika i flocka Generuję nową instancję klasy wrapującej te rekordy oraz parametry flock statów
        
        for(House__c h : houseList)
        {
            if(flockList.size()>0)
            {
                for(Flock__c f : flockList)
                {
                    if(f.House__c == h.id)
                    {
                        // wrapper potrzebny do przekazania obu obiektów i ich statów do komponentu
                        PanelFermDataClass pData = new PanelFermDataClass();
                        pData.house = h;
                        pData.flock = f;
                        
                        if(f.Flock_Stats__r.size()>0)
                        {
                            decimal oldTableWeightValue = null;
                            decimal oldChickWeightValue = null;
                            //Obiekt przetrzymujący parametry flock statów
                            PanelFermFlockStatsParams fsParam = new PanelFermFlockStatsParams();
                            
                            fsParam.dead.setTotals(f.Total_dead__c, f.Total_dead_percent__c);
                            for(Flock_Stats__c fls : f.Flock_Stats__r)
                            {
                            	//Uzupełnij paramaetry wagi
                            	fsParam.weight.setAvgWeight(fls.Avg_body_weight__c,fls.Density__c ,fls.Date__c,fls.Day__c);
                            	
                            	//Uzupełnij Normę wagi, Spożycie wody i spożycie paszy
                                fsParam.weightStandard.setChickStandard(fls.Weight_Standard_Chick__c,fls.Date__c,fls.Day__c);
                                fsParam.weightStandard.setTableStandard(fls.Weight_Standard_table__c,fls.Standard_Line_Id__r.Avg_body_weight__c,fls.Avg_body_weight__c ,'g',fls.Date__c,fls.Day__c);
                                
                                fsParam.waterStandard.setChickStandard(fls.Water_consumption_Standard_Chick__c,fls.Date__c,fls.Day__c);
                                fsParam.waterStandard.setTableStandard(fls.Water_consumption_Standard_table__c,fls.Standard_Line_Id__r.Daily_water_consumption__c,fls.Avg_water_consumption__c ,'ml',fls.Date__c,fls.Day__c);
                                
                                fsParam.feedConsumptionStandard.setChickStandard(fls.Feed_consumption_Standard_Chick__c,fls.Date__c,fls.Day__c);
                                fsParam.feedConsumptionStandard.setTableStandard(fls.Feed_consumption_Standard_Table__c,fls.Standard_Line_Id__r.Daily_feed_consumed__c ,fls.Avg_feed_consumed__c ,'g',fls.Date__c,fls.Day__c);
                                
                                // uzupełnij upadeki
                                fsParam.dead.setDailyStat(fls.Daily_dead_total__c,fls.Date__c,fls.Day__c, fls.Daily_dead__c, fls.Daily_selection__c );
                                
                                fsParam.waterFeed.setDailyStat(fls.Water_Feed__c,fls.Date__c,fls.Day__c);
                                //System.debug(fls.Day__c+' water feed'+fls.Water_Feed__c + ' flock '+f.name);
                                // Sprawdzam czy dany parametr dla danej instancji obiektu jest pusty, jeśli jest pusty przypisuję do niego wartość najnowszego Flock Statu
                                // Po przypisaniu parametru, określam ilość dni od ostatniej aktualizacji oraz dzień flocku dla danego parametru
                                // Dla Normy wagi określam również kolor(parametr w obiekcie) w jakim ma się pojawić na stronie
                                
                                if(fsParam.fsDate == null)
                                {
                                    fsParam.fsDate = fls.Date__c;
                                    // Sprawdzam czy data została poprawnie przypisana
                                    system.debug('PanelFermWrapperController.selectFarm: '+fsParam.fsDate);
                                }
                                //stary kod do wyswietlania wagi
                                /*
                                if(fsParam.avgBodyWeight == null)
                                {
                                    if(fls.Avg_body_weight__c != null)
                                    {
                                        system.debug('PanelFermWrapperController.selectFarm: '+fls.Avg_body_weight__c);
                                        fsParam.avgBodyWeight = fls.Avg_body_weight__c;
                                        Date today = date.today();
                                        integer dateV = fls.Date__c.daysBetween(today);
                                        if(dateV == 0)
                                        {
                                            fsParam.avgBodyWeightLastUpdatedDaysNumber = '0';
                                        }
                                        else if(dateV != null)
                                        {
                                            fsParam.avgBodyWeightLastUpdatedDaysNumber = string.valueOf(dateV);
                                        }
                                        
                                        fsParam.avgBodyWeightDay = integer.valueOf(fls.Day__c);



                                    if(fsParam.density == null)
                                    {
                                        if(fls.Density__c != null)
                                        {
                                            system.debug('PanelFermWrapperController.selectFarm: '+fls.Density__c);
                                            fsParam.density = fls.Density__c;

                                        }
                                    }

                                    }
                                }
                                */
                                //stary kod do wyswietlania spożycia wody
                                /*
                                if(fsParam.WaterConsumptionStandardChick == null)
                                {
                                    if(fls.Daily_water_consumption__c != null && fls.Avg_body_weight__c != null)
                                    {
                                        system.debug('PanelFermWrapperController.selectFarm: '+fls.Water_consumption_Standard_Chick__c);
                                        
                                        fsParam.WaterConsumptionStandardChick = fls.Water_consumption_Standard_Chick__c;
                                        fsParam.WaterConsumptionStandardTable = fls.Water_consumption_Standard_Table__c;
                                        Date today = date.today();
                                        integer dateV = fls.Date__c.daysBetween(today);
                                        
                                        if(dateV == 0)
                                        {
                                            fsParam.WaterConsumptionStandardChickLastUpdatedDaysNumber = '0';
                                        }
                                        else if(dateV != null)
                                        {
                                            fsParam.WaterConsumptionStandardChickLastUpdatedDaysNumber = string.valueOf(dateV);
                                        }
                                        
                                        fsParam.WaterConsumptionStandardChickDay = integer.valueOf(fls.Day__c);
                                    }
                                }
                                */
                                /*
                                if(fsParam.WaterConsumptionStandardTable == null)
                                {
                                    if(fls.Daily_water_consumption__c != null)
                                    {
                                        system.debug('PanelFermWrapperController.selectFarm: '+fls.Water_consumption_Standard_Table__c);
                                        
                                        fsParam.WaterConsumptionStandardTable = fls.Water_consumption_Standard_Table__c;
                                    }
                                }*/
                                //stary kod do wyswietlania spożycia paszy
                                /*
                                if(fsParam.FeedConsumptionStandardChick == null)
                                {
                                    if(fls.Daily_Feed_Consumed__c != null && fls.Avg_body_weight__c != null)
                                    {
                                        system.debug('PanelFermWrapperController.selectFarm: '+fls.Feed_consumption_Standard_Chick__c);
                                        
                                        fsParam.FeedConsumptionStandardChick = fls.Feed_consumption_Standard_Chick__c;
                                        fsParam.FeedConsumptionStandardTable = fls.Feed_consumption_Standard_Table__c;
                                        Date today = date.today();
                                        integer dateV = fls.Date__c.daysBetween(today);
                                        
                                        if(dateV == 0)
                                        {
                                            fsParam.FeedConsumptionStandardChickLastUpdatedDaysNumber = '0';
                                        }
                                        else if(dateV != null)
                                        {
                                            fsParam.FeedConsumptionStandardChickLastUpdatedDaysNumber = string.valueOf(dateV);
                                        }
                                        
                                        fsParam.FeedConsumptionStandardChickDay = integer.valueOf(fls.Day__c);
                                    }
                                }
                                */
                                /*
                                if(fsParam.FeedConsumptionStandardTable == null)
                                {
                                    if(fls.Daily_Feed_Consumed__c != null)
                                    {
                                        system.debug('PanelFermWrapperController.selectFarm: '+fls.Feed_consumption_Standard_Table__c);
                                        
                                        fsParam.FeedConsumptionStandardTable = fls.Feed_consumption_Standard_Table__c;
                                    }
                                    
                                }*/
                                // stary kod woda/pasza
                                /*
                                if(fsParam.WaterFeed == null)
                                {
                                    if(fls.Daily_water_consumption__c != null && fls.Daily_Feed_Consumed__c != null)
                                    {
                                        system.debug('PanelFermWrapperController.selectFarm: '+fls.Water_Feed__c);
                                        
                                        fsParam.WaterFeed = fls.Water_Feed__c;
                                        Date today = date.today();
                                        integer dateV = fls.Date__c.daysBetween(today);
                                        
                                        if(dateV == 0)
                                        {
                                            fsParam.WaterFeedLastUpdatedDaysNumber = '0';
                                        }
                                        else if(dateV != null)
                                        {
                                            fsParam.WaterFeedLastUpdatedDaysNumber = string.valueOf(dateV);
                                        }
                                        
                                        fsParam.WaterFeedDay = integer.valueOf(fls.Day__c);
                                    }
                                }
                                */
                                // stary kod od oblicznia zmian w normie wagi
                                /*
                                if(fsParam.WeightStandardTable != null)
                                {
                                    if(fls.Avg_body_weight__c != null && oldTableWeightValue == null)
                                    {
                                        oldTableWeightValue = fsParam.WeightStandardTable;
                                        //system.debug('oldTableWeightValue: '+oldTableWeightValue);
                                    }
                                    if(oldTableWeightValue != null && fsParam.weightTableArrow == null)
                                    {
                                        fsParam.weightTableArrow = oldTableWeightValue - fls.Weight_Standard_table__c;
                                        //system.debug('fls.Weight_Standard_table__c: '+fls.Weight_Standard_table__c);
                                        //system.debug('fsParam.weightTableArrow: '+fsParam.weightTableArrow);
                                    }
                                }

                                if(fsParam.WeightStandardChick != null)
                                {
                                    if(fls.Avg_body_weight__c != null && oldChickWeightValue == null)
                                    {
                                        oldChickWeightValue = fsParam.WeightStandardChick;
                                        //system.debug('fls.Weight_Standard_Chick__c: '+fls.Weight_Standard_Chick__c);
                                        //system.debug('oldChickWeightValue: '+oldChickWeightValue);
                                    }
                                    if(oldChickWeightValue != null && fsParam.weightChickArrow == null)
                                    {
                                        fsParam.weightChickArrow = oldChickWeightValue - fls.Weight_Standard_Chick__c;
                                        system.debug('fsParam.weightChickArrow: '+fsParam.weightChickArrow);
                                    }
                                }


                                
                                if(fsParam.WeightStandardChick == null)
                                {
                                    if(fls.Avg_body_weight__c != null)
                                    {
                                        system.debug('PanelFermWrapperController.selectFarm: '+fls.Weight_Standard_Chick__c);
                                        
                                        
                                        fsParam.WeightStandardChick = fls.Weight_Standard_Chick__c;
                                        Date today = date.today();
                                        integer dateV = fls.Date__c.daysBetween(today);
                                        
                                        if(dateV == 0)
                                        {
                                            fsParam.WeightStandardChickLastUpdatedDaysNumber = '0';
                                        }
                                        else if(dateV != null)
                                        {
                                            fsParam.WeightStandardChickLastUpdatedDaysNumber = string.valueOf(dateV);
                                        }
                                        
                                        fsParam.WeightStandardChickDay = integer.valueOf(fls.Day__c);
                                        
                                        if(fls.Weight_Standard_Chick__c < 95 && fls.Weight_Standard_Chick__c != null)
                                        {
                                            fsParam.WeightStandardChickColor = 'red';
                                        }
                                        else if(fls.Weight_Standard_Chick__c >= 95 && fls.Weight_Standard_Chick__c < 100)
                                        {
                                            fsParam.WeightStandardChickColor = 'orange';
                                        }
                                        else if(fls.Weight_Standard_Chick__c >= 100)
                                        {
                                            fsParam.WeightStandardChickColor = 'green';
                                        }
                                        else
                                        {
                                            fsParam.WeightStandardChickColor = 'black';
                                        }

                                        fsParam.WeightStandardTable = fls.Weight_Standard_table__c;
                                        
                                        if(fls.Weight_Standard_table__c < 95 && fls.Weight_Standard_table__c != null)
                                        {
                                            fsParam.WeightStandardTableColor = 'red';
                                        }
                                        else if(fls.Weight_Standard_table__c >= 95 && fls.Weight_Standard_table__c < 100)
                                        {
                                            fsParam.WeightStandardTableColor = 'orange';
                                        }
                                        else if(fls.Weight_Standard_table__c >= 100)
                                        {
                                            fsParam.WeightStandardTableColor = 'green';
                                        }
                                        else
                                        {
                                            fsParam.WeightStandardTableColor = 'black';
                                        }

     

                                    }
                                }
                                */
                                /*
                                if(fsParam.WeightStandardTable == null)
                                {
                                    if(fls.Avg_body_weight__c != null)
                                    {
                                        system.debug('PanelFermWrapperController.selectFarm Weight IMPORTANT: '+fls.Weight_Standard_table__c);
                                        
                                        
                                        fsParam.WeightStandardTable = fls.Weight_Standard_table__c;
                                        
                                        if(fls.Weight_Standard_table__c < 95 && fls.Weight_Standard_table__c != null)
                                        {
                                            fsParam.WeightStandardTableColor = 'red';
                                        }
                                        else if(fls.Weight_Standard_table__c >= 95 && fls.Weight_Standard_table__c < 100)
                                        {
                                            fsParam.WeightStandardTableColor = 'orange';
                                        }
                                        else if(fls.Weight_Standard_table__c >= 100)
                                        {
                                            fsParam.WeightStandardTableColor = 'green';
                                        }
                                        else
                                        {
                                            fsParam.WeightStandardTableColor = 'black';
                                        }
                                        
                                    }
                                }*/
                                
                                // stary kod od upadków
                                /*
                                if(fsParam.DailyDeadTotal == null)
                                {
                                    if(fls.Daily_dead__c != null || fls.Daily_selection__c != null)
                                    {
                                        system.debug('PanelFermWrapperController.selectFarm: '+fls.Daily_Dead_Total__c);
                                        
                                        fsParam.DailyDeadTotal = f.Total_dead__c;
                                        Date today = date.today();
                                        integer dateV = fls.Date__c.daysBetween(today);
                                        
                                        if(dateV == 0)
                                        {
                                            fsParam.DailyDeadTotalLastUpdatedDaysNumber = '0';
                                        }
                                        else if(dateV != null)
                                        {
                                            fsParam.DailyDeadTotalLastUpdatedDaysNumber = string.valueOf(dateV);
                                        }
                                        
                                        fsParam.DailyDeadTotalDay = integer.valueOf(fls.Day__c);

                                        fsParam.DailyDeadPercent = f.Total_dead_percent__c;
                                        
                                        if(fls.Daily_Dead_Percent__c > 5)
                                        {
                                            fsParam.DailyDeadPercentColor = 'red';
                                        }

                                    }
                                }
                                
                                if(fsParam.DailyDeadPercent == null)
                                {
                                    if(fls.Daily_dead__c != null || fls.Daily_selection__c != null)
                                    {
                                        system.debug('PanelFermWrapperController.selectFarm: '+fls.Daily_Dead_Percent__c);
                                        
                                        fsParam.DailyDeadPercent = f.Total_dead_percent__c;
                                        
                                        if(fls.Daily_Dead_Percent__c > 5)
                                        {
                                            fsParam.DailyDeadPercentColor = 'red';
                                        }
                                    }
                                }
                                */
                                
                            }
                            // Przypisuję wygenerowane parametry do instancji obiektu oraz dodaję do listy tych obiektów
                            pData.flockStats = fsParam;
                            houseFlock.add(pData);
                        }
                        else
                        {
                            //Jeśli related lista z flock statami jest pusta dodaję do listy obiektów, instancję bez parametrów (ale z fermą i kurnikiem)
                            houseFlock.add(pData);
                        }
                    }
                }
            }
            else
            {
                // Jeśli nie istnieją żadne flocki dla danego kurnika dodaję do listy obiektów, instancję posiadającą jedynie kurnik
                PanelFermDataClass pData = new PanelFermDataClass();
                pData.house = h;
                houseFlock.add(pData);
            }
        }

        // Przekazuję do kontrolera javascryptowego wygenerowaną listę obiektów
        return houseFlock;
    }
}