public class NaturalSort {
        
    public static House__c[] sortHousesByName(House__c[] listToSort){
        HouseWrapper[] housersWrapper = new HouseWrapper[]{};
            for(House__c h:listToSort){
                housersWrapper.add(new HouseWrapper(h) );
            }
        housersWrapper.sort();
        
        House__c[] sortedList = new House__c[]{};
        
        for(HouseWrapper hw:housersWrapper){
            sortedList.add( hw.house );
        }
        
        return sortedList;
    }
        
    public class  HouseWrapper implements Comparable {
        
        public House__c house ;
        
        public HouseWrapper(House__c h) {
            house = h;
            
        }
        public Integer compareTo(Object compareTo) {
            // Cast argument to OpportunityWrapper
            HouseWrapper compareToHouse = (HouseWrapper)compareTo;
            return NaturalSort.compare(house.House_Name__c, compareToHouse.house.House_Name__c);    
        }
    }
    
    
    private static  boolean isDigit(String ch)
    {
        return (ch.isNumeric());
    }
    
    private static  String getChunk(String s, Integer slength, Integer marker)
    {
        String chunk ='';
        String c = s.substring(marker,marker+1);
        chunk += c;
        marker++;
        System.debug(c +' c');
        if (c.isNumeric())
        {
            while (marker < slength)
            {
                c = s.substring(marker,marker+1);
                if (!c.isNumeric())
                    break;
                chunk += c;
                marker++;
            }
        } else
        {
            while (marker < slength)
            {
                c = s.substring(marker,marker+1);
                if (isDigit(c))
                    break;
                chunk += c ;
                marker++;
            }
        }
        return String.valueOf(chunk);
    }
    
    public static Integer compare(String s1, String s2)
    {
        if ((s1 == null) || (s2 == null)) 
        {
            return 0;
        }
        
        Integer thisMarker = 0;
        Integer thatMarker = 0;
        Integer s1Length = s1.length();
        Integer s2Length = s2.length();
        
        while (thisMarker < s1Length && thatMarker < s2Length)
        {
            String thisChunk = getChunk(s1, s1Length, thisMarker);
            thisMarker += thisChunk.length();
            
            String thatChunk = getChunk(s2, s2Length, thatMarker);
            thatMarker += thatChunk.length();
            
            // If both chunks contain numeric characters, sort them numerically
            Integer result = 0;
            if ( thisChunk.substring(0,1).isNumeric()  && thatChunk.substring(0,1).isNumeric() )
            {
                // Simple chunk comparison by length.
                Integer thisChunkLength = thisChunk.length();
                result = thisChunkLength - thatChunk.length();
                // If equal, the first different number counts
                if (result == 0)
                {
                    for (Integer i = 0; i < thisChunkLength; i++)
                    {
                        result = thisChunk.charAt(i) - thatChunk.charAt(i);
                        if (result != 0)
                        {
                            return result;
                        }
                    }
                }
            } 
             else
            {
                result = thisChunk.compareTo(thatChunk);
            }
            
            if (result != 0)
                return result;
        }
        /*
        Integer finalValue = s1Length - s2Length;
        System.debug(s1.compareTo(s2) + ' <- compare | final -> ' +finalValue);
        System.debug(s1 +' <- s1 | s2 -> ' +s2);
        if(finalValue == 0){
            finalValue = s1.compareTo(s2);
        }
        return finalValue;
		*/
        return  s1Length - s2Length;
    }
    
}