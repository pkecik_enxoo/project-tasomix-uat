/*
* @author Jarosław Kołodziejczyk 2017-04-04
* 1. Klasa służąca do sortowania listy obiektów po wybranym parametrze
*/


global class FlockStatsWrapper implements Comparable {

    public Flock_stats__c fls;
    
    // Constructor
    public FlockStatsWrapper(Flock_stats__c op) {
        fls = op;
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        // Cast argument to FlockStatsWrapper
        FlockStatsWrapper compareToOppy = (FlockStatsWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (fls.Date__c > compareToOppy.fls.Date__c) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (fls.Date__c < compareToOppy.fls.Date__c) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}