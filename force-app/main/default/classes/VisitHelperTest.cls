@isTest
public class VisitHelperTest {
    
    @testSetup
    public static void testSetup(){
        Account test = new Account(Name='test', Brojler__c=true);
        insert test;
        Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
        insert testC;
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
        insert farm;
        House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
        insert house;
        //dodaje jakas norme
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
        Standard_Stats__c stdSt = new Standard_Stats__c(Standard__c = std.id, Day__c=2, Daily_water_consumption__c=33, 
                                                        Daily_feed_consumed__c=16, Avg_body_weight__c =50  );
        insert stdSt;
        
        Date day = System.today()-2;
        Flock__c flock = new Flock__c( House__c = house.id, 
                                      Birds_at_arrival__c  = 5000,
                                      Chick_weight_at_arrival__c = 40,
                                        Species__c='Broiler',
                                      Genetic_Line__c = 'Ross 308',
                                      Date_of_arrival__c = day);
        insert flock;
    }
    @isTest
    public static void getForFlockTest(){
        Flock__c flock = [SELECT id, House__c, House__r.Farm__c, House__r.Farm__r.Farm_Owner_Account__c from Flock__c LIMIT 1];
        VisitHelper vH = new VisitHelper(new Set<id>(), new Set <id>(), new Set <id>(), new Set <id>{flock.id} );
        System.assertEquals(flock.House__r.Farm__r.Farm_Owner_Account__c, vH.getAccountForFlock(flock.id) );
        System.assertEquals(flock.House__r.Farm__c, vH.getFarmForFlock(flock.id) );
        System.assertEquals(flock.House__c, vH.getHouseForFlock(flock.id) );
    }
    @isTest
    public static void getForHouseTest(){
        House__c House = [SELECT id, Farm__c, Farm__r.Farm_Owner_Account__c, (SELECT id from Flocks__r where Status__c = 'Active') from House__c LIMIT 1];
        VisitHelper vH = new VisitHelper(new Set<id>(), new Set <id>(), new Set <id>{House.id}, new Set <id>{} );
        System.assertEquals(House.Farm__r.Farm_Owner_Account__c, vH.getAccountForHouse(house.id) );
        System.assertEquals(House.Farm__c, vH.getFarmForHouse(house.id) );
        System.assertEquals(House.Flocks__r[0].id, vH.getFlockForHouse(house.id) );
    }
    @isTest
    public static void getForFarmTest(){
        Farm__c Farm = [SELECT id, Farm_Owner_Account__c from Farm__c LIMIT 1];
        VisitHelper vH = new VisitHelper(new Set<id>(), new Set <id>{Farm.id}, new Set <id>(), new Set <id>{} );
        System.assertEquals(Farm.Farm_Owner_Account__c, vH.getAccountForFarm(Farm.id) );    
    }
    
    @isTest
    public static void fillVisitFields(){
        Flock__c flock = [SELECT id, House__c, House__r.Farm__c, House__r.Farm__r.Farm_Owner_Account__c from Flock__c LIMIT 1];
        VisitHelper vH = new VisitHelper(new Set<id>(), new Set <id>(), new Set <id>(), new Set <id>{flock.id} );
        Visit__c visit = new Visit__c();//Flock__c = flock.id);
        vH.fillEmptyFields(visit);
        //System.assertEquals(flock.House__r.Farm__r.Farm_Owner_Account__c, visit.Farm_Owner_Account__c  );   
    }
    
    @isTest
    public static void fillIssueFields(){
        House__c House = [SELECT id, Farm__c, Farm__r.Farm_Owner_Account__c from House__c LIMIT 1];
        VisitHelper vH = new VisitHelper(new Set<id>(), new Set <id>(), new Set <id>{House.id}, new Set <id>{} );
        //Issue__c Issue = new Issue__c(House__c = House.id);
        //vH.fillEmptyFields(Issue);
        //System.assertEquals(House.Farm__r.Farm_Owner_Account__c, Issue.Farm_Owner_Account__c  );   
    }
    
    @isTest
    public static void sortTest(){
        
        Account test1 = new Account(Name='test2', Brojler__c=true);
        insert test1;
        Contact testC1 = new Contact(AccountId = test1.id, LastName = 'testFlock2');
        insert testC1;
        Farm__c farm1 = new Farm__c(Manager__c = testC1.id, Farm_Owner_Account__c = test1.id, Postal_Code__c = '123-123');
        insert farm1;
        House__c house1 = new House__c(Farm__c = farm1.id, House_Name__c = 'Kurnik 2', House_area__c = 1000);
        insert house1;
        
        Test.startTest();
        Farm__c[] Farms = [SELECT id, name, Farm_Owner_Account__c from Farm__c ];
                
        Farm__c[] FarmsNoChange = VisitHelper.sortByOngoingVisit(Farms);
        System.assertEquals(Farms[0].id, FarmsNoChange[0].id  );   
         
        Visit__c visit1 = new Visit__c(Farm__c = Farms[0].id, Date__c = system.today(), Start_Date__c = system.now() );
        Visit__c visit2 = new Visit__c(Farm__c = Farms[1].id, Date__c = system.today(), Start_Date__c = system.now() );
        
        insert visit1;
 
        visit1.End_Date__c = system.now();
        update visit1;
        
        
        Farm__c[] farmsSorted1 =  VisitHelper.sortByOngoingVisit(farms); 
        System.assertEquals(visit1.Farm__c, farmsSorted1[0].id  );
        
        insert visit2;
        visit2 = [select id, Farm_Owner_Account__c, Farm__c, name from Visit__c where id =:visit2.id ] ;                             
      
        
        farms = [SELECT id, Farm_Owner_Account__c, name from Farm__c ];
        Farm__c[] farmsSorted2 =  VisitHelper.sortByOngoingVisit(farms); 
        System.assertEquals(visit2.Farm__c, farmsSorted2[0].id  );
        
        
         
        Account[] accs = [SELECT id, name from Account ];
        Account[] accsSorted =  VisitHelper.sortByOngoingVisit(accs); 
        System.assertEquals(visit2.Farm_Owner_Account__c, accsSorted[0].id  ); 
        
        Test.stopTest();
    }
    
    
    
}