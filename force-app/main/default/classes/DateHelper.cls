public class DateHelper {
    public static String GetIsoWeekString(Date dt){
        if(dt == null){
            return null;
        }
        Integer isoWeekInt = GetIsoWeek(dt);
        return  Math.mod(isoWeekInt, 100) + '-' + isoWeekInt / 100;
    }
    public static Date GetIsoWeekOne(Integer Year) {
        // get the date for the 4-Jan for this year
        Date dt = Date.newInstance(Year, 1, 4);
        Date sunday = Date.newInstance(1900, 1, 7);
        date DateB = dt.toStartofWeek();

		integer numberDays = Math.mod(sunday.daysBetween(dt), 7);
        
        // get the ISO day number for this date 1==Monday, 7==Sunday
        Integer dayNumber = numberDays; // 0==Sunday, 6==Saturday
        if (dayNumber == 0) {
            dayNumber = 7;
        }
        
        // return the date of the Monday that is less than or equal
        // to this date
        return dt.AddDays(1 - dayNumber);
    }
    // returns calendar week and year 
    // to get week Math.mod( result , 100) 
    // to get year result / 100;
    public static Integer GetIsoWeek(Date dt) {
        if(dt == null){
            return null;
        }
        Date week1;
        Integer IsoYear = dt.Year();
        if (dt >=  Date.newInstance(IsoYear, 12, 29)) {
            week1 = GetIsoWeekOne(IsoYear + 1);
            if (dt < week1) {
                week1 = GetIsoWeekOne(IsoYear);
            }
            else {
                IsoYear++;
            }
        }
        else {
            week1 = GetIsoWeekOne(IsoYear);
            if (dt < week1) {
                week1 = GetIsoWeekOne(--IsoYear);
            }
        }
        
        return (IsoYear * 100) +
            ((week1.daysBetween(  dt) ) / 7 + 1);
    }
}