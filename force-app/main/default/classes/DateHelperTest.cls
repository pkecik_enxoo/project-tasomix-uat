@isTest
public class DateHelperTest {
	@isTest
    public static void testGetIsoWeekString(){
        
        // random dates
        System.assertEquals('13-2010', DateHelper.GetIsoWeekString(Date.newInstance(2010,04,04)));
        System.assertEquals('25-2011', DateHelper.GetIsoWeekString(Date.newInstance(2011,06,26)));
        System.assertEquals('34-2015', DateHelper.GetIsoWeekString(Date.newInstance(2015,08,22)));
        System.assertEquals('13-2018', DateHelper.GetIsoWeekString(Date.newInstance(2018,03,29)));
        System.assertEquals('2-2024', DateHelper.GetIsoWeekString(Date.newInstance(2024,01,11)));
        
        // corner cases
        System.assertEquals('52-2011', DateHelper.GetIsoWeekString(Date.newInstance(2011,12,26)));
        System.assertEquals('52-2011', DateHelper.GetIsoWeekString(Date.newInstance(2012,1,1)));
        System.assertEquals('52-2012', DateHelper.GetIsoWeekString(Date.newInstance(2012,12,30)));
        System.assertEquals('53-2020', DateHelper.GetIsoWeekString(Date.newInstance(2020,12,28)));
        System.assertEquals('53-2020', DateHelper.GetIsoWeekString(Date.newInstance(2021,1,3)));
        System.assertEquals('52-2021', DateHelper.GetIsoWeekString(Date.newInstance(2021,12,27)));
        System.assertEquals('52-2021', DateHelper.GetIsoWeekString(Date.newInstance(2022,1,2)));
        System.assertEquals('1-2025', DateHelper.GetIsoWeekString(Date.newInstance(2024,12,30)));
        System.assertEquals('1-2025', DateHelper.GetIsoWeekString(Date.newInstance(2025,1,5)));

        // test one whole week
        System.assertEquals('1-2013', DateHelper.GetIsoWeekString(Date.newInstance(2012,12,31)));
        System.assertEquals('1-2013', DateHelper.GetIsoWeekString(Date.newInstance(2013,1,1)));
        System.assertEquals('1-2013', DateHelper.GetIsoWeekString(Date.newInstance(2013,1,2)));
        System.assertEquals('1-2013', DateHelper.GetIsoWeekString(Date.newInstance(2013,1,3)));
        System.assertEquals('1-2013', DateHelper.GetIsoWeekString(Date.newInstance(2013,1,4)));
        System.assertEquals('1-2013', DateHelper.GetIsoWeekString(Date.newInstance(2013,1,5)));
        System.assertEquals('1-2013', DateHelper.GetIsoWeekString(Date.newInstance(2013,1,6)));
      
        System.assertEquals(null, DateHelper.GetIsoWeekString(null));  
        System.assertEquals(null, DateHelper.GetIsoWeek(null));  
    }
}