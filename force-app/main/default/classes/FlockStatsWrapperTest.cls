@isTest 
private class FlockStatsWrapperTest {
    static testmethod void test1() {
        // Add the opportunity wrapper objects to a list.
        FlockStatsWrapper[] oppyList = new List<FlockStatsWrapper>();
        Date closeDate = Date.today().addDays(10);
        oppyList.add( new FlockStatsWrapper(new Flock_stats__c(
            Date__c=closeDate+4)));
        oppyList.add( new FlockStatsWrapper(new Flock_stats__c(
            Date__c=closeDate+1)));
       oppyList.add( new FlockStatsWrapper(new Flock_stats__c(
            Date__c=closeDate)));
       oppyList.add( new FlockStatsWrapper(new Flock_stats__c(
            Date__c=closeDate+3)));
		        
        // Sort the wrapper objects using the implementation of the 
        // compareTo method.
        oppyList.sort();
        
        // Verify the sort order
        System.assertEquals(closeDate, oppyList[0].fls.Date__c);
        System.assertEquals(closeDate+1, oppyList[1].fls.Date__c);
        System.assertEquals(closeDate+3, oppyList[2].fls.Date__c);
        System.assertEquals(closeDate+4, oppyList[3].fls.Date__c);
        // Write the sorted list contents to the debug log.
        System.debug(oppyList);
    }
}