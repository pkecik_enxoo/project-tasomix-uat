@isTest
public class GPSFarmControllerTest {
    @testSetup
    public static void testSetup(){
        
        Account test1 = new Account(Name='test', Brojler__c=true);
        insert test1;
        Contact testC = new Contact(AccountId = test1.id, LastName = 'testFlock');
        insert testC;
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test1.id, Postal_Code__c = '123-123', Geolocation__Latitude__s = 1, Geolocation__Longitude__s = 1);
        insert farm;
    }
    @isTest
    public static void test(){
        Farm__c farm =[SELECT id, Geolocation__Latitude__s, Geolocation__Longitude__s from Farm__c  LIMIT 1];
        PageReference pageRef = Page.GPSFarmLocation;
        pageRef.getParameters().put('latitude', '0');
        pageRef.getParameters().put('longitude', '0');
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('Id', String.valueOf(farm.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(farm);
        GPSFarmController extension = new GPSFarmController(sc);
        
        
        System.assertEquals(1, [select Geolocation__Latitude__s from Farm__c where id=: farm.id].Geolocation__Latitude__s);
        extension.farm.Geolocation__Latitude__s = 0;
        extension.farm.Geolocation__Longitude__s = 0;
       	extension.saveLocation();
             
        System.assertEquals(0, [select Geolocation__Latitude__s from Farm__c where id=: farm.id].Geolocation__Latitude__s);
        
        extension.updateLocation();
        System.assertEquals(0, extension.farm.Geolocation__Latitude__s);
        System.assertEquals(0, extension.farm.Geolocation__Longitude__s);
        
        extension.cancel();
    }
    @isTest
    public static void testCallout(){
        Farm__c farm =[SELECT id, Geolocation__Latitude__s, Geolocation__Longitude__s from Farm__c LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new GeolocationHelper.GoogleGeolocationCalloutMock());
        
        PageReference pageRef = Page.GPSFarmLocation;
        pageRef.getParameters().put('latitude', '0');
        pageRef.getParameters().put('longitude', '0');
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('Id', String.valueOf(farm.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(farm);
        GPSFarmController extension = new GPSFarmController(sc);
        test.startTest();
        extension.getLocationBasedOnAddress();
        test.stopTest();
    }
    
    
}