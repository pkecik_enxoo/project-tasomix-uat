/**
 	* @author Maria Raszka 2017-03-23
	* Kontroller do porównywania serii danych
	*/
public class ComparePanelWrapperApexController {

    // pobieranie serii danych wsadowych dla komponentu rysyjacego wykres
    // 1. dane wejsciowe sa parsowanie
    // 2. pobieram serie danych
    // 3. serializuje odpowiedz
    @AuraEnabled
    public static String getRemoteSeries(String params) {
        System.debug('Apex getRemoteSeries called '+ params );
        List<ComparePanelSelectParams> paramList = new List<ComparePanelSelectParams>();
        JSONParser parser = JSON.createParser(params);
        while (parser.nextToken() != null) {
            // Start at the array of invoices.
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != null) {
                    // Advance to the start object marker to
                    //  find next invoice statement object.
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        ComparePanelSelectParams tmp = (ComparePanelSelectParams)parser.readValueAs(ComparePanelSelectParams.class);
                        system.debug('Flock id: ' + tmp.flockId);
                        system.debug('param: ' + tmp.param);
                        paramList.add(tmp);
                        parser.skipChildren();
                    }
                }
            }
        }

        String result = JSON.serialize(ComparePanelWrapperApexController.getAllSeries(paramList));
        return result;
    }

    // Pobieranie serii danych dla wskazanych wstawien
    // 1. pobieram liste id wstawien
    // 2. wyszukuje statystyk stada dla podanych flokow
    // 3. grupuje dane wg flokow
	// 4. tworze serie danych
    public static List<DataSeries> getAllSeries(ComparePanelSelectParams[] paramList)
    {
        List<DataSeries> allSeries = new List<DataSeries>();
        System.debug('getAllSeries called - list size:' + paramList.size());
        try{          
            // 1. pobieram liste id wstawien
            List<String> flockIdList = new List<String>();
            // lista pomicnicza z id norm juz dodanych -> nie ma potrzeby generowac kilka razy tej samej normy
            Set<String> stdIdList = new Set<String>();
            // TODO: jesli bedzie zmaina dodanie wiecj niez jednego parametrow trzeba zmianic klucz w mapie!
            Map<String, ComparePanelSelectParams> flockMap = new Map<String, ComparePanelSelectParams>();
            for(ComparePanelSelectParams singleSearch : paramList)
            {
                System.debug('getAllSeries flockId:' + singleSearch.flockId);
                if(singleSearch.flockId != '')
                {
                    flockIdList.add(singleSearch.flockId);
                    flockMap.put(singleSearch.flockId, singleSearch);
                }
            }
            System.debug('Liczba wskazanych wstawien ' + flockIdList.size());
            // 2. wyszukuje statystyk stada dla podanych flokow
            List<Flock_Stats__c> flockStats = [Select Avg_body_weight__c, Avg_feed_consumed__c, Avg_water_consumption__c, Water_Feed__c, Daily_dead_total__c, 
                                               Daily_dead__c, Daily_selection__c, Daily_feed_consumed__c, Daily_water_consumption__c, Day__c, flock__c,
                                               Weight_in_Chick_standard__c, Feed_consumption_in_Chick_stndard__c , Water_consumption_in_Chick_Standard__c 
                                               from Flock_Stats__c where Flock__c in : flockIdList order by flock__c, day__c];
            List<Flock__c> flockList = [Select Standard__c, Standard__r.Genetic_Line__c, House__r.Farm__r.Name, House__r.House_Name__c , Flock_number__c from Flock__c where id in : flockIdList order by id ];
           

            if(flockStats.size()<1)
            {
            	return null;
            }
            
            Integer maxStatDay = findMaxDay( flockStats );
            
            // standardy będą z przodu tabeli
            for(ComparePanelSelectParams singleSearch : paramList)
            {
            
            	Flock__c flock = getFlockForId(singleSearch.flockId,  flockList);
                
                if(flock==null)
                    continue;
                // 3. grupuje dane wg flokow
                List<Flock_Stats__c> singleFlockStList = groupStatsByFlock(flock.id,  flockStats );
                
                String param = singleSearch.param;
                
                // 4. tworze serie danych
                List<Point> singleFlockSerie = getSerie(singleFlockStList, param);
                
                // zapytanie w petli poniewaz maksymalnie bedzie 6 standardow
                if( singleSearch.tableStandard == true )
                {
                    addStandardSeries(flock, param, maxStatDay, allSeries, stdIdList);
                }
            }
            for(ComparePanelSelectParams singleSearch : paramList)
            {
                Flock__c flock = getFlockForId(singleSearch.flockId,  flockList);
                
                if(flock==null)
                    continue;
                // 3'. grupuje dane wg flokow
                System.debug('Dodaje serie dla: ' + flock.Flock_number__c);  
                List<Flock_Stats__c> singleFlockStList = groupStatsByFlock(flock.id,  flockStats );
                
                String param = singleSearch.param;
                system.debug('param ' + singleSearch.param);
                
                // 4'. tworze serie danych
                List<Point> singleFlockSerie = getSerie(singleFlockStList, param);
                
                /*
                System.debug('Porownanie z norma tabela?: ' + singleSearch.tableStandard);
                // zapytanie w petli poniewaz maksymalnie bedzie 6 standardow
                if( singleSearch.tableStandard==true )
                {
                    addStandardSeries(flock, param, flockStats, allSeries, stdIdList);
                }
                */
                System.debug('Porownanie z norma kurczaka?: ' + singleSearch.chickStandard);
                // 5. jesli zaznaczono porownanie z norma chick - to tworze dodatkowa serie
                if(singleSearch.chickStandard==true)
                {
                	 addNewChickSerie( flock,  param,  flockStats, allSeries) ;
                }
                // dodaję pomiar do listy
                addNewSerie(flock, param, singleFlockSerie, allSeries);    
            }
            
        }
        
        catch(Exception e)
        {
            system.debug('exception line ' + e.getLineNumber() + ' :' + e.getMessage());
        }
        return allSeries;
    }
    
    private static Integer findMaxDay(List<Flock_Stats__c> flockStats)
    {
    	Integer maxDay = 0;
    	for(Flock_Stats__c flockStat : flockStats)
    	{
    		Integer dayInt = Integer.valueOf( flockStat.Day__c );
    		if( dayInt > maxDay  )
    		{
    			maxDay = dayInt;
    		}
    	}
    	
    	return maxDay;
    }
    
    private static Flock__c getFlockForId(id flockId, List<Flock__c> flockList)
    {
    	for(Flock__c singleFlock : flockList)
                    {
                        if(singleFlock.id == flockId)
                        {
                            return singleFlock;
                        }
                    }
    	return null;
    }
    
    private static List<Flock_Stats__c> groupStatsByFlock(id flockId, List<Flock_Stats__c> flockStats )
    {
        List<Flock_Stats__c> singleFlockStList = new List<Flock_Stats__c>();
        for(Flock_Stats__c flSt : flockStats)
        {
            if(flSt.flock__c == flockId)
            {
                singleFlockStList.add(flSt);
            }
        }
        System.debug('Ilosc statystyk: ' + singleFlockStList.size());
	    return  singleFlockStList;
    }
    
    private static void addNewSerie(Flock__c flock, String param, List<Point> singleFlockSerie, List<DataSeries> allSeries ){
        String serieLabel = ComparePanelWrapperApexController.getLabelForParam(param)+ ' ' + 
        	flock.House__r.Farm__r.Name + ' ' +flock.House__r.House_Name__c + ' ' + flock.Flock_number__c;
        allSeries.add( new DataSeries( serieLabel, singleFlockSerie, param, flock.Standard__r.Genetic_Line__c ) ); 
    }
    
    private static void addNewChickSerie(Flock__c flock, String param, List<Flock_Stats__c> flockStats, List<DataSeries> allSeries)
    {
    	if( ! ( param=='waga' || param=='woda' || param=='pasza' ) )
        {
        	return;
        }
        List<Point> singleStdChickSerie = ComparePanelWrapperApexController.getSerie(groupStatsByFlock(flock.id,  flockStats ), param+'_pisklak');
        String chickSerieLabel = ComparePanelWrapperApexController.getLabelForParam(param)+' pisklak: ' +flock.House__r.House_Name__c + ' ' + flock.Flock_number__c;
        allSeries.add( new DataSeries(chickSerieLabel, singleStdChickSerie, param) );       
    }
    
    private static void addStandardSeries(Flock__c flock, String param, Integer lastDay, List<DataSeries> allSeries, Set<String> stdIdList )
    {
        if( ! ( param=='waga' || param=='woda' || param=='pasza' ) )
        {
        	return;
        }
        // sprawdzam czy standard byl juz pobrany
        if(flock.Standard__c!= null && !stdIdList.contains(flock.Standard__c+'_'+param)) 
        {
            // pobieram dane 
            // zapytanie w petli poniewaz maksymalnie bedzie 6 standardow
            Standard_Stats__c[] stdStList = [Select Day__c, Daily_feed_consumed__c, Daily_water_consumption__c, 
                                             Avg_body_weight__c from Standard_Stats__c where Standard__c=:flock.Standard__c AND Day__c <=: lastDay order by Day__c];
            List<Point> stdSerie = ComparePanelWrapperApexController.getStdSerie(stdStList, param);
            String stdSerieLabel = ComparePanelWrapperApexController.getLabelForParam(param)+ ' tabela ' +flock.Standard__r.Genetic_Line__c ;
            DataSeries stdDataSeries = new DataSeries(stdSerieLabel, stdSerie, param);
            allSeries.add(stdDataSeries);   
            stdIdList.add(flock.Standard__c+'_'+param);
        }
    }
    
    
    
    // pobieranie opisu dla parametru
    public static String getLabelForParam(String param)
    {
        if(param=='waga')
            return 'Waga';
        else if(param=='woda')
            return 'Spoż. wody';
        else if(param=='pasza')
            return 'Spoż. paszy';
        else if(param=='upadki')
            return 'Upadki';
        else if(param=='woda_pasza')
            return 'Woda/pasza';
        else
            return '';
    }
    
    // kazda seria danych to zbior punktow - pobieram lise dla jednej serii
    public static List<Point> getSerie(List<Flock_Stats__c> flockStList, String selectedParam) {
		List<Point> serie = new List<Point>();
        
        for(Flock_Stats__c singleSt : flockStList)
        {
            Point tmpPoint=ComparePanelWrapperApexController.getStat(singleSt, selectedParam);
            if(tmpPoint!=null)
 	    		serie.add(tmpPoint);
		}
        return serie;    
    }
    
    // pobieranie konkretnej statystyki w zaleznosci od parametru
    public static Point getStat(Flock_Stats__c flockSt, String selectedParam)
    {         
        if(selectedParam == 'waga' && flockSt.Avg_body_weight__c!= null)
			return new Point(flockSt.Day__c, flockSt.Avg_body_weight__c);
        else if(selectedParam == 'woda' && flockSt.Daily_water_consumption__c!= null)
			return new Point(flockSt.Day__c, flockSt.Avg_water_consumption__c);
        else if(selectedParam == 'pasza' && flockSt.Daily_feed_consumed__c!= null)
			return new Point(flockSt.Day__c, flockSt.Avg_feed_consumed__c);
        else if(selectedParam == 'upadki' && (flockSt.Daily_dead__c!= null || flockSt.Daily_selection__c!=null) )
			return new Point(flockSt.Day__c, flockSt.Daily_dead_total__c);
        else if(selectedParam == 'woda_pasza' && flockSt.Daily_feed_consumed__c!= null && flockSt.Daily_water_consumption__c!=null)
			return new Point(flockSt.Day__c, flockSt.Water_Feed__c);
        else if(selectedParam == 'waga_pisklak' && flockSt.Avg_body_weight__c!= null)
			return new Point(flockSt.Day__c, flockSt.Weight_in_Chick_standard__c);
        else if(selectedParam == 'woda_pisklak' && flockSt.Water_consumption_in_Chick_Standard__c != null)
			return new Point(flockSt.Day__c, flockSt.Water_consumption_in_Chick_Standard__c);
        else if(selectedParam == 'pasza_pisklak' && flockSt.Feed_consumption_in_Chick_stndard__c != null)
			return new Point(flockSt.Day__c, flockSt.Feed_consumption_in_Chick_stndard__c);
        else
            return null;
    }
    
   public static List<Point> getStdSerie(List<Standard_Stats__c> standardStList, String selectedParam) {
		List<Point> serie = new List<Point>();
        
        for(Standard_Stats__c singleSt : standardStList)
        {
            Point tmpPoint=ComparePanelWrapperApexController.getStdStat(singleSt, selectedParam);
            if(tmpPoint!=null)
 	    		serie.add(tmpPoint);
		}
        return serie;    
    }
    
    // pobieranie konkretnej statystyki w zaleznosci od parametru
    public static Point getStdStat(Standard_Stats__c stdSt, String selectedParam)
    {  
        if(selectedParam == 'waga' && stdSt.Avg_body_weight__c!= null)
			return new Point(stdSt.Day__c, stdSt.Avg_body_weight__c);
        else if(selectedParam == 'woda' && stdSt.Daily_water_consumption__c!= null)
			return new Point(stdSt.Day__c, stdSt.Daily_water_consumption__c);
        else if(selectedParam == 'pasza' && stdSt.Daily_feed_consumed__c!= null)
			return new Point(stdSt.Day__c, stdSt.Daily_feed_consumed__c);
        else
            return null;
    }
    
    // wrapper do serii danych
    public class DataSeries{
        public String label {get;set;}
        public List<Point> data {get;set;}
        public String param {get;set;}
        public String yAxisID {get;set;}
        public String geneticLine {get;set;}
        
		public DataSeries(String label, List<Point> data, String param, String geneticLine)
        {
            this.label = label;
            this.data = data;
            this.param = param;
            this.yAxisID = 'y-'+param;
            this.geneticLine = geneticLine;
        }
		public DataSeries(String label, List<Point> data, String param)
        {
        	this(label, data, param, '');
        }
    }
    
    // wrapper do moich punktów
    public class Point{
        public Decimal x {get;set;}
        public Decimal y {get;set;}
        
        public Point(Decimal x, Decimal y)
        {
            this.x = x;
            this.y = y;
        }
    }    

}