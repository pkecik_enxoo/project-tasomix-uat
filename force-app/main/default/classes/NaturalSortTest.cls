@isTest
public class NaturalSortTest {
	@isTest
    public static void test(){
        System.assert( NaturalSort.compare('K 1','K2') > 0);
        System.assert( NaturalSort.compare('K10','K2') > 0);
        System.assert( NaturalSort.compare('K13','K9') > 0);
        System.assert( NaturalSort.compare('K13a','K9') > 0);
        System.assert( NaturalSort.compare('13000','234') > 0);
        
        
        System.assert( NaturalSort.compare('K1','K2') < 0);
        System.assert( NaturalSort.compare('K10','K20') < 0);
        System.assert( NaturalSort.compare('K12','K13') < 0);
        System.assert( NaturalSort.compare('K112','K122') < 0);
        System.assert( NaturalSort.compare('K1','K32') < 0);
        System.assert( NaturalSort.compare('K10','K20eee') < 0);
        System.assert( NaturalSort.compare('123','20384') < 0);
        System.assert( NaturalSort.compare('13aK','K9K') < 0);
        System.assert( NaturalSort.compare('13000','232344') < 0);
        
        
        System.assert( NaturalSort.compare('K10','K10') == 0);
        System.assert( NaturalSort.compare('K5','K5') == 0);
        System.assert( NaturalSort.compare('K5 b','K5 b') == 0);
    }
}