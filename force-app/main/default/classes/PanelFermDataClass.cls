/*
* @author Jarosław Kołodziejczyk 2017-04-04
* Klasa potrzebna do generacji listy powiązanych danych dla kontrolera aurowego/jsowego, zawiera z sobie inną klasę przechowującą konkretne parametry dla danych kurników i ferm
*/

public with sharing class PanelFermDataClass {
	@AuraEnabled
	public House__c house {get;set;}
	@AuraEnabled
	public Flock__c flock {get;set;}
	@AuraEnabled
	public PanelFermFlockStatsParams flockStats {get;set;}
}