/*
    * @author Maria Raszka 2017-04-18
    * ustawianie estymowanej liczny ptakow na podstawie wczesniejszych statystyk i ubiorek
    */

public class FlockStatsEstimateNumberOfBirds {

    // 1. Pobieram wszystkie flock staty
    // 2. Pobietam ubiorki
    // 3. Dla kazdego z flokow 
    //// 3.1. Tworze pomocnicza liste statow
    //// 3.2. Tworze pomocnicza liste ubiorek
    //// 3.3. estymuje ilosc ptakow
    // 4. Aktualizuje liste statow
    public void estimateForFlocks( Set<Id> flockIdSet )
    {
        system.debug('FlockStatsEstimateNumberOfBirds.estimateForFlockStatUpdate - initialized size: '+ flockIdSet.size());
        List<Flock_stats__c> listToUpdate = new List<Flock_stats__c>();
        // 1. Pobieram wszystkie  flock staty
        List<Flock_stats__c> allFlstList = [Select flock__c, flock__r.Birds_at_arrival__c, flock__r.Birds_After_Complaint__c, flock__r.Birds_Under_Complaint__c, flock__r.Date_of_Complaint__c, Date__c, Estimated_number_of_birds__c, Daily_dead__c, Daily_selection__c from Flock_stats__c where flock__c in : flockIdSet order by flock__c, Date__c];
        
        // 2. Pobietam ubiorki
        List<Thinning__c> allThngs = [Select flock__c, Date__c, Birds_thinned_at_farm__c from Thinning__c where flock__c in : flockIdSet order by flock__c, Date__c];
        
        // 3. Dla kazdego z flokow 
        for(Id flockId : flockIdSet)
        {
            //// 3.1. Tworze pomocnicza liste statow    
            List<Flock_stats__c> singleFlockStList = new List<Flock_stats__c> ();
            for(Flock_stats__c singleFlSt : allFlstList)
            {
                if(singleFlSt.flock__c == flockId)
                {
                    singleFlockStList.add(singleFlSt);
                }
            }
             //// 3.2. Tworze pomocnicza liste ubiorek
            List<Thinning__c> singleFlockThngs = new List<Thinning__c>(); 
            for(Thinning__c singleThg : allThngs)
            {
                if(singleThg.flock__c == flockId)
                {
                    singleFlockThngs.add(singleThg);
                }
            }
            
            if(singleFlockStList.size()>0)
            {
                //// 3.3. estymuje ilosc ptakow    
                List<Flock_stats__c> tmpUpdateList = calculate(singleFlockStList, singleFlockThngs);   
                if(tmpUpdateList.size()>0)
                    listToUpdate.addAll(tmpUpdateList);
            }
        }
        // 4. Aktualizuje liste
        if(listToUpdate.size()>0)
        {
            system.debug('FlockStatsEstimateNumberOfBirds ilosc aktualizowanych statystyk '+ listToUpdate.size());
            update listToUpdate;
        }
    }
    
    // 1. Pobieram liczbe ptakow
    // 2. Dla kazdej pozycji ze statystyk
    //// 2.1 Odejmuje wczesniejsze ubiorki
    //// 2.2 Odejmuję liczbę ptaków Birds_Under_Complaint__c jeżeli data statystyki jest większa lub równa dacie reklamacji
    //// 2.3 Przyrownuje do poprzedniej wartoscji jesli jest inna to pozycje nalezy zaktualizowac
    //// 2.4 Odejmuje selekcje i upadki z danego dnia
    // 3. Zwracam liste do aktualizacji
    public List<Flock_stats__c> calculate (List<Flock_stats__c> allFlstList, List<Thinning__c> allThinningsList)
    {    
        // 1. Pobieram liczbe ptakow
        Flock__c flock = allFlstList.get(0).flock__r; 
        decimal NumOfBirds = flock.Birds_at_arrival__c;
        List<Flock_stats__c> listToUpdate = new List<Flock_stats__c>();
        Boolean includesComplaint = false;
        // 2. Dla kazdej pozycji ze statystyk
        for(Flock_stats__c singleFlock : allFlstList)
        {
            
            system.debug('FlockStatsEstimateNumberOfBirds kalkuluje dzien ' + singleFlock.Date__c );
            List<Thinning__c> tmpList = new List<Thinning__c>();
            tmpList.addAll(allThinningsList);

            //// 2.1 Odejmuje wczesniejsze ubiorki -> ilosc ptakow zadeklarowana na farmie
            for(Thinning__c thg : tmpList)
            {
                if(thg.Date__c < singleFlock.Date__c)
                {
                    system.debug('FlockStatsEstimateNumberOfBirds odejmuje ubiorke ' + thg.Birds_thinned_at_farm__c );
                    NumOfBirds = NumOfBirds - (thg.Birds_thinned_at_farm__c == null? 0 : thg.Birds_thinned_at_farm__c);
                    allThinningsList.remove(0);
                }
            }
            // @author Kacper Augustyniak 2018-02-08
            //// 2.2 Odejmuję liczbę ptaków Birds_Under_Complaint__c jeżeli data statystyki jest większa lub równa dacie reklamacji
            
            if(singleFlock.Date__c >= flock.Date_of_Complaint__c && flock.Birds_Under_Complaint__c <> null && includesComplaint == false){
                NumOfBirds -= flock.Birds_Under_Complaint__c ;
                includesComplaint = true;               
                system.debug('FlockStatsEstimateNumberOfBirds wliczam reklamację od dnia '+singleFlock.Date__c);
            }
            // ~ @author Kacper Augustyniak 2018-02-08
            //
            //// 2.3 Przyrownuje do poprzedniej wartoscji jesli jest inna to pozycje nalezy zaktualizowac
            if(singleFlock.Estimated_number_of_birds__c!=NumOfBirds)
            {                
                
                system.debug('FlockStatsEstimateNumberOfBirds zmieniam szacunek w dniu '+singleFlock.Date__c+' z '+  singleFlock.Estimated_number_of_birds__c + ' na ' + NumOfBirds);

                singleFlock.Estimated_number_of_birds__c=NumOfBirds;

                listToUpdate.add(singleFlock);
 
            }
            //// 2.4 Odejmuje selekcje i upadki z danego dnia
            NumOfBirds = NumOfBirds - (singleFlock.Daily_dead__c == null? 0: singleFlock.Daily_dead__c) - (singleFlock.Daily_selection__c==null? 0 :singleFlock.Daily_selection__c);          
        } 
        return listToUpdate;
    }
}