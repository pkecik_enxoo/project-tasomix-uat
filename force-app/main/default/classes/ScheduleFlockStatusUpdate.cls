/*
 * update statusHidden on flocks
 * 
 */ 
global class ScheduleFlockStatusUpdate implements Schedulable {
	global void execute(SchedulableContext sc){
        // select only flocks, where status mismatch
        List<Flock__c> flocks = [select  statushidden__c, status__c, name, House__r.Farm__r.name,
                               id from Flock__c 
                               where Status_Mismatch__c = true
                               FOR UPDATE];
        
        if (flocks.size() < 1 ){
            System.debug('No flocks with mismatching status found');
        }
        for (Flock__c flock:flocks){
        	System.debug( 'Updating flock '+flock.name +' from  '+ flock.House__r.Farm__r.name+' from status ' + flock.statushidden__c+ ' to status ' +flock.status__c);
            flock.statushidden__c = flock.status__c;
        }
        update flocks;
        System.debug( flocks.size() +' flocks updated');
    }
}
/*
 * Example code. Executes code above every day at 2:30
 * 
 * System.schedule('Udate flock status', '0 30 2 * * ?', new ScheduleFlockStatusUpdate());
 * 
 * Prefered code. Executes code above every day at 0:10
 * 
 * System.schedule('Udate flock status', '0 10 0 * * ?', new ScheduleFlockStatusUpdate());
 */