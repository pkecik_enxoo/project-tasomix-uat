@isTest
public class PanelStatystykWrapperControllerTest {
    @isTest
    public static void checkContorller()
    {
        Account test = new Account(Name='test', Brojler__c=true);
        insert test;
        Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
        insert testC;
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
        insert farm;
        House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
        insert house;
        //dodaje jakas norme
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
        Standard_Stats__c stdSt = new Standard_Stats__c(Standard__c = std.id, Day__c=2, Daily_water_consumption__c=33, 
                                                      Daily_feed_consumed__c=16, Avg_body_weight__c =50  );
        insert stdSt;
        
        Date day = System.today()-2;
        Flock__c flock = new Flock__c( House__c = house.id, 
                                      Birds_at_arrival__c  = 5000,
                                      Chick_weight_at_arrival__c = 40,
                                        Species__c='Broiler',
                                      Genetic_Line__c = 'Ross 308',
                                      Date_of_arrival__c = day);
        insert flock;
        
        Flock_stats__c flSt = new Flock_stats__c(Flock__c=flock.id, Date__c = System.today(), Daily_water_consumption__c=33, Daily_feed_consumed__c=16); 
        insert flSt;

        Account[] acc = PanelStatystykWrapperController.getAccounts();       
        system.assertEquals(1, acc.size());
        
        Farm__c[] farms = PanelStatystykWrapperController.getFarms(test.Id);
        system.assertEquals(1, farms.size());
        
        House__c[] houses = PanelStatystykWrapperController.getHouses(farm.Id);
        system.assertEquals(1, houses.size());
        
        Flock__c[] flocks =  PanelStatystykWrapperController.getFlocks(house.Id);
		system.assertEquals(1, houses.size());
       
        
        List<Flock_stats__c> resp = PanelStatystykWrapperController.getFlockStats(flock.id);
       
        system.assertEquals(50, resp.size() );
        
        String jsonInputWoda = '[{"id":"'+flSt.id+'","Daily_dead__c":"10"}]';
        String res = PanelStatystykWrapperController.saveRecordsApex(jsonInputWoda);
        System.debug('test:'+res);
        system.assertEquals('Success', res);
        
        jsonInputWoda = '[{"id":"cokolwiek","Feed_code__c":"10", "hasChanged__c": "true"}]';
        
        try
        {
            res = PanelStatystykWrapperController.saveRecordsApex(jsonInputWoda);
        	System.debug('test:'+res);
        }
        catch (AuraHandledException ae)
        {
            system.assert(true);
        }
        catch (Exception e)
        {
            system.assert(false);
        }
        
        

        String codelist =  PanelStatystykWrapperController.getCodePicklist();
		system.assertEquals(true, codelist.length()>0);
	
    }
}