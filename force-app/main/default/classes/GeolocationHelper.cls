global class GeolocationHelper {
    global class GoogleGeolocationCalloutMock implements HttpCalloutMock  {
        global HTTPResponse respond(HTTPRequest req) {
            System.debug('In AccountsInfoCalloutMock.respond() method.');
            HTTPResponse res = new HTTPResponse();
            res.setStatusCode(200);
            res.setBody('{"status":"OK","results":[{"types":["street_address"],"place_id":"ChIJf7VV1DU5BUcRq6YQwygFImc","geometry":{"viewport":{"southwest":{"lng":17.4436109197085,"lat":51.8031900197085},"northeast":{"lng":17.44630888029151,"lat":51.8058879802915}},"location_type":"ROOFTOP","location":{"lng":17.4449599,"lat":51.804539}},"formatted_address":"Czarny Sad 50, 63-720 Czarny Sad, Poland","address_components":[{"types":["premise"],"short_name":"50","long_name":"50"},{"types":["locality","political"],"short_name":"Czarny Sad","long_name":"Czarny Sad"},{"types":["administrative_area_level_2","political"],"short_name":"krotoszyński","long_name":"krotoszyński"},{"types":["administrative_area_level_1","political"],"short_name":"wielkopolskie","long_name":"wielkopolskie"},{"types":["country","political"],"short_name":"PL","long_name":"Poland"},{"types":["postal_code"],"short_name":"63-720","long_name":"63-720"}]}]}');
            return res;
        }
    }
    public enum StatusCode {OK, ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED, INVALID_REQUEST, UNKNOWN_ERROR } 
    
    public static String getGoogleApiKey(){
        return 'AIzaSyDNpZGFjA6np87mNoAQMKIVX2Uqy3jWlv8';//'AIzaSyBuZwG4U7JsujbbjRuHepcHACXlRJkt1Mc'; // hardcoded API key
    }
    
    /*
     * @aurhor Kacper Augustniak 2017-12-06
     * 
     * Class for storing results from Geocoding API
     */  
    public class ApiResponse {
        public List<Geolocation> locations {public get; private set;}
        public StatusCode status {public get; private set;}
        public Boolean isSuccess {public get {
            if(this.status == StatusCode.OK ){  return true; } 
            else {  return false; }
        } }
        
        public ApiResponse(String statusString){
            this.status = GeolocationHelper.parseStringToCode(statusString);
            this.locations = new List<Geolocation>();
        }
        
        public void addLocation(Geolocation loc){
            this.locations.add(loc);
        }
    }
    /*
     * @aurhor Kacper Augustniak 2017-12-06
     * 
     * Class for storing single geolocation
     */  
    public class Geolocation {
        public Decimal lat {public get; private set;}
        public Decimal lng {public get; private set;}
        public String formatted_address {public get; private set;}
        
        public Geolocation(Decimal lat, Decimal lng){
            this.lat = lat;
            this.lng = lng;
        }
        
        public Geolocation(Decimal lat, Decimal lng, String formatted_address){
            this.lat = lat;
            this.lng = lng;
            this.formatted_address = formatted_address;
        }
    }
    /*
    @future ( callout = true )
    public static void getFarmGeolocation(id farmId, String city, String postalCode, String street ){
        ApiResponse resp =  getGeolocationForAddress(city + ' ' + postalCode + ' ' + street);
        if(resp.isSuccess){
            setGeolocationForRecord(Farm__c.sObjectType, Farm__c.Geolocation__c, farmId, resp.locations[0]);
        }
    }*/
    
    /*
     * @aurhor Kacper Augustniak 2017-12-06
     * 
     * method updating geolocation field for a record
     * @param objType - object token to update
     * @param fieldToken - field to update
     * @param id - record id
     * @param geo - geolocation
     */
    /*
    public static void setGeolocationForRecord(Schema.sObjectType objType, Schema.SObjectField fieldToken, id recordId, Geolocation geo){
        if(fieldToken.getDescribe().getType() <> Schema.DisplayType.LOCATION ){
            return;
        }
        sObject sObj = objType.newSObject(recordId);
        String fieldName = fieldToken.getDescribe().getName().removeEnd('__c');
        String latFieldName = fieldName + '__latitude__s';
        sObj.put(latFieldName, geo.lat);
        String lngFieldName = fieldName + '__longitude__s';
        sObj.put(lngFieldName, geo.lng);
        update sObj;
    }
    */
    /*
     * @aurhor Kacper Augustniak 2017-12-06
     * 
     * method return geolocation for addressQuery according to google Geocoding API
     * 1. Create call url
     * 2. Send request
     * 3. If request is successful parse response for first result 
     */ 
    public static ApiResponse getGeolocationForAddress(String addressQuery){
        String apiKey = getGoogleApiKey() ; // hardcoded API key
        String baseUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
        String endpoint = baseUrl + EncodingUtil.urlEncode(addressQuery,'UTF-8') + '&key=' + apiKey;
        return getGeolocationForEndpoint(endpoint);
    }
    /*
    public static ApiResponse getGeolocationForGeolocation(Decimal lat,Decimal lng){
        String locationQuery = String.valueOf(lat) + ',' + String.valueOf(lng);
        String apiKey = getGoogleApiKey() ; // hardcoded API key
        String baseUrl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';
        String endpoint = baseUrl + EncodingUtil.urlEncode(locationQuery,'UTF-8') + '&key=' + apiKey;
        return getGeolocationForEndpoint(endpoint);
    }
*/
    
    private static ApiResponse getGeolocationForEndpoint(String endpoint){
        ApiResponse geocodeRes;
        
        // 1. Create call url
        //String apiKey = getGoogleApiKey() ; // hardcoded API key
        //String baseUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
        //String endpoint = baseUrl + EncodingUtil.urlEncode(addressQuery,'UTF-8') + '&key=' + apiKey;
        
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(endpoint);
        
        Http http = new Http();
        
        try
        {
            // 2. Send request
            HttpResponse res = http.send(req);
            //System.debug('resp: ' + res.getBody()); 
            if (res.getStatusCode() != 200) {
                System.debug('Error from ' + req.getEndpoint() + ' : ' +
                             res.getStatusCode() + ' ' + res.getStatus());
                throw new CalloutException ('Error: ' + res.getStatusCode() + ' ' + res.getStatus());
            }
            Map<String, Object> response = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            String status = String.valueOf(response.get('status'));
            System.debug(JSON.serialize(response) );
            geocodeRes = new ApiResponse(status);
            // 3. If request is successful parse response for results 
            if(status == 'OK'){
                List<Object> results = (List<Object>)response.get('results');
                for(Object result: results){
                    geocodeRes.addLocation(parseAddressComponent(result) );
                }
            } else {
                System.debug('Error getting response. Response status: '+status);
            }
        }
        catch(Exception e)
        {
            System.debug('Error ' + e.getLineNumber() + ' ' + e.getMessage());
            throw new CalloutException( e.getMessage() );
        }
        
        return geocodeRes;
    }
    
    private static Geolocation parseAddressComponent(Object result){
        Map<String, Object> address_components  = (Map<String, Object>)result;
        Map<String, Object> geometry = (Map<String, Object>)address_components.get('geometry');
        Map<String, Object> location = (Map<String, Object>)geometry.get('location');
        
        Decimal lat = Decimal.valueOf( String.valueOf( location.get('lat') ) );
        Decimal lng = Decimal.valueOf( String.valueOf( location.get('lng') ) );
        String formatted_address =  String.valueOf( address_components.get('formatted_address')  );
        return new Geolocation(lat, lng, formatted_address);
    }
    
    private static StatusCode parseStringToCode(String stringCode){
        for (StatusCode sts: StatusCode.values()) {
            if (sts.name() == stringCode) {
                return sts;
            }
        }
        return null;
    }
    
}