public class ComparePanelSelectApexController {

    @AuraEnabled
    public static Account[] getAccounts(){
        List<Account> options = ClientUtils.getAccount();
        return options;
    }
    
    // pobieranie listy ferm dla accounta
    @AuraEnabled
    public static Farm__c[] getFarms(String accId){
        List<Farm__c> options = ClientUtils.getFarms(accId);
        return options;
    }

    // pobieranie listy kurnikow dla wybranej fermy
    @AuraEnabled
    public static House__c[] getHouses(string farmId){
        List<House__c> options = ClientUtils.getHouses(farmId);
        return options;
    }
    
	// pobieranie listy wstawien dla okreslonego kurnika    
    @AuraEnabled
    public static Flock__c[] getFlocks(string houseId){
         List<Flock__c> options = ClientUtils.getFlocks(houseId);
        return options; 
    }

}