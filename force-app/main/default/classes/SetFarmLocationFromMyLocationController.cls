public without sharing class SetFarmLocationFromMyLocationController {
@AuraEnabled
    public static Farm__c saveFarm(Farm__c farm) {
        Farm__c f = new Farm__c(id = farm.id);
        f.Geolocation__Latitude__s = farm.Geolocation__Latitude__s;
        f.Geolocation__Longitude__s = farm.Geolocation__Longitude__s;
        update f;
        return farm;
    }
}