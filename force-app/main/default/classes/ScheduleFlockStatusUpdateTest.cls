@isTest
public class ScheduleFlockStatusUpdateTest {
    @testSetup
    public static void setup(){
        Account test = new Account(Name='test', Brojler__c=true);
        insert test;
        Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
        insert testC;
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
        insert farm;
        House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
        insert house;
        //dodaje jakas norme
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
        Date day = System.today().addDays(-1);
        Flock__c flock = new Flock__c( House__c = house.id, 
                                      Birds_at_arrival__c  = 1000,
                                      Chick_weight_at_arrival__c = 40,
                                        Species__c='Broiler',
                                      Genetic_Line__c = 'Ross 308',
                                      Date_of_arrival__c = day);
        insert flock;
        flock.StatusHidden__c = 'Active';
        update flock;
        
        
    }
    @isTest
    public static void test(){
        scheduleFlockStatusUpdate s = new scheduleFlockStatusUpdate();
        s.execute(null);
    }
}