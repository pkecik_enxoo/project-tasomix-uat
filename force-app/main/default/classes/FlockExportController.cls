public class FlockExportController {
    
    private final string flockId;
    public Flock__c flock {get;set;}
    public Flock__c flockThinning {get;set;}
    public List<Thinning__c> thinningList {get;set;}
    public string xmlheader {get;set;}
    public string endfile {get;set;}
    public string recordId {get;set;}
    
    public FlockExportController()
    {
        xmlheader ='<?xml version="1.0" encoding="UTF-8"?><?mso-application progid="Excel.Sheet"?>';
        endfile = '</Workbook>';
        recordId = ApexPages.currentPage().getParameters().get('recordId');
        flockId = recordId;
        flock = [Select id,Name,House__r.Name, House__r.Farm__r.Name,Flock_number__c,Total_dead_percent__c,Hatchery__c,Genetic_line__c,Total_dead__c,FCR__c,EWW__c,Avg_age__c,Avg_body_weight__c,Feed_Type__c,Birds_at_arrival__c,Confirmed_correction_of_chicks__c,
                 (select id,Name,Date__c,DateHidden__c,Day__c,Avg_body_weight__c,Daily_dead__c,Daily_selection__c,Estimated_number_of_birds__c,Daily_feed_consumed__c,Daily_water_consumption__c,Feed_delivery_from_farm_to_house__c,
                  Feed_code__c,Feed_code_delivered__c,Treatment__c,Note__c,Formulations__c from Flock_stats__r order by Day__c asc) from Flock__c where id =: flockId];
        
        flockThinning = [Select id,Name,House__r.Name, House__r.Farm__r.Name,Flock_number__c,Hatchery__c,Genetic_line__c,Total_dead__c,FCR__c,EWW__c,Avg_age__c,Avg_body_weight__c,Feed_Type__c,Birds_at_arrival__c,
                         (select id,Avg_body_weight__c,Birds_thinned_at_farm__c,Birds_at_slaughterhouse__c,Date__c,Day__c,Dead_chickens__c,Total_weight_at_Slaughterhouse__c,Net_unit_price__c,Weight_of_confiscation__c,
                        Weight_of_dead_chickens__c,Weight_after_reductions__c,Waste_Return__c,Total_weight_at_farm__c,DateHidden__c from Deliveries__r order by Day__c asc) from Flock__c where id =: flockId];
        system.debug('thinningList size: '+thinningList);
        String dateFormatString = 'yyyy-MM-dd';
        
        for(Flock_stats__c fls : flock.Flock_stats__r)
        {
            Datetime dt = Datetime.newInstance(fls.Date__c.year(), fls.Date__c.month(),fls.Date__c.day());
            String dateString = dt.format(dateFormatString);
            fls.DateHidden__c = dateString;
            system.debug('fls.DateHidden__c: '+fls.DateHidden__c);
        }
        
        for(Thinning__c thinn : flockThinning.Deliveries__r)
        {
            Datetime dt2 = Datetime.newInstance(thinn.Date__c.year(), thinn.Date__c.month(),thinn.Date__c.day());
            String dateString2 = dt2.format(dateFormatString);
            thinn.DateHidden__c = dateString2;
            system.debug('thinn.DateHidden__c: '+thinn.DateHidden__c);
        }
		
        system.debug('FlockExportController - FlockId: '+flockId);
        
    }
    
    
    public Pagereference exportAll(){
        return new Pagereference('/apex/flockId');
        
    }
    
    
    
    
}