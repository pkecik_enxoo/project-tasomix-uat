@isTest
public class IssueTriggerTest {

    @testSetup
    public static void testSetup(){
        Account test = new Account(Name='test', Brojler__c=true);
        insert test;
        Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
        insert testC;
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
        insert farm;
        House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
        insert house;
        //dodaje jakas norme
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
        Standard_Stats__c stdSt = new Standard_Stats__c(Standard__c = std.id, Day__c=2, Daily_water_consumption__c=33, 
                                                        Daily_feed_consumed__c=16, Avg_body_weight__c =50  );
        insert stdSt;
        
        Date day = System.today()-2;
        Flock__c flock = new Flock__c( House__c = house.id, 
                                      Birds_at_arrival__c  = 5000,
                                      Chick_weight_at_arrival__c = 40,
                                        Species__c='Broiler',
                                      Genetic_Line__c = 'Ross 308',
                                      Date_of_arrival__c = day);
        insert flock;
    }
    
    @isTest
    public static void testFillFieldsFromVisit(){
        Flock__c flock = [SELECT id, House__c, House__r.Farm__c, House__r.Farm__r.Farm_Owner_Account__c, Farm_Owner_Account__c from Flock__c LIMIT 1];
        Visit__c visit = new Visit__c(Farm_Owner_Account__c = flock.House__r.Farm__r.Farm_Owner_Account__c, Date__c = system.today(), Start_Date__c = system.now());
        insert visit;
        visit = [select id, Farm_Owner_Account__c, Farm__c, Status__c from Visit__c where id =:visit.id ] ;
        system.debug('visit.Status__c '+visit.Status__c);
        
        Visit__c openVisit = VisitHelper.getCurrentUserOngoingVisit();system.debug('openVisit '+openVisit);
        
        Test.startTest();
        Issue__c Issue = new Issue__c(Type__c = 'Alert' );
        insert Issue;
        Test.stopTest();
        Issue = [select id, Farm_Owner_Account__c, Name, Farm__c, House__c, Flock__c from Issue__c where id =:Issue.id ] ; 
        system.debug('Issue '+JSON.serialize(Issue) );
        
        System.assertEquals(flock.House__r.Farm__r.Farm_Owner_Account__c, Issue.Farm_Owner_Account__c );
        //System.assertEquals(flock.House__r.Farm__c, Issue.Farm__c );
        //System.assertEquals(flock.House__c, Issue.House__c );
    }
    
    @isTest
    public static void testFillFields(){
        Flock__c flock = [SELECT id, House__c, House__r.Farm__c, House__r.Farm__r.Farm_Owner_Account__c, Farm_Owner_Account__c from Flock__c LIMIT 1];
        Test.startTest();
        Issue__c Issue = new Issue__c(Type__c = 'Alert', Flock__c = flock.id );
        insert Issue;
        Test.stopTest();
        Issue = [select id, Farm_Owner_Account__c, Name, Farm__c, House__c, Flock__c from Issue__c where id =:Issue.id ] ; 
        system.debug('Issue '+JSON.serialize(Issue) );
        
        System.assertEquals(flock.House__r.Farm__r.Farm_Owner_Account__c, Issue.Farm_Owner_Account__c );
        System.assertEquals(flock.House__r.Farm__c, Issue.Farm__c );
        System.assertEquals(flock.House__c, Issue.House__c );
    }
    
}