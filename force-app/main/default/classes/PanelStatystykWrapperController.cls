public class PanelStatystykWrapperController {
    
    private static Map<String,Integer> speciesToDayNo = new Map<String,Integer>{
        'Turkey' => 147,
        'Broiler' => 49,
        'Duck' =>  60,
        'Goose' =>  112
    };
    
    @AuraEnabled
    public static Account[] getAccounts(){
        List<Account> options = ClientUtils.getAccount();
        options = VisitHelper.sortByOngoingVisit(options);
        return options;
    }
    
    // pobieranie listy ferm dla accounta
    @AuraEnabled
    public static Farm__c[] getFarms(String accId){
        List<Farm__c> options = ClientUtils.getFarms(accId);
        options = VisitHelper.sortByOngoingVisit(options);
        return options;
    }
    
    // pobieranie listy kurnikow dla wybranej fermy
    @AuraEnabled
    public static House__c[] getHouses(string farmId){    
        List<House__c> options = ClientUtils.getHouses(farmId);
        options = VisitHelper.sortByOngoingVisit(options);
        return options;
    }
    
    // pobieranie listy wstawien dla okreslonego kurnika    
    @AuraEnabled
    public static Flock__c[] getFlocks(string houseId){
        List<Flock__c> options = ClientUtils.getFlocks(houseId);
        //options = VisitHelper.sortByOngoingVisit(options);
        return options;
    }
    
    // pobieram liste kodow pasz
    @AuraEnabled
    public static String  getCodePicklist(){
        Schema.DescribeFieldResult fieldResult = Schema.getGlobalDescribe().get('Flock_stats__c').getDescribe().fields.getMap().get('Feed_code__c').getDescribe(); 
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        return JSON.serialize(ple); 
    } 
    
    @AuraEnabled
    public static String  getCodeDeliveredPicklist(){
        Schema.DescribeFieldResult fieldResult = Schema.getGlobalDescribe().get('Flock_stats__c').getDescribe().fields.getMap().get('Feed_code_delivered__c').getDescribe(); 
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        return JSON.serialize(ple); 
    } 
    
    //qfgView
    
    @AuraEnabled
    public static Boolean  getQfgView(){
    	List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
		String proflieName = PROFILE[0].Name;
		system.debug( 'proflieName.endsWith(QFG)' + proflieName.endsWith('QFG'));
        return proflieName.endsWith('QFG'); 
    } 
    
    // pobieranie statystyk dla danego wstawienia
    @AuraEnabled
    public static List<Flock_stats__c> getFlockStats(string flockId){
        System.debug('PanelStatystykWrapperController - Wyszukuje statystyk dla flocka: ' + flockId);
        List<Flock_stats__c> options = new List<Flock_stats__c>();
        Flock__c flockDate = [select Date_of_arrival__c, Id, Name, Species__c from flock__c where id =: flockId];
        if(flockId!= null && flockId != '')
        {
            List<Flock_stats__c> flockList = [Select id, Flock__c, Day__c, Daily_water_consumption__c, Daily_feed_consumed__c, Name, Date__c, 
            								Flock__r.Date_of_arrival__c, Avg_body_weight__c,Daily_dead__c, Daily_selection__c,Feed_code__c,Feed_code_delivered__c,
            								Feed_delivery_from_farm_to_house__c,Manual_body_weight_variability__c,Number_of_weighings__c,Note__c, Treatment__c, Formulations__c  
            								from Flock_stats__c where Flock__c=:flockId 
                                              order by Day__c asc];
            for(Flock_stats__c flock : flockList)
            {
                if(flock.Name != null)
                    options.add(flock);     
            }
        }
        boolean flockFound = false;
        List<Flock_stats__c> flsNewList = new List<Flock_stats__c>();
        Integer days = speciesToDayNo.keySet().contains(flockDate.Species__c) ? speciesToDayNo.get(flockDate.Species__c) : 49;
        for(integer i=0; i<=days; i++)
        {
            for(Flock_stats__c fls : options)
            {
                if(fls.Day__c == i)
                {
                    flockFound = true;
                    // ustawiam has changed na false
                    fls.hasChanged__c = false;
                    flsNewList.add(fls);
                    break;
                }
            }
            if(flockFound == false)
            {
                Flock_stats__c fsNew = new Flock_stats__c(Date__c = flockDate.Date_of_arrival__c + i, Flock__c = flockId,DayHidden__c = i);
                flsNewList.add(fsNew);
            }
            flockFound = false;
        }
        return flsNewList;
    }
    
    @AuraEnabled
    public static String saveRecordsApex(String flNew){
        system.debug('PanelStatystykWrapperController.saveRecordsApex initialized **** ');
        List<Flock_stats__c> flStList = new List<Flock_stats__c>();
        
        flStList = (List<Flock_stats__c>)JSON.deserializeStrict(flNew,List<Flock_stats__c>.class);
        system.debug('PanelStatystykWrapperController - Floc stat list size: '+flStList.size());
        
        List<Flock_stats__c> flockStatsListToUpsert = new List<Flock_stats__c>();
        for(Flock_stats__c flsList : flStList)
        {
            if(flsList.HasChanged__c == true)
            {
                system.debug('PanelStatystykWrapperController - Adding fls to update/insert: '+flsList.Daily_dead__c);
                flockStatsListToUpsert.add(flsList);
            }
        }
        
        try
        {
            system.debug('PanelStatystykWrapperController - upsert size: '+flockStatsListToUpsert.size());
            upsert flockStatsListToUpsert;
        }
        catch(dmlException dmlE)
        {
            system.debug('PanelStatystykWrapperController - Error: '+dmlE.getDmlMessage(0));
            system.debug(flockStatsListToUpsert);
            throw new AuraHandledException('Wystąpił błąd podczas zapsu danych: '+dmlE.getDmlMessage(0));
            return 'error';
        }
        catch(Exception e)
        {
            system.debug('PanelStatystykWrapperController - Error: '+e.getMessage());
            throw new AuraHandledException('Wystąpił błąd: '+e.getMessage());
            return 'error';
        }
        
        return 'Success';
    }
    
}