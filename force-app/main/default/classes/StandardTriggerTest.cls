@isTest
public class StandardTriggerTest {
    
    @testSetup
    static void insertFarm()
    {
        Account test = new Account(Name='test', Brojler__c=true);
        insert test;
        Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
        insert testC;
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
        insert farm;
        House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
        insert house;
        //dodaje jakas norme
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
    }
    
    @isTest
    public static void newStandardInsert()
    {
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
        std = [Select Status__c from Standard__c where id=: std.id];
        System.assertEquals('Active', std.Status__c);
    }
    
    @isTest
    public static void standardUpdate()
    {
        Standard__c oldStd = [Select Status__c from Standard__c limit 1];
        
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
        std = [Select Status__c from Standard__c where id=: std.id];
        std.Effective_Date__c = System.today()-1;
        update std;
        
        oldStd=[Select Status__c from Standard__c where id=: oldStd.id];
        
        System.assertEquals('Inactive', oldStd.Status__c);
    }
}