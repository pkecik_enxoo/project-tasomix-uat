public class FlockHelper {
    public static void updateFeedWeight(set<id> flockIdSet){
        List<Flock_stats__c> allFlstList = [Select flock__C, Feed_Weight_Ratio_Standard__c, flock__r.Last_Feed_Weight_Ratio_Standard__c  from Flock_stats__c where flock__c in : flockIdSet order by  flock__c, Date__c asc ];
        Map<id, Flock__c> flocksMap = new Map<id, Flock__c>();
        for(Flock_stats__c flsStat: allFlstList){
            if(flsStat.Feed_Weight_Ratio_Standard__c <> null){
                Flock__c flock = new Flock__c(id = flsStat.Flock__c);
                flock.Last_Feed_Weight_Ratio_Standard__c = flsStat.Feed_Weight_Ratio_Standard__c;
                flocksMap.put(flsStat.flock__c, flock );
            }
        }
        update flocksMap.values();
    }   
}