/*
 	* @author Maria Raszka 2017-04-18
 	* trigger do szacowania ilosci ptakow -> wywoluje klase pomocnicza jesli zaszy zmiany tj.:
	* -> jesli jest to insert i podano ilosci upadkow lub selekcji 
	* -> jesli jest to update i zmienila sie ilosci z upadkow lub selekcji
 	*/
trigger FlockStatsTriggerUpdate on Flock_stats__c (after update, after insert) {
		
    Set<Id> flockIdSetFeedWeight = new Set<Id>();
	
	for(Flock_stats__c insertedFls : Trigger.new){
		flockIdSetFeedWeight.add(insertedFls.flock__c);
	}
	
	FlockHelper.updateFeedWeight(flockIdSetFeedWeight);
	
	
    Set<Id> flockIdSet = new Set<Id>();
    // 1. sprawdzam czy jest to insert
    //// 1.1 dla kazdej pozycji  sprawdzam czy podano ilosc upadkow lub selekcji
    if(Trigger.isInsert)
    {   
        system.debug('FlockStatsTriggerUpdate after insert initialized, size: ' +  Trigger.new.size() );
        for(Flock_stats__c insertedFls : Trigger.new)
        {
            if(insertedFls.Daily_dead__c != null || insertedFls.Daily_selection__c != null)
            {
                system.debug('FlockStatsTriggerUpdate - podano ilosc w dniu  '+ insertedFls.Date__c );
                flockIdSet.add(insertedFls.flock__c);
            }
        }
    }
    else
    {
        system.debug('FlockStatsTriggerUpdate after update initialized, size: ' +  Trigger.new.size() );
        for(Flock_stats__c updatedFls : Trigger.new)
        {
            if(updatedFls.Daily_dead__c != Trigger.oldMap.get(updatedFls.Id).Daily_dead__c || updatedFls.Daily_selection__c !=  Trigger.oldMap.get(updatedFls.Id).Daily_selection__c)
            {
                system.debug('FlockStatsTriggerUpdate - zmieniono liczbe w dniu '+ updatedFls.Date__c );
                flockIdSet.add(updatedFls.flock__c);
            }
        }
    }

    if(flockIdSet.size()==0)
    {
        system.debug('FlockStatsTriggerUpdate BRAK zmian ilosciowych - KONIEC');
        return;
    }
    FlockStatsEstimateNumberOfBirds flsHandler = new FlockStatsEstimateNumberOfBirds();
    flsHandler.estimateForFlocks(flockIdSet);
    
    /*
    Map<Id,List<Flock_stats__c>> flockStatMapEN = new Map<Id,List<Flock_stats__c>>();
	List<Id> flockIdEN = new List<Id>();
    
    for(Flock_stats__c flockFor : Trigger.new)
    {
        flockIdEN.add(flockFor.Flock__c);
    }
    
    system.debug('FlockStatsTriggerUpdate flockIdEN.size: '+flockIdEN);  
    List<Flock_stats__c> flockStatsListAll = new List<Flock_stats__c>([select id,Date__c,Flock__r.id,Flock__r.Current_number_of_birds__c,Daily_dead_total__c,
                                                                       Daily_selection__c, Daily_dead__c, Estimated_number_of_birds__c 
                                                                       from Flock_stats__c where Flock__r.id in: flockIdEN order by Date__c asc]);
    system.debug('FlockStatsTriggerUpdate flockStatsListAll.size: '+flockStatsListAll.size());
    
    List<Flock_stats__c> flockStatsListToIterate = new LisT<Flock_stats__c>();    
    for(Flock_stats__c fls : Trigger.new)
    {
        if(trigger.newMap.get(fls.id).Daily_dead__c != trigger.oldMap.get(fls.Id).Daily_dead__c || trigger.newMap.get(fls.id).Daily_selection__c != trigger.oldMap.get(fls.Id).Daily_selection__c){
            
            flockStatsListToIterate.add(fls);     
        }
    }
    
    if(flockStatsListToIterate.size()>0)
    {     
        FlockStatsWrapper[] flsListWrapper2 = new List<FlockStatsWrapper>();
        for(Flock_stats__c flsWrapperList2 : flockStatsListToIterate)
        {
            flsListWrapper2.add(new FlockStatsWrapper(flsWrapperList2));
        }
        flsListWrapper2.sort();
        FlockStatsWrapper[] flsListWrapper = new List<FlockStatsWrapper>();
        
        for(Flock_stats__c flsWrapperList : flockStatsListAll)
        {
            flsListWrapper.add(new FlockStatsWrapper(flsWrapperList));
        }
        flsListWrapper.sort();
        
        for(FlockStatsWrapper fwrap : flsListWrapper)
        {
            system.debug('FlockStatsTriggerUpdate.DATE: '+fwrap.fls.Date__c);
        }
        
        List<Flock__c> flockListWithValues = new List<Flock__c>([select id,Birds_at_arrival__c from Flock__c where id in: flockIdEN]);
        
        // lista przyszlych wstawien do aktualizacji
        List<Flock_stats__c> flockStatsListToUpdateAfterTrigger = new List<Flock_stats__c>();
        for(Flock__c flValues : flockListWithValues)
        {
            for(FlockStatsWrapper flsC : flsListWrapper2)
            {
                List<Flock_stats__c> flockStatsListToUpdate = new LisT<Flock_stats__c>();
                
                if(flsC.fls.Flock__c == flValues.id){
                    
                    decimal allRemovedChickens = 0;       
                    for(FlockStatsWrapper flsAll : flsListWrapper)
                    {
                        if(flsAll.fls.Flock__c == flsC.fls.Flock__c )
                        {
                            system.debug('FlockStatsTriggerUpdate.DateIF: '+flsC.fls.Date__c +'  /'+flsAll.fls.Date__c);
                            if(flsC.fls.Date__c > flsAll.fls.Date__c)
                            {
                                allRemovedChickens += flsAll.fls.Daily_dead_total__c; 
                            }
                            else if(flsC.fls.Date__c == flsAll.fls.Date__c)
                            {
                                if(flsC.fls.Daily_selection__c != null )
                                	allRemovedChickens += flsC.fls.Daily_selection__c;
                                if(flsC.fls.Daily_dead__c != null)
                                	allRemovedChickens += flsC.fls.Daily_dead__c; 
                                
                                system.debug('FlockStatsTriggerUpdate allRemovedChickens:' +allRemovedChickens);
                                system.debug('FlockStatsTriggerUpdate Birds_at_arrival__c:' +flValues.Birds_at_arrival__c);
                                flsC.fls.Estimated_number_of_birds__c = (flValues.Birds_at_arrival__c - allRemovedChickens);
                              
                            }
                            else
                            {
                                system.debug('FlockStatsTriggerUpdate kolejne do aktualizacji : '+flsAll.fls);
                            	flockStatsListToUpdate.add(flsAll.fls);
                            }
                        }
              
                    }
                   
                    
                   
                    system.debug('FlockStatsTriggerUpdate. flockStatsListToUpdate'+flockStatsListToUpdate.size());
                    for(Flock_stats__c fls2 : flockStatsListToUpdate)
                    {
                        decimal allRemovedChickens2 = 0;
                        for(FlockStatsWrapper flsAll2 : flsListWrapper)
                        {
                            if(flsAll2.fls.Flock__c == fls2.Flock__c )
                            {
                                system.debug('FlockStatsTriggerUpdate.DateIF: '+fls2.Date__c +'  /'+flsAll2.fls.Date__c);
                                if(fls2.Date__c != flsAll2.fls.Date__c)
                                {
                                    allRemovedChickens2 += flsAll2.fls.Daily_dead_total__c;  
                                }
                                else if(fls2.Date__c == flsAll2.fls.Date__c)
                                {
                                    if(fls2.Daily_selection__c != null)
                                        allRemovedChickens2 += fls2.Daily_selection__c;
                                    if( fls2.Daily_dead__c != null)
                                        allRemovedChickens2 +=  fls2.Daily_dead__c;
                                    
                                    system.debug('FlockStatsTriggerUpdate allRemovedChickens:' +allRemovedChickens2);
                                    system.debug('FlockStatsTriggerUpdate Birds_at_arrival__c:' +flValues.Birds_at_arrival__c);
                                    fls2.Estimated_number_of_birds__c = (flValues.Birds_at_arrival__c - allRemovedChickens2);
                                    system.debug('FlockStatsTriggerUpdate fls2.Estimated_number_of_birds__c:' +fls2.Estimated_number_of_birds__c);
                                    flockStatsListToUpdateAfterTrigger.add(fls2);
                                    break;
                                }
                            }
                        }
                        
                    }
                    
                }
            }
        }
        if(flockStatsListToUpdateAfterTrigger.size()>0)
        {
            system.debug('FlockStatsTriggerUpdate UPDATE IN FlockStatsTrigger');
            update flockStatsListToUpdateAfterTrigger;
        }
        
        
    }
*/

}