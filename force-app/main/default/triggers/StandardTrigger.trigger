/**
 	* @author Maria Raszka 2017-03-20
 	* W momencie wprowadzenia nowej normy system zmienai status starszej normie
	* Jeśli jest to insert
	* 1. Pobieram liste ras 
	* 2. Wyszukuje obowiazujace normy dla ras - norma obowiazuje jesli nie jest nadpisana inna
	* 3. nadpisuje normy : Data zakończenia obowiązywania ustawiana jest na datę “Rozpoczęcia obowiązywania” -1 dla nowej normy.	
	* Jeśli jest to update i norma zmieniła date obowiazywania -> aktualizuje nadpisane normy
	*/

trigger StandardTrigger on Standard__c (before insert, before update) {

    List<Standard__c> stdToUpdate = new List<Standard__c>();
    
    if(Trigger.isInsert)
    {
        List<String> genLineList = new List<String>();
        // 1. Pobieram liste ras 
        for(Standard__c std: Trigger.new)
        {
            genLineList.add(std.Genetic_Line__c);
        }
        
        // 2. Wyszukuje obowiazujace normy dla ras
        List<Standard__c> oldStdList = [Select name, Effective_Date__c, Termination_Date__c, Overriden_by_standard__c, Genetic_Line__c  
                                    from Standard__c where Genetic_Line__c in :genLineList and Overriden_by_standard__c = null ];
        
        for(Standard__c std: Trigger.new)
        {
            Date terminationDate = std.Effective_Date__c;
            terminationDate = terminationDate.addDays(-1);
			for(Standard__c oldStd: oldStdList)
            {
                if(oldStd.Genetic_Line__c == std.Genetic_Line__c)
                {
                    system.debug('Modyfikuje norme ' + oldStd.name);
                    system.debug('Nowa norma ' + std.name);
                    oldStd.Overriden_by_standard__c = std.id;
                    oldStd.Termination_Date__c = terminationDate;
                    stdToUpdate.add(oldStd);
                }
            }
        }
         
	}
    else if(Trigger.isUpdate)
    {
        List<String> stdIdList = new List<String>();
        for(Standard__c std: Trigger.new)
        {
 			if(std.Effective_Date__c != Trigger.oldMap.get(std.id).Effective_Date__c)
            {
                stdIdList.add(std.id);
            }
        }
        
        if(stdIdList.size()>0)
        {
            List<Standard__c> oldStdList = [Select name, Effective_Date__c, Termination_Date__c, Overriden_by_standard__c, Genetic_Line__c  
                                    from Standard__c where Overriden_by_standard__c in : stdIdList];
   
            
            for(Standard__c std: Trigger.new)
            {
                Date terminationDate = std.Effective_Date__c;
                terminationDate = terminationDate.addDays(-1);
                for(Standard__c oldStd: oldStdList)
                {
                    if(oldStd.Genetic_Line__c == std.Genetic_Line__c)
                    {
                        system.debug('Modyfikuje norme ' + oldStd.name);
                        system.debug('Nowa norma ' + std.name);
                        oldStd.Termination_Date__c = terminationDate;
                        stdToUpdate.add(oldStd);
                    }
                }
            }
        }
    }
    
    if(stdToUpdate.size()>0)
    {
		update stdToUpdate;
    }
           
}