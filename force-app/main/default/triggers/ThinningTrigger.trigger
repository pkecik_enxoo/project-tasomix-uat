/**
* @author Maria Raszka 2017-03-21
* 1. System po każdej ubiórce weryfikuje bilans ptaków, 
* jeśli jest mniejszy niż 1000 to automatycznie kończy wstawienie
* z datą ostatniej ubiórki.
* 2. System sprawdza czy nalezy zaktualizowac szacowana ilosc ptakow
*/
trigger ThinningTrigger on Thinning__c (before insert, before update, after insert, after update, after delete) {
    
    if(Trigger.isBefore)
    {   // 1. System po każdej ubiórce weryfikuje bilans ptaków, 
        List<String> flockIdList = new List<String>();
        for(Thinning__c thin : Trigger.new)
        {
            flockIdList.add(thin.flock__c);
        }
        
        List<Flock__c> flockList = [Select Current_number_of_birds__c from Flock__c where id in :flockIdList ];
        List<Flock__c> flockListToUpdate = new List<Flock__c>();
        // mapa do szacowania ilosci ptakow na farmie
        Map<Flock__c, Double> flockMap = new Map<Flock__c, Double>();
        // jesli sumarycznie jest mniej niz 1000 ptaków to zakoncz wstawienie 
        for(Thinning__c thin : Trigger.new)
        {
            
            for(Flock__c flock : flockList)
            {
                if(thin.Flock__c == flock.id)
                {
                    if(Trigger.isInsert)
                    {
                        // jesli insert odejmuje cala ilosc z ubiorki
                        if(flockMap.containsKey(flock))
                        {
                            flockMap.put(flock, flockMap.get(flock) - thin.Birds_thinned_at_farm__c);
                        }
                        else
                        {
                            System.debug('Ilosc ptakow : '+ (flock.Current_number_of_birds__c - thin.Birds_thinned_at_farm__c));
                            flockMap.put(flock, flock.Current_number_of_birds__c - thin.Birds_thinned_at_farm__c);
                        }
                    }
                    else if(Trigger.isUpdate && Trigger.oldmap.get(thin.id).Birds_thinned_at_farm__c != thin.Birds_thinned_at_farm__c)
                    {
                        // jesli zmieniona zostala ilosc ptakow wydanych z farmy -> aktualizuje tylko o roznice
                        double diff = thin.Birds_thinned_at_farm__c - Trigger.oldmap.get(thin.id).Birds_thinned_at_farm__c;
                        if(flockMap.containsKey(flock))
                        {
                            flockMap.put(flock, flockMap.get(flock) - diff);
                        }
                        else
                            flockMap.put(flock, flock.Current_number_of_birds__c - diff);
                    }
                    
                    // sprawdzam czy zamknac flocka
                    if(flockMap.containsKey(flock) && flockMap.get(flock)<1000)
                    {
                        System.debug('Zamykam wstawienie '+ flock.id);
                        flock.Date_of_removal__c = thin.Date__c;
                        flockListToUpdate.add(flock);
                    }
                } 
            }
            
        }
        
        System.debug('Liczba zamykanych wstawien '+ flockListToUpdate.size());
        // aktualizuje flock
        if(flockListToUpdate.size()>0)
        {
            update flockListToUpdate;
        }
    } 
    else
    {
        // 2. System sprawdza czy nalezy zaktualizowac szacowana ilosc ptakow
        // dla insertu i deleteu zawsze aktualizuje
        // dla update sprawdzam czy ilosc na farmie ulegla zmianie
        Set<Id> flockIdSet = new Set<Id>();
        
        if(Trigger.isInsert)
            for(Thinning__c thin : Trigger.new)
            {
                flockIdSet.add(thin.Flock__c);
            }
        else if(Trigger.isDelete)
            for(Thinning__c thin : Trigger.old)
            {
                flockIdSet.add(thin.Flock__c); 
            }
        else           
        {
            for(Thinning__c thin : Trigger.new)
            {
                if(thin.Birds_thinned_at_farm__c != Trigger.oldMap.get(thin.id).Birds_thinned_at_farm__c)
                    flockIdSet.add(thin.Flock__c);
            }
        }
        
        if(flockIdSet.size()==0)
        {
            system.debug('ThinningTrigger BRAK zmian ilosciowych - KONIEC');
            return;
        }
        FlockStatsEstimateNumberOfBirds flsHandler = new FlockStatsEstimateNumberOfBirds();
        flsHandler.estimateForFlocks(flockIdSet);   
    }


}