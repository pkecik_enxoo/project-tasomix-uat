import {api, LightningElement, track, wire} from 'lwc';
import {getFieldValue, getRecord, updateRecord} from 'lightning/uiRecordApi';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

import SPECIES_FIELD from '@salesforce/schema/Flock__c.Species__c';
import GENETIC_LINE_FIELD from '@salesforce/schema/Flock__c.Genetic_Line__c';
import TYPE_FIELD from '@salesforce/schema/Flock__c.Type__c';
import BIRDS_REMOVED from '@salesforce/schema/Flock__c.Total_birds_removed__c';

import turkeyApiName from '@salesforce/label/c.Turkey_api_name';
import fatteningApiName from '@salesforce/label/c.Fattening_api_name';
import fattening from '@salesforce/label/c.Fattening';
import rearingApiName from '@salesforce/label/c.Rearing_appi_name';
import save from '@salesforce/label/c.Save';
import errorOnSave from '@salesforce/label/c.Error_on_save';
import fatteningCreated from '@salesforce/label/c.Fattening_created';
import weightAtArrival from '@salesforce/label/c.Pieces_weight_at_arrival';
import recordId from '@salesforce/label/c.Record_Id';


const FIELDS = ['Flock__c.Species__c', 'Flock__c.Genetic_Line__c', 'Flock__c.Type__c', 'Flock__c.Current_number_of_birds__c', 'Flock__c.Total_birds_removed__c'];

export default class TransformFlock extends LightningElement {
    
    @api recordId;
    @api objectApiName;
    @track showSpinner = false;

    @track flock;
    @track disableTransformation = false;
    @track birdsNo;

    label = {
        turkeyApiName,
        fatteningApiName,
        fattening,
        rearingApiName,
        save,
        errorOnSave,
        fatteningCreated,
        weightAtArrival,
        recordId
    };

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wireFlock({data, error}) {
        if(data) {
            this.flock = data;
            console.log('this flock ', this.flock);
        }
    }

    get species() {
        console.log('getFieldValue(this.flock, SPECIES_FIELD)', getFieldValue(this.flock, SPECIES_FIELD));
        return getFieldValue(this.flock, SPECIES_FIELD);
        //return this.flock.fields.Species__c.value;
    }

    get geneticLine() {
        return getFieldValue(this.flock, GENETIC_LINE_FIELD);
    }
    
    get type() {
        console.log('getFieldValue(this.flock, TYPE_FIELD)', getFieldValue(this.flock, TYPE_FIELD));
        return getFieldValue(this.flock, TYPE_FIELD);
    }

    get removedBirdsNo() {
        return getFieldValue(this.flock, BIRDS_REMOVED);
    }

    handleOnLoad() {
        this.showSpinner = true;
        let speciesName = this.species;
        let typeName = this.type;
        console.log('speciesName ', speciesName);
        console.log('typeName ', typeName);
        if(speciesName !== this.label.turkeyApiName || typeName !== this.label.rearingApiName) this.disableTransformation = true;
    }

    getBirdsToRemove(event) {
        this.birdsNo = event.target.value;
    }

    prepareRearing() {
        const todayDate = new Date();
        const birdsSum = (this.removedBirdsNo) ? parseInt(this.removedBirdsNo, 10) + parseInt(this.birdsNo, 10) : parseInt(this.birdsNo, 10);
        return {
            fields: {
                Id: this.recordId,
                Date_of_removal__c: todayDate,
                Total_birds_removed__c: birdsSum
            },
        };
    }

    revertRearing() {
        const birdsSum = parseInt(this.removedBirdsNo, 10) - parseInt(this.birdsNo, 10);
        const date = (this.removedBirdsNo) ? new Date() : null;
        return {
            fields: {
                Id: this.recordId,
                Date_of_removal__c: date,
                Total_birds_removed__c: birdsSum
            },
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        updateRecord(this.prepareRearing())
        .then(() => {
            const fields = event.detail.fields;
            this.template.querySelector('lightning-record-edit-form').submit(fields);
        })
        .catch(error => {
            if(!error.body.output) updateRecord(this.revertRearing());
            const errorMessage = (error.body.output) ? error.body.output.errors[0].message : error.body.message;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: this.label.errorOnSave,
                    message: errorMessage,
                    variant: 'error',
                }),
            );
        });
    }

    handleSuccess(event) {
        const evt = new ShowToastEvent({
            title: this.label.fatteningCreated,
            message: this.label.recordId + event.detail.id,
            variant: "success"
        });
        this.dispatchEvent(evt);
    }

    handleError() {
        updateRecord(this.revertRearing());
    }
}