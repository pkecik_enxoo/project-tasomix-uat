({
    goToLink : function(component,event,helper)
    {
        console.log('Cell cliked!: '+event.target.id);
        var evVal = event.target.id;
        
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": evVal
        });
        navEvt.fire();
        
    },
    getCustomSortedFlocks : function(cmp, event, helper) {
        var orderByField = event.currentTarget.id;
        var order;
        if(orderByField == cmp.get("v.orderByField")){
        	helper.flipSortingOrder(cmp);
        } else {
        	cmp.set("v.orderByDirection","DESC");
        }
        cmp.find('flockTabSelect').getSortedFlocks(orderByField,cmp.get('v.orderByDirection'));
        cmp.set("v.orderByField",orderByField);
    },
})