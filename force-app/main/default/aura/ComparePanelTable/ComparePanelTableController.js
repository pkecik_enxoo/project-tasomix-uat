({
    getData: function(component, event, helper) {
        //set colors;
        var colors = component.get("v.defaultColors");
        if (colors) {
            colors = JSON.parse(JSON.stringify(colors));
        }

        var colors_pisklak = [{
            "bgc": "rgba(249,246,29,0.1)",
            "bdc": "rgba(249,246,29,1)"
        }];

        var colors_table = component.get("v.defaultColorsTable");
        if (colors_table) {
            colors_table = JSON.parse(JSON.stringify(colors_table));
        }


        //console.log('Test wywołania metody na change dla ComparePanelTable');
        var dataSeries = component.get("v.tableSeries");
        //var dataSeries = [{"param":null,"label":"Ferma Młodynie K9 2-2017","data":[{"y":1125,"x":1},{"y":1317,"x":2},{"y":1725,"x":3},{"y":2893,"x":4},{"y":2346,"x":5},{"y":2602,"x":6},{"y":2897,"x":7},{"y":3257,"x":8},{"y":3441,"x":9},{"y":3700,"x":10},{"y":4355,"x":11},{"y":4920,"x":12},{"y":5396,"x":13},{"y":5896,"x":14},{"y":6609,"x":15},{"y":7276,"x":16},{"y":7355,"x":17},{"y":8022,"x":18},{"y":8652,"x":19},{"y":9108,"x":20},{"y":9370,"x":21},{"y":10826,"x":22},{"y":9000,"x":23},{"y":9924,"x":24},{"y":11160,"x":25},{"y":9200,"x":26},{"y":9332,"x":27},{"y":10286,"x":28},{"y":11078,"x":29}]}];

        console.log('dataSeries');
        console.log(dataSeries);
        //console.log('dataseries ' + JSON.stringify(dataSeries));
        var dataList = [51];
        for (var j = 0; j < 51; j++) {

            dataList[j] = [dataSeries.length];
            for (var p = 0; p <= dataSeries.length; p++) {
                dataList[j][p] = {};
                if (p == 0 && j > 0) 
                {
                    dataList[j][p].value = j - 1;
                } else 
                {
                    dataList[j][p].value = '-';
                    dataList[j][p].styleBase = dataList[0][p].styleBase;
                }

                if (j == 0 && p == 0) {
                    dataList[j][p].value = 'Dzień';
                } else if (j == 0) {
                    var paramValue;
                    switch (dataSeries[p - 1].param) {
                        case 'waga':
                            paramValue = 'Waga[g]';
                            break;
                        case 'woda':
                            paramValue = 'Spożycie wody[ml]';
                            break;
                        case 'pasza':
                            paramValue = 'Spożycie paszy[g]';
                            break;
                        case 'upadki':
                            paramValue = 'Upadki dzienne';
                            break;
                        case 'woda_pasza':
                            paramValue = 'Woda/pasza';
                            break;
                    }
                    dataList[j][p].value = dataSeries[p - 1].label + '<br/>' + paramValue;
                    if (dataSeries[p - 1].geneticLine) {
                        dataList[j][p].value += ' / Rasa ' + dataSeries[p - 1].geneticLine;
                    }
                    
                    ////////// colour my colums
                    let label = dataList[j][p].value;
                    var color = '';
                    var borderType = '';
                    let bgColor = '';
                    dataList[j][p].styleBase = '';
	                if (label.indexOf(' pisklak:') > -1) {
	                    if (colors_pisklak.length > 0 && colors.length) {
	                        color = colors[colors.length - 1].bdc;
	                        bgColor = colors[colors.length - 1].bgc;
	                        borderType = ' dotted ';
	                    }
	                } else if (label.indexOf(' tabela ') > -1) {
	                    if (colors_table.length > 0) {
	                        color = colors_table[0].bdc;
	                        bgColor = colors_table[0].bgc;
	                        borderType = ' solid ';
	                    }
	                } else if (colors.length > 0) {
	                    color = colors[colors.length - 1].bdc;
	                    bgColor = colors[colors.length - 1].bgc;
	                    borderType = ' solid ';
	                    colors.pop();
	                }
	                let style = ' border-left:  3px '+ borderType + color +' ; border-right: 3px '+ borderType + color + ' ;';
	                
	                console.log(bgColor);
	                bgColor.a = 0.1;
	                dataList[j][p].styleBase = style;
	                dataList[j][p].styleBgColor = 'background-color:' + bgColor+';';
	                /////////
                }
                
                

            }
        }


        for (var i = 1; i <= dataSeries.length; i++) {
            //console.log('single dataseries ' + JSON.stringify(dataSeries[i]));
            var singleDataSeries = dataSeries[i - 1].data;
            for (var k = 0; k < singleDataSeries.length; k++) {
                // k+1 , żeby zrobić miejsce dla opisu na górze tabeli
                if (singleDataSeries[k].x || singleDataSeries[k].x == 0) {
                    //console.log('x and y: '+singleDataSeries[k].x +' '+singleDataSeries[k].y);
                    dataList[singleDataSeries[k].x + 1][i].value = singleDataSeries[k].y;
                } else {
                    if (dataList[k + 1]) {
                        dataList[k + 1][i].value = singleDataSeries[k];
                    }
                }
            }
        }
        component.set("v.dataList", dataList);
        console.log("dataList");
        console.log(dataList);
    }

})