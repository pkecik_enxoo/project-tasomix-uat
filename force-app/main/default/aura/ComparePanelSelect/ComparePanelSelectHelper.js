({  
    // pobieranie listy wartosci dla list_to_set_id, oraz ustawianie pola field_id
    doSearchObjects : function(component, action_name, param_name, param_id, list_to_set_id, field_id){
    	var param_val = null;
        if(param_id!=null)
			param_val = component.get(param_id);
        // ustawiam wartosc dla selectParams
        if(param_id!=null)
        {
             // var selectParams = component.get("v.selectParams");  
				//console.log(" selectParams " + selectParams );  
            //selectParams[param_name] = param_val;
              //component.set("v.selectParams", selectParams);  
        }
        
        //console.log("doSearchObjects dla " + param_name +": " + param_val);
        if(param_val != null || param_id==null){
            var action = component.get(action_name);
            action.setStorable({
                "ignoreExisting": "true"
            });
            if(param_name!=null)
                action.setParams({ 
                    [param_name] : param_val
                });
            var self = this;
            action.setCallback(this, function(response) {
            
            
                var data = response.getReturnValue();
                var state = response.getState();
                var errors = action.getError();
  				if (errors!= 0 && errors.length>0) {
    				console.log("error " + errors[0].message);
                }
                if(data != null){
                    //console.log("List " +  list_to_set_id+ ":"+ JSON.stringify(data));
                    component.set(list_to_set_id, data);
                    var idToSet = '';
                    // jesli ustawiono ze ma sie wybierac
                    var selectFirst =  component.get("v.selectFirst");
                    if(data.length>0 && selectFirst===true)
                    {
                        //console.log("ustawiam pierwszys listy "+field_id+": " + data[0].Id);
                        idToSet = data[0].Id; 
                    }
                    
                    //set defaults for the first run
                    idToSet = self.getDefaults(component, self, data, field_id, idToSet);
                    
                    // jesli jest to ostatnia lista i ustawiono 
                    if(selectFirst===false && list_to_set_id == "v.flockList")
                    {
                        component.set("v.selectFirst", true);
                        this.fireDrawChartEvent();
                    }
                    
                    if( list_to_set_id == "v.flockList" ){
                    	component.set("v.firstRun", false);
                    }
                    
                	component.set(field_id, idToSet); 
            	}           
            });
            $A.enqueueAction(action);
    	}
	},
    
    doSetObj : function (component, param_name, param_id)
    {
        var selectParams = component.get("v.selectParams");
        var tmpId = component.get(param_id);
        selectParams[param_name] = tmpId;
        //console.log("Zmiana na "+tmpId);
        component.set("v.selectParams", selectParams);
        //console.log("after change "+ JSON.stringify(selectParams));
    },
    
    fireDrawChartEvent : function()
    {
        console.log("Select change event fired");
        var appEvent = $A.get("e.c:ComparePanelEvent");
        // appEvent.setParams({
        //     "object" : object
        // });
        appEvent.fire();
    },
    
    getDefaults : function(component, helper, data, field_id, oldId) {
    	if(component.get("v.firstRun") ){
        	var selectParams = component.get("v.selectParams");
        	if (selectParams && field_id.length > 2 && selectParams[field_id.substring(2, field_id.length )] ){
        		let defaultId = selectParams[field_id.substring(2, field_id.length )];

				for(var i = 0; i < data.length; i++) {
				    if (data[i].Id == defaultId) {
				        return defaultId;
				    }
				}
        	}
        }
        return oldId;
    }
})