({
	initGeolocation : function(component) {
        if (navigator && navigator.geolocation) {
            var self = this;
            navigator.geolocation.getCurrentPosition( function(position){ self.successCallback(position, component) }, this.errorCallback);
        } else {
            console.log('Geolocation is not supported');
        }
	},
    
    successCallback : function(position, component) {
        
        var farm = component.get('v.farm');
        var mapUrl = component.get('v.mapUrl');
        mapUrl = 'https://maps.google.com/maps?q='+position.coords.latitude + ',' + position.coords.longitude+'&t=&z=13&ie=UTF8&iwloc=&output=embed';
        
        farm.Geolocation__Latitude__s = position.coords.latitude;
        farm.Geolocation__Longitude__s = position.coords.longitude;
        component.set('v.farm', farm);
        component.set('v.mapUrl', mapUrl);
        // position.coords.latitude  position.coords.longitude
        
	},
    
    errorCallback : function() {
        console.log('Error getting Geolocation');

	},
    
    showToast : function(type, title, message) {
        var toastEvent = $A.get('e.force:showToast');
        if(toastEvent) {
            toastEvent.setParams({
                'title': title,
                'message': message,
                'type': type,
                'mode': 'dismissible',
                'duration' : 10000
            });
            toastEvent.fire();
        } else {
            alert(message);
        }
        var closeAction = $A.get("e.force:closeQuickAction")
        if(closeAction){
            closeAction.fire();
        }
        
    },
})