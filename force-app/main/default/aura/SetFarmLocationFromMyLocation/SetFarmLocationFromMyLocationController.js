({
    doInit : function(component, event, helper) {
        helper.initGeolocation(component);
    },
    
    refreshLocation : function(component, event, helper) {
        helper.initGeolocation(component);
    },
    
    save : function(component, event, helper){
        var farm = component.get('v.farm');
        var farmId = component.get('v.recordId');
        farm.Id = farmId;
        var action = component.get("c.saveFarm");
        action.setParams({ 
            "farm": farm
        });
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                helper.showToast('success', '', 'Geolokacja została zapisana');
                $A.get('e.force:refreshView').fire();
                $A.get("e.force:closeQuickAction").fire();
                
            }else if (state === 'ERROR') {
                var errors = response.getError();
                console.log(errors);
                console.log(errors[0]);
                
                var  firstError = '';
                if(errors[0].fieldErrors.Id.length){
                    firstError = errors[0].fieldErrors.Id[0].message;
                } else if(errors[0].pageErrors.Id.length){
                    firstError = errors[0].pageErrors.Id[0].message;
                }
                
                helper.showToast('error','Wystąpił błąd przy zapisie ', firstError);
            }
        });
        
        $A.enqueueAction(action);
    }
})