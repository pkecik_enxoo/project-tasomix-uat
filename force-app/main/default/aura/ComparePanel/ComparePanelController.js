({    
	downloadChart : function(component, event, helper ){
		helper.setChartFileName(component);
		helper.downloadChart(component, event);
	},
	
	drawChart: function(component, event, helper) {
        // pozwala na ustawienie koloru tła
    	Chart.pluginService.register({
			beforeDraw: function (chart, easing) {
				if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
					var helpers = Chart.helpers;
					var ctx = chart.chart.ctx;
					var chartArea = chart.chartArea;

					ctx.save();
					ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
					ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
					ctx.restore();
				}
			}
		});
        
  		var ctx = component.find("chart").getElement();  
        var allSeries =  component.get("v.series"); 
    	var labelsArray = [];
		for (let i = 0; i < 50; i++) {
		    labelsArray.push(i.toString());
		}
		var chartType = component.get("v.chartType");
		
        window.myChart = new Chart(ctx, {
            type: chartType,
            data:  {
                datasets: [],
                labels: labelsArray
            },
            options: {
                /*scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        scaleLabel: {
                            display: true,
                            labelString: 'Dzień chowu'
                        },
                         ticks: {
                                beginAtZero: true
                            }
                    }],
                    yAxes: [ {  
                        		"id":"y-waga",
                              	"scaleLabel": {
                              		"display": true,
                              		"labelString": 'Waga [g]'
                    			},
                            	"ticks": {
                            		"beginAtZero": true
                            	}
                           	},
                            {
                            	"id":"y-woda",
                              	"scaleLabel": {
                              		"display": true,
                              		"labelString": 'Spożycie wody [ml]'
                    			},
                            	"ticks": {
                            		"beginAtZero": true
                            	},
                            	display: false
                           },
                            { 
                            	"id":"y-pasza",
                            	"scaleLabel": {
                              		"display": true,
                              		"labelString": 'Spożycie paszy [g]'
                    			},
                            	"ticks": {
                            		"beginAtZero": true
                            	},
                            	display: false
                            },
                                                        { 
                            	"id":"y-upadki",
                            	"scaleLabel": {
                              		"display": true,
                              		"labelString": 'Upadki dzienne'
                    			},
                            	"ticks": {
                            		"beginAtZero": true
                            	},
                            	display: false
                            },
                                                        { 
                            	"id":"y-woda_pasza",
                            	"scaleLabel": {
                              		"display": true,
                              		"labelString": 'Woda/pasza'
                    			},
                            	"ticks": {
                            		"beginAtZero": true,
                                    "max": 4
                            	},
                            	display: false
                            }
                           ]
                }*/
            }
		});	
		
	},
    
    updateChart: function(component, event, helper) {
    	helper.drawChart(component );

    	/*
		var chartType = component.get("v.chartType");
        if( window.myChart ) {
            var ctx = component.find("chart").getElement();  
            var allSeries =  component.get("v.series"); 
            console.log(allSeries);
            // sprawdzam jaka statystyka
            if(allSeries.length>0)
            {
                var param = allSeries[0].param;
			}
			window.myChart.data.datasets = helper.parseAllSeries( component, allSeries );
            var colors = [{ "bgc": "rgba(0, 0, 0,0.1)", "bdc": "rgba(0, 0, 0,1)"},
                          { "bgc": "rgba(138, 141, 142,0.1)", "bdc": "rgba(138, 141, 142,1)"},
                          { "bgc": "rgba(8, 127, 27,0.1)", "bdc": "rgba(58, 127, 27,1)"},
                          { "bgc": "rgba(127, 8, 70,0.1)", "bdc": "rgba(127, 8, 70,1)"},
                		  { "bgc": "rgba(255, 73, 251,0.1)", "bdc": "rgba(255, 73, 251,1)"},
                		  { "bgc": "rgba(249, 131, 29,0.1)", "bdc": "rgba(249, 131, 29,1)"},
                  		  { "bgc": "rgba(39, 29, 249,0.1)", "bdc": "rgba(39, 29, 249,1)"},
                		  { "bgc": "rgba(172, 29, 249,0.1)", "bdc": "rgba(172, 29, 249,1)"},
                          { "bgc": "rgba(53, 224, 255,0.1)", "bdc": "rgba(53, 224, 255,1)"},
                          { "bgc": "rgba(43, 249, 29,0.1)", "bdc": "rgba(43, 249, 29,1)"}
                          ];
           
            var colors_pisklak = [{ "bgc": "rgba(249,246,29,0.1)", "bdc": "rgba(249,246,29,1)"}];
            
            var colors_table=[{ "bgc": "rgba(239,9,9,0.1)", "bdc": "rgba(239,9,9,1)"}]; 
            
            
            // sprawdzam ile jest serii i jakiego typu w zależnosci od tego ustalam kolory
            // sprawdzamjakie sa osie y i ile ich jest 
            var yAxisID = [];
            for(var i=0; i<allSeries.length;i++)
            {
                if(allSeries[i].yAxisID!=undefined)
                    yAxisID.push(allSeries[i].yAxisID);
                    
                var label = allSeries[i].label;
             
                if(label.indexOf(' pisklak:')>-1)
                {
                    if(colors_pisklak.length>0)
                    {
                    	window.myChart.data.datasets[i].backgroundColor = colors_pisklak[0].bgc;
                    	window.myChart.data.datasets[i].borderColor = colors_pisklak[0].bdc;    
                    }
                    //window.myChart.data.datasets[i].borderWidth  = 1;
                    window.myChart.data.datasets[i].pointRadius  = 1;
                    window.myChart.data.datasets[i].pointBorderWidth = 1;
                    window.myChart.data.datasets[i].pointBackgroundColor = "#fff";
 					window.myChart.data.datasets[i].pointHoverBorderWidth = 2;
                    //window.myChart.data.datasets[i].fill = false;                             
                }
                else if(label.indexOf(' tabela ')>-1)
                {
                    if(colors_table.length>0)
                    {
                    	window.myChart.data.datasets[i].backgroundColor = colors_table[0].bgc;
                    	window.myChart.data.datasets[i].borderColor = colors_table[0].bdc;
                    }
                   // window.myChart.data.datasets[i].borderWidth  = 1;
                    window.myChart.data.datasets[i].pointRadius  = 1;
                    window.myChart.data.datasets[i].pointBorderWidth = 1;
                    window.myChart.data.datasets[i].pointBackgroundColor = "#fff";
 					window.myChart.data.datasets[i].pointHoverBorderWidth = 2;
                    //window.myChart.data.datasets[i].fill = false;                   
                }
                else if(colors.length>0)
                {
                    //console.log('ustawiam kolor ' + colors[colors.length-1].bgc);
                    window.myChart.data.datasets[i].backgroundColor = colors[colors.length-1].bgc;
                    window.myChart.data.datasets[i].borderColor = colors[colors.length-1].bdc;
                    
                    //window.myChart.data.datasets[i].pointRadius  = 1;
                    //window.myChart.data.datasets[i].pointBorderWidth = 1;
                    //window.myChart.data.datasets[i].borderWidth  = 1;
                    window.myChart.data.datasets[i].pointBackgroundColor = "#fff";
 					window.myChart.data.datasets[i].pointHoverBorderWidth = 2;
                    window.myChart.data.datasets[i].fill = false;
                	colors.pop();
                }
            }
            // ustawiam osie y
            var isRight = 0;
        	for(var i=0; i<window.myChart.options.scales.yAxes.length; i++)
            {	
                //console.log('zmieniam os y ' + window.myChart.options.scales.yAxes[i].id);
                window.myChart.options.scales.yAxes[i].display = false;
                
                for(var j=0;j<yAxisID.length;j++)
                {
                    if(yAxisID[j]==window.myChart.options.scales.yAxes[i].id &&  window.myChart.options.scales.yAxes[i].display==false )
                    {
                        window.myChart.options.scales.yAxes[i].display =true;
                        window.myChart.options.scales.yAxes[i].position = (isRight==1?'right':'left');
                        isRight=1-isRight;
                    }
                }
            }
        
           
            //console.log('Data series: '+JSON.stringify(allSeries));
            window.myChart.update();
            console.log(window.myChart);
        }
        else
        {
            alert('Błąd podczas ładowania danych');
        }
    */
        
    }
})