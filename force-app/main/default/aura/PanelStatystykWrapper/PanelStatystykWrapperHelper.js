({  
    // pobieranie listy wartosci dla list_to_set_id, oraz ustawianie pola field_id
    doSearchObjects : function(component, action_name, param_name, param_id, list_to_set_id, field_id){

    	var param_val = null;
        if(param_id!=null)
			param_val = component.get(param_id);

        //console.log("doSearchObjects dla " + param_name +": " + param_val);
        if(param_val != null || param_id==null){
            var action = component.get(action_name);
            action.setStorable({
                "ignoreExisting": "true"
            });
            if(param_name!=null)
                action.setParams({ 
                    [param_name] : param_val
                });
            action.setCallback(this, function(response) {
                var data = response.getReturnValue();
                var state = response.getState();
                var errors = action.getError();
  				if (errors!= 0 && errors.length>0) {
    				console.log("error " + errors[0].message);
                }
                if(data != null){
                    //console.log("List " +  list_to_set_id+ ":"+ JSON.stringify(data));
                    component.set(list_to_set_id, data);
                    //component.set("v.flsListOld", data);
                  
                    if(data.length>0 && field_id!=undefined)
                    {
                        //console.log("ustawiam "+field_id+": " + data[0].Id);
                    	component.set(field_id, data[0].Id);  
                    }
                    
                    if(data.length==0 && list_to_set_id == 'v.flockList')
                    {
                        console.log("czyszcze liste");
                        var emptyList =[];
                        component.set("v.flockStatsList",emptyList);
                    }
                }
                if( list_to_set_id == "v.flockStatsList" || data==null || data.length==0)
                {
                    component.set("v.loadingData", "false");
                    var spinner = component.find('mySpinner');   
                    var cmpClassNew = spinner.get('v.class')
                    if(cmpClassNew != 'slds-hide')
                    {
                        $A.util.addClass(spinner, "slds-hide");
                    }
                }

            });
            $A.enqueueAction(action);
    	}

	},
    
    doSetObj : function (component, param_name, param_id)
    {
        var selectParams = component.get("v.selectParams");
        var tmpId = component.get(param_id);
        selectParams[param_name] = tmpId;
        //console.log("Zmiana na "+tmpId);
        component.set("v.selectParams", selectParams);
        //console.log("after change "+ JSON.stringify(selectParams));
        //
    },
    
    doGetCodes: function(component, action_name, list) {
         var action = component.get(action_name);

        action.setStorable({
            "ignoreExisting": "true"
        });

        action.setCallback(this, function(response) {
            var jsonData = response.getReturnValue();
            var data = JSON.parse(jsonData);
            var state = response.getState();
            var errors = action.getError();
            if (errors!= 0 && errors.length>0) {
                console.log("error " + errors[0].message);
            }
            if(data != null){
                //console.log("data format" + JSON.stringify(data));
                var emptyVal = [{label:"", value:""}];
              	var allData = emptyVal.concat(data);
                component.set(list, allData);
            }           
        });
        $A.enqueueAction(action);
    },

	showSomeToast : function (title, message,type, duration){
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"title" : title,
			"message": message,
			"type" : type,
			"duration" : duration
		});
		toastEvent.fire();
	},
    
    doGetQfgView: function(component, action_name, list) {
        var action = component.get("c.getQfgView");
        action.setCallback(this, function(response) {
            var data = response.getReturnValue();
            var state = response.getState();
            var errors = action.getError();
            console.log(data);
            if (errors!= 0 && errors.length>0) {
                console.log("error " + errors[0].message);
            }
            if(data != null){
                component.set("v.qfgView", data);
            }           
        });
        $A.enqueueAction(action);
    }
})