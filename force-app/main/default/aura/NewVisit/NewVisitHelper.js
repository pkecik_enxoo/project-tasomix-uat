({  
    // pobieranie listy wartosci dla list_to_set_id, oraz ustawianie pola field_id
    doSearchObjects : function(component, action_name, param_name, param_id, list_to_set_id, field_id){
        
        var action = component.get(action_name);
        action.setStorable({
            "ignoreExisting": "true"
        });
        
        if(param_name!=null)
            action.setParams({ 
                [param_name] : component.get(param_id)
            });
        
        action.setCallback(this, function(response) {
            var data = response.getReturnValue();
            var state = response.getState();
            var errors = action.getError();
            if (errors!= 0 && errors.length>0) {
                console.log("error " + errors[0].message);
            }
            if(data != null){
                console.log("List " +  list_to_set_id+ ":"+ JSON.stringify(data));
                component.set(list_to_set_id, data);
                if(field_id && data.length>0)
                {
                	component.set(field_id, data[0].Id);  
                }
            }           
        });
        $A.enqueueAction(action);
        
    },
    
    saveVisit : function(component) {
        
        var action = component.get("c.saveVisitApex");
        
        var houses = component.get("v.houses");
        
        for (var i = houses.length -1; i >=0; --i){
            if(! houses[i].selected ){
                houses.splice(i,1);
            }
        }
        
        action.setParams(
            {
                farmId: component.get('v.selectedFarmId'),
                visit: component.get('v.visit'),
                selectedHouses: houses,
                postBody: component.get('v.note'),
            }
        );        
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                this.showToast('success', '', 'Wizyta została zapisana');
                $A.get('e.force:refreshView').fire();
                $A.get("e.force:closeQuickAction").fire();

          }else if (state === 'ERROR') {
              var errors = response.getError();
              console.log(errors);
              console.log(errors[0]);
              console.log(errors[0].message );
              var  firstError = (errors && errors[0] && 
              errors[0].pageErrors && errors[0].pageErrors[0].message) 
              ?  errors[0].pageErrors[0].message : 'Unknown error';
              this.showError('Wystąpił błąd przy zapisie ', firstError);
          }
       });
                
        $A.enqueueAction(action);  
        
    },
    
    selectAllHouses : function(component, valueToSet) {
        var houses = component.get("v.houses");
        for (var i = houses.length -1; i >=0; --i){
           houses[i].selected = valueToSet;
        } 
        component.set("v.houses",houses);
    },
    
    showToast : function(type, title, message) {
        var toastEvent = $A.get('e.force:showToast');
        if(toastEvent) {
            toastEvent.setParams({
                'title': title,
                'message': message,
                'type': type,
                'mode': 'dismissible',
                'duration' : 10000
            });
            toastEvent.fire();
        } else {
            alert(message);
        }
    },
    
    showError : function(title, message) {
        $A.get("e.force:closeQuickAction").fire();
        this.showToast('error', title, message);
    }
})