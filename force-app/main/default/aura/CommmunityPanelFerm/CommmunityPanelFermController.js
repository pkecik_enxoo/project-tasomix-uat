({
   doInit : function(component, event, helper) {
      $A.createComponent(
         "c:PanelFermWrapper",
         {
 
         },
         function(newCmp){
            if (component.isValid()) {
               component.set("v.body", newCmp);
            }
         }
      );
   },
    
   NavigateComponent : function(component,event,helper) {
      $A.createComponent(
         "c:ComparePanelWrapper",
         {
            "newSelectParamsList": event.getParam("newSelectParamsList"),
            "selectFirst": event.getParam("selectFirst")
         },
         function(newCmp){
            if (component.isValid()) {
                component.set("v.body", newCmp);
            }
         }
      );
   }
})