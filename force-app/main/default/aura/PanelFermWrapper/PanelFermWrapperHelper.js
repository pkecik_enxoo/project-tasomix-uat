({
    setSelectedValue : function(component, event, acc)
    {
        //console.log("Helper method test if account was passed: "+acc);
        component.set("v.selectedValue", acc);
    },
    
    doSearchFarms : function(component, event, accountId)
    {
        //console.log("doSearchFarms WYWOŁANE z helpera!");
        
        var nullChecker = accountId;
        //console.log("accId : "+accountId);
        if(nullChecker != null)
        {
            var action = component.get("c.getFarms");
            
            action.setStorable({
                "ignoreExisting": "true"
            });
            
            action.setParams({ 
                "accId" : accountId
            })
            
            action.setCallback(this, function(response) 
            {
                var data = response.getReturnValue();
                var state = response.getState();
                var errors = action.getError();
                
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        cmp.set("v.message", errors[0].message);
                    }
                }
                
                if(!data)
                {
                    //console.log("List size: " + JSON.stringify(data));
                    //console.log("Failed with state: " + state);
                    component.set("v.farmList", data);
                    
                    this.setSelectedValue(component,event,data[0].Id);
                }
            });
            $A.enqueueAction(action);
        }
    },
    
})