({  
    // pobieranie listy wartosci dla list_to_set_id, oraz ustawianie pola field_id
    doSearchObjects : function(component, action_name, param_name, param_id, list_to_set_id, field_id){
    	var param_val = null;
        if(param_id!=null)
			param_val = component.get(param_id);
        // ustawiam wartosc dla selectParams
        if(param_id!=null)
        {
             // var selectParams = component.get("v.selectParams");  
				//console.log(" selectParams " + selectParams );  
            //selectParams[param_name] = param_val;
              //component.set("v.selectParams", selectParams);  
        }
        
        //console.log("doSearchObjects dla " + param_name +": " + param_val);
        if(param_val != null || param_id==null){
            var action = component.get(action_name);
            action.setStorable({
                "ignoreExisting": "true"
            });
            if(param_name!=null)
                action.setParams({ 
                    [param_name] : param_val
                });
            action.setCallback(this, function(response) {
                var data = response.getReturnValue();
                var state = response.getState();
                var errors = action.getError();
  				if (errors!= 0 && errors.length>0) {
    				console.log("error " + errors[0].message);
                }
                if(data != null){
                    console.log("List " +  list_to_set_id+ ":"+ JSON.stringify(data));
                    component.set(list_to_set_id, data);
                  
                    // jesli ustawiono ze ma sie wybierac
                    var selectFirst =  component.get("v.selectFirst");
                    if(data.length>0 && selectFirst===true)
                    {
                        //console.log("ustawiam pierwszy z listy "+field_id+": " + data[0].Id);
                    	component.set(field_id, data[0].Id);  
                    }
            	}           
            });
            $A.enqueueAction(action);
    	}
	},
    
    doSetObj : function (component, param_name, param_id)
    {
        var selectParams = component.get("v.selectParams");
        var tmpId = component.get(param_id);
        selectParams[param_name] = tmpId;
        //console.log("Zmiana na "+tmpId);
        component.set("v.selectParams", selectParams);
        //console.log("after change "+ JSON.stringify(selectParams));
    },
    
    doGetHouses : function(component){
        var action = component.get("c.getHouses");
        action.setStorable({
            "ignoreExisting": "true"
        });
        
        action.setParams({ 
            "farmId" :  component.get("v.farmId"),
        });
        action.setCallback(this, function(response) {
            var data = response.getReturnValue();
            var state = response.getState();
            var errors = action.getError();
            if (errors!= 0 && errors.length>0) {
                console.log("error " + errors[0].message);
            }
            if(data != null){
                console.log("List " +  "v.houseList"+ ":"+ JSON.stringify(data));
                // dodaje pozycje wszytskie
                var firstPos = {"House_Name__c": "- Wszystkie -", Id: "all" };
                var allData = [];
                allData.push(firstPos);
                allData = allData.concat(data);
                component.set("v.houseList", allData);
                
                // jesli ustawiono ze ma sie wybierac
                var selectFirst =  component.get("v.selectFirst");
                if(allData.length>0 && selectFirst===true)
                {
                    console.log("ustawiam pierwszy z listy : " + allData[0].Id);
                    component.set("v.houseId", allData[0].Id);  
                }
            }           
        });
        $A.enqueueAction(action);	
	},
    
    doGetFlocks : function(component, orderByField, orderByDirection)
    {       
        console.log("Szukam wstawień ");
        var action = component.get("c.getFlocks");
        action.setStorable({
            "ignoreExisting": "true"
        });
        
        action.setParams({ 
            "houseId" : component.get("v.houseId"),
            "showActive" : component.get("v.showActive"),
            "showClosed" : component.get("v.showClosed"),
            "farmId" : component.get("v.farmId"),
            "hatcheryName" : component.get("v.selectedHatchery"),
            "orderByField" : orderByField,
            "orderByDirection" : orderByDirection,
            "showRearing" : component.get("v.showRearing"),
            "showFattening" : component.get("v.showFattening"),
            "showBroiler" : component.get("v.showBroiler")
        });
        action.setCallback(this, function(response) {
            var data = response.getReturnValue();
            var state = response.getState();
            var errors = action.getError();
            if (errors!= 0 && errors.length>0) {
                console.log("error " + errors[0].message);
            }
            if(data != null){
                component.set("v.flockList", data);
                component.set("v.orderByField",orderByField);
                if(!orderByField){
                    component.set("v.orderByDirection","ASC");
                }
            }           
        });
        $A.enqueueAction(action);
    },
    showToast : function(type, title, message) {
        var toastEvent = $A.get('e.force:showToast');
        if(toastEvent) {
            toastEvent.setParams({
                'title': title,
                'message': message,
                'type': type
            });
            toastEvent.fire();
        } else {
            alert(message);
        }
    },
    
    doGetHatchery: function(component) {
        var action = component.get("c.getCodePicklist");

        action.setStorable({
            "ignoreExisting": "true"
        });
        
        action.setParams({ 
            "objName" : "Flock__c",
            "fieldName" : "Hatchery__c"
        });

        action.setCallback(this, function(response) {
            var jsonData = response.getReturnValue();
            var data = JSON.parse(jsonData);
            var state = response.getState();
            var errors = action.getError();
            if (errors!= 0 && errors.length>0) {
                console.log("error " + errors[0].message);
            }
            if(data != null){
                //console.log("data format" + JSON.stringify(data));
                var emptyVal = [{label:"- Wszystkie -", value:""}];
              	var allData = emptyVal.concat(data);
                component.set("v.hatcheryList", allData);
            }           
        });
        $A.enqueueAction(action);
    },
})