<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateBirdNumber</fullName>
        <field>Estimated_number_of_birds__c</field>
        <formula>Flock__r.Current_number_of_birds__c</formula>
        <name>UpdateBirdNumber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FlockStatsUpdateBirdsNo</fullName>
        <actions>
            <name>UpdateBirdNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Flock_stats__c.Day__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Aktualizacja, aktualnej liczby ptaków (na dany dzień)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
