<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Close_Flock</fullName>
        <description>Ustawia datę usunięcia z fermy na dzisiaj</description>
        <field>Date_of_removal__c</field>
        <formula>TODAY()</formula>
        <name>Close Flock</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStatus</fullName>
        <field>StatusHidden__c</field>
        <formula>Status__c</formula>
        <name>UpdateStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CloseFlockManualValues</fullName>
        <actions>
            <name>Close_Flock</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Zamyka rzut po wpisaniu ręcznych wartości statystyk:
 średnia waga, upadki (%), średni wiek, FCR</description>
        <formula>AND(
  OR( NOT(ISBLANK(Avg_age_manually__c) ),   
    NOT(ISBLANK(Avg_body_weight_manually__c) ),   
    NOT(ISBLANK(FCR_manually__c) ), 
    NOT(ISBLANK(Total_dead_percent_manually__c) ) ),
  ISBLANK( Date_of_removal__c )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FlockUpdateHiddenStatus</fullName>
        <actions>
            <name>UpdateStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Flock__c.Status__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>n	Aktualizacja ukrytego statusu na potrzeby rollupa, upewniającego się że istnieje tylko 1 aktywny flock na danym kurniku</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
