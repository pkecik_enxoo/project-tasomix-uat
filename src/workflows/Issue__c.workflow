<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Powiadom_Technolog_w</fullName>
        <description>Powiadom Technologów</description>
        <protected>false</protected>
        <recipients>
            <recipient>Salesforce_Technolog</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>kasia@tasomix.pl</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mj_admin@tasomix.pl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Powiadomienie_o_Reklamacji</template>
    </alerts>
    <rules>
        <fullName>Powiadomienie o Reklamacji</fullName>
        <actions>
            <name>Powiadom_Technolog_w</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Issue__c.Type__c</field>
            <operation>equals</operation>
            <value>Complaint</value>
        </criteriaItems>
        <description>Kiedy do systemu zostaje dodany nowy rekord Zgłoszenie o typie Reklamacja zostaje wysłany mail do wszystkich użytkowników z rolą Technolog</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
