<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateDayHiddednAction</fullName>
        <field>Day_Hidden__c</field>
        <formula>Day__c</formula>
        <name>UpdateDayHiddednAction</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ThinningUpdateDayHidden</fullName>
        <actions>
            <name>UpdateDayHiddednAction</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Thinning__c.Day__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Aktualizacja pomocniczego pola Day Hidden, gdy pole Day ulegnie zmianie</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
