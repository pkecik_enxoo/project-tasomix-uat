public without sharing class GPSFarmController {
    public Farm__c farm { get; set; }
    public final Decimal lat  { get; set; }
    public final Decimal lng  { get; set; }
    public Decimal latAddress  { get; set; }
    public Decimal lngAddress  { get; set; }
    public String googleApiKey  { get{return GeolocationHelper.getGoogleApiKey();} }
    public Boolean disableSaveButton  { get; set; }
    
    public GPSFarmController(ApexPages.StandardController stdController) {
        this.farm = (Farm__c)stdController.getRecord();
        lat = farm.Geolocation__Latitude__s;
        lng = farm.Geolocation__Longitude__s;
        disableSaveButton = true;
        
        
        if(!Test.isRunningTest()){
            getLocationBasedOnAddress();
        }
    }
    @testVisible
    private void getLocationBasedOnAddress() {
        if(Test.isRunningTest()){
            farm=[select id, city__c, Postal_Code__c, Street__c from farm__c where id=:farm.id  ];
        }
        GeolocationHelper.ApiResponse resp =  GeolocationHelper.getGeolocationForAddress(farm.city__c + ' ' + farm.Postal_Code__c + ' ' + farm.Street__c);
        if(resp.status == GeolocationHelper.StatusCode.OK){
        	latAddress = resp.locations[0].lat;
        	lngAddress = resp.locations[0].lng;
        }
    }
    
    public pagereference saveLocation(){
        system.debug('farmId '+farm.Id);
        system.debug('lat '+farm.Geolocation__Latitude__s);
        system.debug('lng '+farm.Geolocation__Longitude__s);
        updateLocationRemote( farm.Id, String.valueOf( farm.Geolocation__Latitude__s ), String.valueOf( farm.Geolocation__Longitude__s ) );
        return  new PageReference('/'+farm.Id);
    }
    
    public pagereference cancel(){
        return  new PageReference('/'+farm.Id);
    }
    
    @RemoteAction
    public static void updateLocationRemote(String farmId, String lat, String lng){
        system.debug('farmId '+farmId);
        system.debug('lat '+lat);
        system.debug('lng '+lng);
        if(lat <> null && lng <> null && farmId <> null){
            Farm__c farm = new Farm__c(id = farmId);
            farm.Geolocation__Latitude__s = Decimal.valueOf( lat );
            farm.Geolocation__Longitude__s = Decimal.valueOf( lng );
            update farm;
        } 
    }
    
    public void updateLocation(){
        String disableSaveButtonValue = Apexpages.currentPage().getParameters().get('disableSaveButton');
        if(disableSaveButtonValue <> null){
        	disableSaveButton = Boolean.valueOf(disableSaveButtonValue);
        }
        
        String lat = Apexpages.currentPage().getParameters().get('latitude');
        String lng = Apexpages.currentPage().getParameters().get('longitude');
        if(lat <> null && lng <> null){
            farm.Geolocation__Latitude__s = Decimal.valueOf( lat );
            farm.Geolocation__Longitude__s = Decimal.valueOf( lng );
        }
        system.debug(farm);
        
    }
}