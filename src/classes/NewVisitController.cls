public class NewVisitController {
	
    @auraEnabled
    public static List<House__c> getHouses(string farmId){
        return ClientUtils.getHouses( farmId );
    }	
    @auraEnabled
    public static List<Farm__c> getFarms(string accountId){
        return ClientUtils.getFarms( accountId );
    }	
    @auraEnabled
    public static String getObjectName(id objId){
        return objId.getSObjectType().getDescribe().getName();
    }
    // zapisanie wizyty
    @auraEnabled
    public static String saveVisitApex (string farmId, Visit__c visit, List<House__c> selectedHouses, String postBody ) {
        
        List<id> houseIds = new List<id>();
        // z kazdego podanego kurnika pobierane jest Id, które następnie jest dodane do list<Id>
        for(House__c house: selectedHouses){
            houseIds.add(house.id);
        }
        // pobieranie aktywnych wstawień z kurników z listy powyżej
        List<Flock__c> activeFlocks = [SELECT id, House__c FROM Flock__c WHERE House__c in:houseIds AND Status__c = 'Active' ORDER BY CreatedDate ASC ];
		// mapa kurnik, wstawienia      
        Map<id, Flock__c> houseFlockMap = new Map<id, Flock__c>();
        for(Flock__c flock: activeFlocks){
            houseFlockMap.put(flock.House__c, flock);
        }
        
        
        visit.Date__c = System.today();
        visit.Start_Date__c = System.now();
        visit.Farm__c = farmId;
        
        if(selectedHouses.size()>1){
        	visit.Remarks__c = 'Wizyta w kurnikach ';
        } else if(selectedHouses.size() == 1) {
        	visit.Remarks__c = 'Wizyta w kurniku ';
        }
        for(Integer i = 0 ; i<selectedHouses.size() ; i++ ){
            visit.Remarks__c += selectedHouses[i].House_Name__c;
            if(i <> selectedHouses.size()-1 ){
                visit.Remarks__c += ', ';
            }
        }
        
        insert visit;
        
        List<House_Visit__c> houseVisits = new List<House_Visit__c>();
        for(House__c house: selectedHouses){
            id flockId = houseFlockMap.containsKey(house.id) ? houseFlockMap.get(house.id).id : null;
            houseVisits.add(new House_Visit__c(House__c = house.id, Visit__c = visit.id, Flock__c = flockId) );
        }
        
        insert houseVisits;
        // create post if provided
        if(String.isNotBlank(postBody)){
            FeedItem post = new FeedItem();
            post.ParentId = visit.id; 
            post.Body = postBody;
            insert post;
        }
        return 'SUCCESS';
    }
}