public with sharing class ClientUtils {
    
    /* @author Jarosław Kołodziejczyk 2017-04-04
     * 1. Metoda getAccount pobiera konta dostępne dla danego użytkownika (dostęp określony poprzez profile,sharing sety i rule)
     * 2. Pobiera wszystkie konta które przypisane są do jakichkolwiek ferm przy pomocy pól Manager__r.AccountId lub Farm_Owner_Account 
     * [Uwaga Marii] - Można by wygenerować listę wszystkich ferm, a następnie SOQL'em pobrać accounty przy pomocy in: (prawdopodobnie trzeba by wygenerować listę id iterując po liście ferm)
     */
    

    @AuraEnabled
    public static Farm__c[] getFarms(string accId){
        // Lista pobierająca fermy dla konta które jest określone jako farm owner account lub master record kontaktu "Manager"
        if(string.isBlank(accId)){
            return null;
        }
        List<Farm__c> farmList = new List<Farm__c>([SELECT id,Name from Farm__c where Manager__r.AccountId =: accId or Farm_Owner_Account__c =: accId]);
        return farmList;

    }

    @AuraEnabled
    public static Account[] getAccount()
    {
        string tasoName = 'Tasomix';
        id id1 = userinfo.getProfileId();
        List<Account> acc;
        List<String> accData = new List<String>();
        string profileName = [select Name from profile where id = :id1].Name;
        
        try
        {
            acc = [Select id,Name from Account];
        }
        catch(Exception e)
        {
            system.debug('PanelFermWrapperController.getAccount : Nie istnieje żaden account o podanych wartościach!');
        }
        
        List<Farm__c> farms = [select id,Manager__r.AccountId,Farm_Owner_Account__r.id from Farm__c where Manager__r.AccountId in: acc or Farm_Owner_Account__c in: acc order by Manager__r.AccountId Desc];
        
        //Sprawdzam jakie rekordy widzi użytkownik otwierający panel
        system.debug('PanelFermWrapperController. ROZMIAR LISTY acc:'+acc.size());
        system.debug('PanelFermWrapperController. ROZMIAR LISTY farm:'+farms.size());
        integer i=0;
        for(Farm__c f : farms)
        {
            system.debug('Iteration: '+ i);
            i = i +1;
            if(f.Manager__r.AccountId != null)
            {
                // Sprawdzam czy metoda przeszła powyższego ifa
                system.debug('f.Manager__r.AccountId NOT null');
                
                if(f.Farm_Owner_Account__r.id != null)  
                {
                    // Sprawdzam czy metoda przeszła powyższego ifa
                    system.debug('f.Farm_Owner_Account__r NOT null');   
                    
                    if(f.Manager__r.AccountId == f.Farm_Owner_Account__r.id)
                    {
                        // Jeśli konta są takie same do listy kont przekazywanych do kontrolera JS przekazuję tylko 1 z nich
                        system.debug('f.Farm_Owner_Account__r.id: '+f.Farm_Owner_Account__r.id);
                        accData.add(f.Farm_Owner_Account__r.id);
                    }
                    else if(f.Manager__r.AccountId != f.Farm_Owner_Account__r.id)
                    {
                        // Jeśli konta się różnią, to przekazuję oba
                        system.debug('f.Manager__r.AccountId: '+f.Manager__r.AccountId);
                        system.debug('f.Farm_Owner_Account__r.id: '+f.Farm_Owner_Account__r.id);
                        accData.add(f.Farm_Owner_Account__r.id);
                        accData.add(f.Manager__r.AccountId);
                    }
                }
                else
                {
                    // Jeśli Farm_Owner_Account__r.id jest puste przekazuję tylko Manager__r.AccountId
                    accData.add(f.Manager__r.AccountId);
                }
            }
            else if(f.Farm_Owner_Account__r.id != null)
            {
                // Sprawdzam czy przeszedłem powyższego ifa oraz przypisuę Farm_Owner_Account__r.id, ponieważ wyższy if określił, że Manager__r.AccountId jest pusty
                system.debug('f.Farm_Owner_Account__r NOT null');
                system.debug('f.Farm_Owner_Account__r.id: '+f.Farm_Owner_Account__r.id);
                accData.add(f.Farm_Owner_Account__r.id);
            }
            
        }
        
        // Sprawdzam rozmiar wygenerowanej listy
        system.debug('acc LIST SIZE!! panel ferm wrapper:'+accData);
        acc = [select id,name from Account where id in: accData order by name];
        
        system.debug('acc LIST SIZE!! panel ferm wrapper:'+acc.size());
        
        if(acc != null)
        {
            return acc;
        }    
        return null;
    }
    
    @AuraEnabled
    public static House__c[] getHouses(string farmId){
        System.debug('Wyszukuje kurniki dla fermy: ' + farmId);
        List<House__c> options = new List<House__c>();
        if(farmId!= null && farmId != '')
        {
            List<House__c> houseList = [Select id,name, House_Name__c  from  House__c where Farm__c=:farmId ORDER BY House_Name__c asc ];// SortOrderNumber__c,
            houseList = NaturalSort.sortHousesByName(houseList);
            for(House__c house : houseList)
            {
                if(house.House_Name__c != null)
                    options.add(house);    	
            }
        }
        return options;
    }
    
    @AuraEnabled
    public static Flock__c[] getFlocks(string houseId){
        System.debug('Wyszukuje wstawienia dla kurnika: ' + houseId);
        List<Flock__c> options = new List<Flock__c>();
        if(houseId!= null && houseId != '')
        {
            List<Flock__c> flockList = [Select id, Flock_number__c from Flock__c where House__c=:houseId 
                                        order by Date_of_arrival__c  DESC];
            for(Flock__c flock : flockList)
            {
                if(flock.Flock_number__c != null)
                    options.add(flock);    	
            }
        }
        
        return options;
    }
    
}