/**
* @author Kacper Augustyniak 2017-11-03
* Class helpind dealing with Visits
*/
public class VisitHelper { 
public class VisitHelperException extends Exception {}

    private Flock__c[] activeFlocks;
    private Map<id, Farm__c> farmsMap;
    private Map<id, House__c> housesMap;
    private Map<Id, Integer> activeFlockPerAccount;
    public VisitHelper(){
    } 
    public VisitHelper(set <id> accIds, Set <id> farmIds, Set <id> houseIds, set <id> flockId){
        this(new List<id>(accIds), new List <id>(farmIds), new List<id>(houseIds), new List <id>(flockId) );
    }    
    public VisitHelper(List <id> accIds, List <id> farmIds, List <id> houseIds, List <id> flockIds){
        // get all active flocks for accounts
        this.activeFlocks = [SELECT name, 
                             House__c, House__r.Id, House__r.Name, House__r.House_Name__c, 
                             House__r.Farm__c, House__r.Farm__r.Id, House__r.Farm__r.Name,
                             House__r.Farm__r.Farm_Owner_Account__c, House__r.Farm__r.Farm_Owner_Account__r.id,
                             House__r.Farm__r.Farm_Owner_Account__r.name,
                             id FROM flock__c WHERE // Status__c = 'Active' AND 
                             ( House__r.Farm__r.Farm_Owner_Account__c in: accIds OR id in:flockIds OR House__c in: houseIds OR House__r.Farm__c IN: farmIds)];
        this.activeFlockPerAccount = new Map<Id, Integer>();
        
        this.farmsMap = new Map<id, Farm__c>( [SELECT name, Farm_Owner_Account__c, id from FARM__C WHERE id in: farmIds] );
        this.housesMap = new Map<id, House__c>( [SELECT name, Farm__c, Farm__r.Farm_Owner_Account__c, house_name__c, 
                                                 (SELECT id from Flocks__r where Status__c = 'Active'), 
                                                 id from House__c WHERE id in: houseIds] );
        system.debug(farmIds);
        
        // count active flocks for every account
        for (Flock__c flock:this.activeFlocks){
            id accId = flock.House__r.Farm__r.Farm_Owner_Account__c;
            if(this.activeFlockPerAccount.containsKey(accId)){
                this.activeFlockPerAccount.put(accId,this.activeFlockPerAccount.get(accId)+1);
            } else {
               this.activeFlockPerAccount.put(accId, 1); 
            }
        }
    }
    /*
    public integer getNumberOfActiveFlocksOnAccount(id accId){
        if(this.activeFlockPerAccount.containsKey(accId)){
            return this.activeFlockPerAccount.get(accId);
        } else {
            return 0;
        }
    }*/
    
    public id getAccountForFlock(id flockId){
        Map<Id, Flock__c> mapOfFlocks =  new Map<Id, Flock__c>(this.activeFlocks);
        if(mapOfFlocks.containsKey(flockId)){
            return mapOfFlocks.get(flockId).House__r.Farm__r.Farm_Owner_Account__c;
        }
        return null;
    }
    
    public id getFarmForFlock(id flockId){
        Map<Id, Flock__c> mapOfFlocks =  new Map<Id, Flock__c>(this.activeFlocks);
        if(mapOfFlocks.containsKey(flockId)){
            return mapOfFlocks.get(flockId).House__r.Farm__c;
        }
        return null;
    }
    
    public id getHouseForFlock(id flockId){
        Map<Id, Flock__c> mapOfFlocks =  new Map<Id, Flock__c>(this.activeFlocks);
        if(mapOfFlocks.containsKey(flockId)){
            return mapOfFlocks.get(flockId).House__c;
        }
        return null;
    }
    
    public id getFlockForHouse(id houseId){
        if(housesMap.containsKey(houseId)){
            if(housesMap.get(houseId).Flocks__r.size() > 0){
                return housesMap.get(houseId).Flocks__r[0].id;
            } 
        }
        return null;
    } 
    
    public id getFarmForHouse(id houseId){
        if(housesMap.containsKey(houseId)){
            return housesMap.get(houseId).Farm__c;
        }
        return null;
    }
    
    public id getAccountForHouse(id houseId){
        if(housesMap.containsKey(houseId)){
            return housesMap.get(houseId).Farm__r.Farm_Owner_Account__c;
        }
        return null;
    }
    
    public id getAccountForFarm(id farmId){
        if(farmsMap.containsKey(farmId)){
            return farmsMap.get(farmId).Farm_Owner_Account__c;
        }
        return null;
    }
    
    public void fillEmptyFields(Visit__c visit){
        fillEmptyFields(visit, Visit__c.sObjectType);
    }
    public void fillEmptyFields(Issue__c issue){
        fillEmptyFields(issue, Issue__c.sObjectType);
    }
    
    // fill account in visit and house, farm and account in issue
    private void fillEmptyFields( sObject obj , Schema.sObjectType objType){
        if(obj.get('Farm__c') <> null){
            // autofill account if farm was specified  
            obj.put('Farm_Owner_Account__c', getAccountForFarm( (id)obj.get('Farm__c') ) ) ;
        }
        // fill issue empty fields
        if(Issue__c.sObjectType == obj.getSObjectType()){
            if(obj.get('House__c') <> null){
                obj.put('Flock__c', getFlockForHouse( (id)obj.get('House__c')) );
                
                // autofill farm if flock was specified 
                obj.put('Farm__c', getFarmForHouse( (id)obj.get('House__c')) );
                
                // autofill account if flock was specified  
                obj.put('Farm_Owner_Account__c', getAccountForHouse( (id)obj.get('House__c')) );
            }
            if(obj.get('Flock__c') <> null){
                // autofill house if flock was specified 
                obj.put('House__c', getHouseForFlock( (id)obj.get('Flock__c')) );
                
                // autofill farm if flock was specified 
                obj.put('Farm__c', getFarmForFlock( (id)obj.get('Flock__c')) );
                
                // autofill account if flock was specified 
                obj.put('Farm_Owner_Account__c', getAccountForFlock( (id)obj.get('Flock__c')) );   
            } 
        }	
    }
    // get current open visit for user
    public static Visit__c getCurrentUserOngoingVisit(){
        Visit__c[] visits = [SELECT Farm_Owner_Account__c , //Flock__c, Flock__r.House__c,
                             //Flock__r.House__r.id, Flock__r.House__r.Farm__c, Flock__r.House__r.Farm__r.id, 
                             //House__c, 
                             Farm__c, (SELECT id, House__c FROM House_Visits__r ORDER BY House__r.House_Name__c ),
                             id FROM Visit__c WHERE CreatedById =: UserInfo.getUserId() AND Status__c =: Label.Ongoing_Feminine ];
        if(visits.size() == 1){
            return visits[0];
        } else if(visits.size() == 0) {
            return null;
        } else {
            throw new VisitHelperException('Znaleziono więcej niż jedną rozpoczętą wizytę');
            return null;
        }
    }
    
    public static boolean visitIsOngoingForUser(Visit__c visit){
        if(visit.CreatedById == UserInfo.getUserId() && visit.Status__c == Label.Ongoing_Feminine){
            return true;
        }
        return false;
    }
    
    public static Map <Id, List<Visit__c>> ongoingVisitsPerUser(){
        List <Visit__c> ongoingVisits = [SELECT CreatedById, id, name FROM Visit__c WHERE Status__c =: Label.Ongoing_Feminine];
        Map <Id, List<Visit__c>> openVisitPerUser = new Map <Id, List<Visit__c>>();
        for(Visit__c visit : ongoingVisits){
            if(openVisitPerUser.containsKey(visit.CreatedById ) ){
                openVisitPerUser.get(visit.CreatedById).add(visit);
            } else {
                List<Visit__c> visits = new List<Visit__c>{visit};
                openVisitPerUser.put(visit.CreatedById, visits  );
            }
        }
        return openVisitPerUser;
    }
    
    // checks how many open visits current usrer have
    // returns false if user have 0 open visits, true otherwise
    public static Boolean maxOneOngoingVisitPerUser( ){
        Map <Id, List<Visit__c>> ongoingVisits = ongoingVisitsPerUser();
        List <Integer> countedList = new List <Integer>();
        for( Id key:ongoingVisits.keySet() ){
            countedList.add(ongoingVisits.get(key).size());
        }
        countedList.sort();
        if(countedList.size() > 0 && countedList[countedList.size()-1] > 1 ){
            return false;
        }
        return true;
    }
    /*
    public static id getCurrentUserOngoingVisitFlock(){
         return getCurrentUserOngoingVisit(). Flock__c;
    } */
    
    public static Account[] sortByOngoingVisit(Account[] accs){
        return sortByOngoingVisitPriv(accs);
    }
    public static Farm__c[] sortByOngoingVisit(Farm__c[] farms){
        return sortByOngoingVisitPriv(farms);
    }
    public static House__c[] sortByOngoingVisit(House__c[] houses){
        return sortByOngoingVisitPriv(houses);
    }
    /*
    public static Flock__c[] sortByOngoingVisit(Flock__c[] flocks){
        return sortByOngoingVisitPriv(flocks);
    }
	*/
    // sorts list by open visit
    private static sObject[] sortByOngoingVisitPriv(sObject[] objs){
        Visit__c visit = getCurrentUserOngoingVisit();
        if(visit == null || objs == null || objs.size() == 0 ){
            return objs;
        }
        Schema.sObjectType sObjToken = objs[0].id.getSObjectType();
        
        if(sObjToken == Schema.Account.getSObjectType()){
            objs = sortByField(objs,visit.Farm_Owner_Account__c );
            //objs = addEmptyObject(objs,1 );
        } else if(sObjToken == Schema.Farm__c.getSObjectType()){
            objs = sortByField(objs,visit.Farm__c);
            //objs = addEmptyObject(objs,1 );
        } else if(sObjToken == Schema.House__c.getSObjectType()){
        	for(Integer i = visit.House_Visits__r.size()-1 ; i >= 0; i--){
            	objs = sortByField(objs, visit.House_Visits__r[i].House__c);
			}
            //objs = addEmptyObject(objs, visit.House_Visits__r.size() );
        } else {
            throw new VisitHelperException('Unhandled list type List<'+objs[0].getSObjectType()+'>' );
            return null;
        } 
        
        return objs;
    }
    
    // puts @atribute firstElementId in front of the list if list contains that element
    private static sObject[] sortByField(sObject[] objs, id firstElementId){
        system.debug('size of objs '+objs.size());
        if(objs.size() == 1 || firstElementId == null){
            return objs;
        }
        for(integer i = 0; i < objs.size(); i++){
            if(objs[i].id == firstElementId){
                sObject obj = objs.remove(i);
                objs.add(0, obj);
                String prefix = '🐥 ';
                if(obj.getSObjectType() == House__c.sObjectType){
                	obj.put( 'House_Name__c',prefix+obj.get('House_Name__c') ) ;
                } else {
                	obj.put( 'Name',prefix+obj.get('Name')) ;
                }
                break;
            }
        }
        return objs;
    }
    
    /*
    // adds empty object to the list to separate 
    private static sObject[] addEmptyObject(sObject[] objs, Integer index){
    	// dont't add object if index is 0 or if empty object would be lat 
    	if(objs.size() > index && index > 0 && objs.size() > 0){
    		String sObjName = objs[0].id.getSObjectType().getDescribe().getName();
    		sObject obj = Schema.getGlobalDescribe().get(sObjName).newSObject() ;
    		objs.add(index, obj);
    	}
        return objs;
    }
    */
    
    // links all lightning notes to current ongoing visit 
    // not used
    @future(callout=true)
    public static void linkNote(id ContentDocumentID,id LinkedEntityId, string ShareType ){
        ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentID = ContentDocumentID,
                                                          LinkedEntityId = LinkedEntityId,
                                                          ShareType = ShareType);
        Database.insert( cdl, false );
    } 
    
    
    /* recalculates sharing so the person in AssignedTo__c field will have Edit permision
    public static void recalculateVisitSharing(List <Visit__c> visits){
        final String rowCause = 'VisitAssignment__c'; 
        Set <id> visitIds = new Set<id>();
        for(Visit__c v: visits ){
            visitIds.add(v.id);
        }
        List < Visit__Share > shares = [SELECT id FROM Visit__Share WHERE ParentId IN :visitIds AND RowCause =: rowCause ];
        delete shares;
        
        List < Visit__Share > newShares = new List < Visit__Share >();
        for(Visit__c v: visits ){
            newShares.add( new  Visit__Share( ParentId = v.Id, RowCause = rowCause, UserOrGroupId = v.AssignedTo__c, AccessLevel = 'Read' ) );
        }
        insert newShares;
    }
*/
}