@isTest
public class VisitTriggerTest {
    
    @testSetup
    public static void testSetup(){
        Account test = new Account(Name='test', Brojler__c=true);
        insert test;
        Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
        insert testC; 
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
        insert farm;
        House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
        insert house;
        //dodaje jakas norme
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
        Standard_Stats__c stdSt = new Standard_Stats__c(Standard__c = std.id, Day__c=2, Daily_water_consumption__c=33, 
                                                        Daily_feed_consumed__c=16, Avg_body_weight__c =50  );
        insert stdSt;
        
        Date day = System.today()-2;
        Flock__c flock = new Flock__c( House__c = house.id, 
                                      Birds_at_arrival__c  = 5000,
                                      Chick_weight_at_arrival__c = 40,
                                        Species__c='Broiler',
                                      Genetic_Line__c = 'Ross 308',
                                      Date_of_arrival__c = day);
        insert flock;
    }
    
    @isTest
    public static void testFillFields(){
        
        Flock__c flock = [SELECT id, House__c, House__r.Farm__c, House__r.Farm__r.Farm_Owner_Account__c from Flock__c LIMIT 1];
        Visit__c visit = new Visit__c( Date__c = system.today(), Start_Date__c = system.now());
        insert visit;
        visit = [select id, Farm_Owner_Account__c, Farm__c from Visit__c where id =:visit.id ] ;   
        
        //System.assertEquals(flock.House__r.Farm__r.Farm_Owner_Account__c, visit.Farm_Owner_Account__c );
        //System.assertEquals(flock.House__r.Farm__c, visit.Farm__c );
        //System.assertEquals(flock.House__c, visit.House__c );
        
    }
}