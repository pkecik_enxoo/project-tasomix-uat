@isTest
public class AccountStatusUpdateBatchTest {
    
    @testSetup
    public static void testSetup(){
        Account test = new Account(Name='test', Brojler__c=true);
        insert test;
        Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
        insert testC;
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
        insert farm;
        House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
        insert house;
        //dodaje jakas norme
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
        Standard_Stats__c stdSt = new Standard_Stats__c(Standard__c = std.id, Day__c=2, Daily_water_consumption__c=33, 
                                                        Daily_feed_consumed__c=16, Avg_body_weight__c =50  );
        insert stdSt;
        
        Date day = System.today()-1000;
        Flock__c flock = new Flock__c( House__c = house.id, 
                                      Birds_at_arrival__c  = 5000,
                                      Chick_weight_at_arrival__c = 40,
                                        Species__c='Broiler',
                                      Genetic_Line__c = 'Ross 308',
                                      Date_of_arrival__c = day);
        insert flock;
        flock.Date_of_removal__c = day.addDays(2);
        
        update flock;


    }
    @isTest
    public static void testLost(){
        
        AccountStatusUpdateBatch myBatch =  new AccountStatusUpdateBatch();
        test.startTest();
        Id batchJobId = Database.executeBatch(new AccountStatusUpdateBatch());
        test.stopTest();
        
        System.assertEquals('Lost', [Select status__c from account limit 1].Status__c);
    }
    /*
    @isTest
    public static void testNotLostActiveFlock(){
        House__c house = [select id from House__c];
        
        Date day = System.today()-20;
        Flock__c flock = new Flock__c( House__c = house.id, 
                                      Birds_at_arrival__c  = 35000,
                                      Chick_weight_at_arrival__c = 40,
                                      Genetic_Line__c = 'Ross 308',
                                      Date_of_arrival__c = day);
        insert flock;
        
        AccountStatusUpdateBatch myBatch =  new AccountStatusUpdateBatch();
        test.startTest();
        Id batchJobId = Database.executeBatch(new AccountStatusUpdateBatch());
        test.stopTest();
        
        System.assertEquals('Active', [Select status__c from account limit 1].Status__c);
    }
    
    @isTest
    public static void testNotLostMultipleFarms(){
        Account testAcc = [SELECT id from Account where name = 'test'];
        Contact testC = [SELECT id from Contact where AccountId =: testAcc.id];
        
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = testAcc.id, Postal_Code__c = '12s3-123');
        insert farm;
        House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 2', House_area__c = 2000);
        insert house;
        
        Date day = System.today()-20;
        Flock__c flock = new Flock__c( House__c = house.id, 
                                      Birds_at_arrival__c  = 35000,
                                      Chick_weight_at_arrival__c = 40,
                                      Genetic_Line__c = 'Ross 308',
                                      Date_of_arrival__c = day);
        insert flock;
        flock.Date_of_removal__c = day.addDays(2);
        
        update flock;
        
        
        AccountStatusUpdateBatch myBatch =  new AccountStatusUpdateBatch();
        test.startTest();
        Id batchJobId = Database.executeBatch(new AccountStatusUpdateBatch());
        test.stopTest();
        
        System.assertEquals('Active', [Select status__c from account limit 1].Status__c);
    }
  */  
}