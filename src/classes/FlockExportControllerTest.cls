@isTest
public class FlockExportControllerTest {

    public static testMethod void MyController() {
      
      
        

        Account test = new Account(Name='test', Brojler__c=true);
    		insert test;
    		Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
       	 	insert testC;
            Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
    		insert farm;
            House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
            insert house;
        	Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
            insert std;
        
        Date day = System.today();
            Flock__c flock = new Flock__c( House__c = house.id, 
                                          Birds_at_arrival__c  = 1000,
                                          Chick_weight_at_arrival__c = 40,
                                            Species__c='Broiler',
                                          Genetic_Line__c = 'Ross 308',
                                          Date_of_arrival__c = day);
        insert flock;
        
        Standard_Stats__c stdSt = new Standard_Stats__c(Standard__c = std.id, Day__c=2, Daily_water_consumption__c=33, 
                                                      Daily_feed_consumed__c=16, Avg_body_weight__c =50  );
        insert stdSt;
        
        Flock_stats__c flSt = new Flock_stats__c(Flock__c=flock.id, Date__c = day+2); 
        insert flSt;
       
		Thinning__c th1 = new Thinning__c(Flock__c = flock.Id,Birds_at_slaughterhouse__c=500,Birds_thinned_at_farm__c=600,Date__c=day,Total_weight_at_farm__c=500,Total_weight_at_slaughterhouse__c=500);
            insert th1;
        
        ApexPages.currentPage().getParameters().put('recordId', flock.Id);
        
		FlockExportController controller = new FlockExportController();
        // Add parameters to page URL
        
      

    }
    
}