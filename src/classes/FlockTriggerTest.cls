@IsTest
public class FlockTriggerTest {
    	@testSetup
		static void insertFarm()
        {
            Account test = new Account(Name='test', Brojler__c=true);
    		insert test;
    		Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
       	 	insert testC;
            Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
    		insert farm;
            House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
            insert house;
            //dodaje jakas norme
            Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
            insert std;
        }
    
    	@IsTest
    	static void checkFlockWithoutStandard()
        {
            House__c house = [Select id from House__c limit 1];
            Date day = System.today();
            Flock__c flock = new Flock__c( House__c = house.id, 
                                          Birds_at_arrival__c  = 1000,
                                          Chick_weight_at_arrival__c = 40,
                                            Species__c='Broiler',
                                          Genetic_Line__c = 'Cobb 500',
                                          Date_of_arrival__c = day);
            
            try{
            	insert flock;
            }
            catch(Exception e)
            {
                system.debug('error ' + e.getMessage());
                Boolean expectedExceptionThrown = e.getMessage().contains('Brak normy dla rasy');
          		System.assertEquals(expectedExceptionThrown, true);  
            }
        }
    
        @IsTest
    	static void checkFlockWithStandard()
        {
            House__c house = [Select id from House__c limit 1];
            Date day = System.today();
            Flock__c flock = new Flock__c( House__c = house.id, 
                                          Birds_at_arrival__c  = 1000,
                                          Chick_weight_at_arrival__c = 40,
                                            Species__c='Broiler',
                                          Genetic_Line__c = 'Ross 308',
                                          Date_of_arrival__c = day);
            
            try{
            	insert flock;
            }
            catch(Exception e)
            {
                system.debug('error ' + e.getMessage());
            }
            
            Standard__c std = [Select id from Standard__c where Genetic_Line__c = 'Ross 308'];
            Flock__c insertedFlock = [Select Standard__c from Flock__c where id =: flock.id ] ;
            System.assertEquals(std.id, insertedFlock.Standard__c);
        }
    
        @IsTest
    	static void checkFlockName()
        {
            House__c house = [Select id from House__c limit 1];
            Date day1 = Date.newInstance(2017, 01, 01);
            Flock__c flock1 = new Flock__c( House__c = house.id, 
                                          Birds_at_arrival__c  = 1000,
                                          Chick_weight_at_arrival__c = 40,
                                            Species__c='Broiler',
                                          Genetic_Line__c = 'Ross 308',
                                          Date_of_arrival__c = day1,
                                          Date_of_removal__c = day1+30);
            Date day2 = Date.newInstance(2017, 03, 01);
            Flock__c flock2 = new Flock__c( House__c = house.id, 
                                          Birds_at_arrival__c  = 2000,
                                          Chick_weight_at_arrival__c = 41,
                                            Species__c='Broiler',
                                          Genetic_Line__c = 'Ross 308',
                                          Date_of_arrival__c = day2);
            
            try{
            	insert flock1;
                insert flock2;
            }
            catch(Exception e)
            {
                system.debug('error ' + e.getMessage());
            }
            
            Thinning__c th1 = new Thinning__c(Flock__c = flock1.Id,Birds_at_slaughterhouse__c=500,Birds_thinned_at_farm__c=600,Date__c=day2,Total_weight_at_farm__c=500,Total_weight_at_slaughterhouse__c=500);
            insert th1;
            
            //Standard__c std = [Select id from Standard__c where Genetic_Line__c = 'Ross 308'];
            List<Flock__c> insertedFlocks = [Select Flock_number__c from Flock__c ] ;
           
            System.assertEquals( '1-'+day1.year(), insertedFlocks.get(0).Flock_number__c );
            System.assertEquals( '2-'+day1.year(), insertedFlocks.get(1).Flock_number__c );
        }
	
}