public class FlockStatsHelper {
	
    /* 
     * @author Kacper Augustyniak 2017-10-30
     * Wymusza aktualizację Flock_Stats__c z Standard_assigned__c = false w celu ponownego dopasowania standardu
     * @param fSToUpdate - list lub set id Flock_Stats__c do aktualizacji standardu
     */
    public static void forceFutureStandardMatch(list<id> fSToUpdate){
    	forceFutureStandardMatch(new set<id>(fSToUpdate));
    }
    
    @future (callout=true)
    public static void forceFutureStandardMatch(set<id> fSToUpdate){
        List< Flock_Stats__c> flStatList = [select id from Flock_Stats__c where id in: fSToUpdate ];
        for(Flock_stats__c flStat : flStatList ) {
                flStat.Standard_assigned__c = false; 
            }
        update flStatList;
    }
}