@isTest
public class SetFarmLocationFromMyLocationTest {
	@isTest
    public static void test(){
        
        Account a = new Account(Name='testAcc', Brojler__c=true);
        insert a;
        Farm__c farm = new Farm__c(Name='testFarm',Farm_Owner_Account__c = a.id,Postal_Code__c = '120312');
        insert farm;
        farm.name = 'NewName';
        farm.Geolocation__Latitude__s = 20;
        farm.Geolocation__Longitude__s = 20;
        
        Farm__c farm2 = SetFarmLocationFromMyLocationController.saveFarm( farm );
        System.assertEquals(0, [select count() from farm__c where name = 'NewName' ] );
        System.assertEquals(1, [select count() from farm__c where Geolocation__Latitude__s = 20 ] );
    }
}