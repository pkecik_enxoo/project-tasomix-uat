/*
* @author Jarosław Kołodziejczyk 2017-04-04
* Klasa przetrzymująca parametry dla kurników i ferm, wykorzystywana w kontrolerze apexowym i w aurze
*/

public with sharing class PanelFermFlockStatsParams {
	@AuraEnabled
	public Date fsDate {get;set;}
	/*
	@AuraEnabled
	public decimal avgBodyWeight {get;set;}
	@AuraEnabled
	public decimal density {get;set;}
	@AuraEnabled
	public string avgBodyWeightLastUpdatedDaysNumber {get;set;}
	@AuraEnabled
	public integer avgBodyWeightDay {get;set;}
	*/
	/*
	@AuraEnabled
	public Integer dailyDead {get;set;}
	@AuraEnabled
	public decimal dailyWaterConsumption {get;set;}
	@AuraEnabled
	public String dailyWaterConsumptionRed {get;set;}
	@AuraEnabled
	public Decimal DailyDeadTotal {get;set;}
	@AuraEnabled
	public Decimal DailyDeadPercent {get;set;}
	@AuraEnabled
	public string DailyDeadPercentColor {get;set;}
	@AuraEnabled
	public string DailyDeadTotalLastUpdatedDaysNumber {get;set;}
	@AuraEnabled
	public integer DailyDeadTotalDay {get;set;}
	*/
	/*
	@AuraEnabled
	public Decimal WaterFeed {get;set;}
	@AuraEnabled
	public string WaterFeedLastUpdatedDaysNumber {get;set;}
	@AuraEnabled
	public integer WaterFeedDay {get;set;}
	*/
	/*
	@AuraEnabled
	public Decimal WeightStandardTable {get;set;}
	@AuraEnabled
	public Decimal WeightStandardChick {get;set;}
	@AuraEnabled
	public string WeightStandardTableColor {get;set;}
	@AuraEnabled
	public string WeightStandardChickColor {get;set;}
	@AuraEnabled
	public string WeightStandardChickLastUpdatedDaysNumber {get;set;}
	@AuraEnabled
	public integer WeightStandardChickDay {get;set;}
	@AuraEnabled
	public string FeedConsumptionStandardChickLastUpdatedDaysNumber {get;set;}
	@AuraEnabled
	public integer FeedConsumptionStandardChickDay {get;set;}
	@AuraEnabled
	public Decimal FeedConsumptionStandardChick {get;set;}
	@AuraEnabled
	public Decimal FeedConsumptionStandardTable {get;set;}
	@AuraEnabled
	public Decimal WaterConsumptionStandardChick {get;set;}
	@AuraEnabled
	public Decimal WaterConsumptionStandardTable {get;set;}
	@AuraEnabled
	public string WaterConsumptionStandardChickLastUpdatedDaysNumber {get;set;}
	@AuraEnabled
	public integer WaterConsumptionStandardChickDay {get;set;}
	@AuraEnabled
	public decimal weightTableArrow {get;set;}
	@AuraEnabled
	public decimal weightChickArrow {get;set;}
	*/
	
	@AuraEnabled
	public SingleParameterWeight weight {get;set;}
	@AuraEnabled
	public SingleParameterDayToDayChange weightStandard {get;set;}
	@AuraEnabled
	public SingleParameterDayToDayChange waterStandard {get;set;}
	@AuraEnabled
	public SingleParameterDayToDayChange feedConsumptionStandard {get;set;}
	@AuraEnabled
	public SingleParameterDailyDead dead {get;set;}
	@AuraEnabled
	public SingleParameterWaterFeed waterFeed {get;set;}
	
    public PanelFermFlockStatsParams(){
        this.weight = new SingleParameterWeight();
        this.weightStandard = new SingleParameterDayToDayChange();
        this.waterStandard = new SingleParameterDayToDayChange();
        this.feedConsumptionStandard = new SingleParameterDayToDayChange();
        this.dead = new SingleParameterDailyDead();
        this.waterFeed = new SingleParameterWaterFeed();
    }
    
    public virtual class SingleBaseStat {
		@AuraEnabled
		public Decimal value {get; private set;}
		@AuraEnabled
		public String unit {get; private set;}
		// change from older parameter
		@AuraEnabled
		public Decimal change {get; private set;}
		// change from older parameter in %
		@AuraEnabled
		public Decimal changePc {get; private set;}
		
		public SingleBaseStat(){}
		public SingleBaseStat(String unit){this.unit=unit;}
		
		// provide values from old to new
		virtual public boolean setValue(Decimal olderValue){
            if(olderValue == null) return false;
            if(this.value <> null ){
                //set arrow if not set yet
                if(this.change == null){
                	this.change = this.value - olderValue ;
                	if(olderValue == 0 && this.value == 0){
                		this.changePc = 0;
                	}else if(olderValue <> 0 && olderValue <> null){
            			this.changePc = (  ( this.value - olderValue)/olderValue) * 100;
            			this.changePc = this.changePc.setScale(2);
                	}
                }
                return false;
            }  else {
				this.value = olderValue;
				return true;	
			}
	    	
	    }
    }
    
    public class SingleStandardStat extends SingleBaseStat {
		@AuraEnabled
		public Decimal standardValue {get; private set;}
		@AuraEnabled
		public Decimal statValue {get; private set;}
		@AuraEnabled
		public String color {get; private set;}
		/*
		@AuraEnabled
		public Decimal value {public get; private set;}
		@AuraEnabled
		public String unit {get; private set;}
		@AuraEnabled
		public Decimal arrow {get; private set;}
		*/
		@AuraEnabled
		public String ratio {get; private set;}
		
		public SingleStandardStat(){
			ratio = '-';
		}
		
		override public boolean setValue(Decimal newValue){
			if(super.setValue( newValue ) ){
				setColor( newValue );
				return true;
			}
			return false;
		}
		
        private Boolean setStandard(Decimal statStandard, Decimal standardValue, Decimal statValue, String unit){
            if(setValue( statStandard )){
				this.standardValue = standardValue;
				this.statValue = statValue;
				this.unit = unit;
				if(statValue <> null && standardValue<>null){
					this.ratio = statValue+unit+' / '+standardValue+unit;
				}
				return true;
            }
            return false;
            /*
            if(statStandard == null) return false;
            if(this.standard <> null && this.standard <> 0){
                //set arrow if not set yet
                if(this.arrow == null){
                	this.arrow = this.standard - statStandard ;
                }
                return false;
            }  else {
				setValue( statStandard );
				this.standardValue = standardValue;
				this.statValue = statValue;
				this.unit = unit;
				if(statValue <> null && standardValue<>null){
					this.ratio = statValue+unit+'/'+standardValue+unit;
				}
				return true;
            }*/
        }
        
		private void setColor( Decimal parameterValue){
			if(parameterValue < 95 && parameterValue != null) {
                color = 'red';
            }
            else if(parameterValue >= 95 && parameterValue < 100) {
                color = 'orange';
            }
            else if(parameterValue >= 100) {
                color = 'green';
            }
            else {
                color = 'black';
            }
		}
    	
    }
    
    // base parameter for panel ferm shares all common traits
	virtual public class SingleParameter {
		@AuraEnabled
		public String daysElapsed {get;private set;}
		@AuraEnabled
		public Decimal statDay {get;private set;}
		@AuraEnabled
		public Date statDate {get; private set;}
		@AuraEnabled
		public SingleBaseStat dailyStat {get; private set;}
		
		protected SingleParameter(){
			this.dailyStat = new SingleBaseStat();
		}
        
        private void setDaysElapsed(Date dt){
        	this.statDate = dt;
            Date today = date.today();
            integer dateV = dt.daysBetween(today);
            daysElapsed = string.valueOf(dateV);
        }
		
        // Feed data from oldest stat to newest
        virtual public void setDailyStat(Decimal value, Date dt, Decimal stDay){
        	//System.debug( stDay+' daily dad '+ value);
        	if(dailyStat.setValue(value)){//
				setDaysElapsed(dt);
				this.statDay = stDay;
        	}
    	}
	
	}
    /*
    public class SingleParameterWaterFeed extends SingleParameter {
		@AuraEnabled
		public SingleDailyDeadStat avgWeight {get; private set;}
		@AuraEnabled
		public Decimal density {get; private set;}
		
		
        // Feed data from oldest stat to newest
        public void setAvgWeight(Decimal value,Decimal newDensity, Date dt, Decimal stDay){
        	if(avgWeight.setValue(value)){
				setDaysElapsed(dt);
				this.statDay = stDay;
				this.density = newDensity;
        	}
    	}
    }
    */
    public class SingleParameterDailyDead extends SingleParameter {
    	/*
		@AuraEnabled
		public SingleBaseStat dailyStat {get; private set;}
		*/
		@AuraEnabled
		public Decimal total {get; private set;}
		@AuraEnabled
		public Decimal totalPercent {get; private set;}
		@AuraEnabled
		public String color {get; private set;}
		
		public SingleParameterDailyDead(){
			this.dailyStat = new SingleBaseStat();
		}
		 // dont use
        override public void setDailyStat(Decimal value, Date dt, Decimal stDay){
            throw new AuraHandledException('Please use setDailyStat(Decimal , Date , Decimal , Integer , Integer  ) instead.');
        }
        // Feed data from oldest stat to newest
        public void setDailyStat(Decimal value, Date dt, Decimal stDay, Decimal dailyDead, Decimal dailySelection ){
        	if(this.statDay <> null && Math.abs(stDay - this.statDay ) > 1){
        		return;
        	}
        	if(dailyDead <> null &&  dailySelection <> null ){
    			super.setDailyStat( value,  dt,  stDay);
        	} 
    	}
    	
    	public void setTotals(Decimal totalDead, Decimal totalDeadPercent ){
    		this.total = totalDead;
    		this.totalPercent = totalDeadPercent;
    		setColor( );
    		
    	}
    	
		private void setColor( ){
			if(this.totalPercent > 5 ) {
                color = 'red';
            }
            else {
                color = 'black';
            }
		}
    }
    
    public class SingleParameterWaterFeed extends SingleParameter {
		
		public SingleParameterWaterFeed(){
		}
		
        // Feed data from oldest stat to newest
        override public void setDailyStat(Decimal value, Date dt, Decimal stDay){
            /*
            System.debug(' water feed ');
            System.debug(value+' v|stDay '+stDay);
            System.debug(this.statDay);
*/
        	if(this.statDay <> null && Math.abs(stDay - this.statDay ) > 1){
        		return;
        	} 
        	super.setDailyStat( value,  dt,  stDay);
    	}
    }
    
    public class SingleParameterWeight extends SingleParameter {
    	/*
		@AuraEnabled
		public SingleBaseStat avgWeight {get; private set;}
		*/
		@AuraEnabled
		public Decimal density {get; private set;}
		
		public SingleParameterWeight(){
			this.dailyStat = new SingleBaseStat('g');
		}
		
        // Feed data from oldest stat to newest
        public void setAvgWeight(Decimal value,Decimal newDensity, Date dt, Decimal stDay){
        	if(this.statDay <> null && Math.abs(stDay - this.statDay ) > 1){
        		return;
        	} 
        	if(dailyStat.setValue(value)){
				setDaysElapsed(dt);
				this.statDay = stDay;
				this.density = newDensity;
        	}
    	}
    }
    
	public class SingleParameterDayToDayChange extends SingleParameter {
		@AuraEnabled
		public SingleStandardStat table {get; private set;}
		@AuraEnabled
		public SingleStandardStat chick {get; private set;}
		/*
		@AuraEnabled
		public Decimal tableStandard {public get;
                                      private set{ 
                                          tableStandard = value;
                                          setArrowColor('tableStandard', value);
                                      }
                                     }
		@AuraEnabled
		public String tableStandardColor {get; private set;}
		@AuraEnabled
		public Decimal tableStandardArrow {get; private set;}
		
        @AuraEnabled
        public Decimal chickStandard {public get;
                                      private set{ 
                                          chickStandard = value;
                                          setArrowColor('chickStandard', value);
                                      }
                                     }
		@AuraEnabled
		public String chickStandardColor {get;private set;}
		@AuraEnabled
		public Decimal chickStandardArrow {get;private set;}
		*/
		/*
		@AuraEnabled
		public Decimal lastUpdatedDaysNumber {get;set;}
		@AuraEnabled
		public Decimal avgLastUpdatedDaysNumber {get;set;}
		@AuraEnabled
		public Decimal standardChickDay {get;set;}
		*/
		
		
		
		public SingleParameterDayToDayChange(){
			this.table = new SingleStandardStat();
			this.chick = new SingleStandardStat();
		}
		
        public void setTableStandard(Decimal value, Decimal standardValue, Decimal statValue, String unit, Date dt, Decimal stDay){
        	if(table.setStandard(value, standardValue, statValue, unit)){
				setDaysElapsed(dt);
				this.statDay = stDay;
        	}
        	/*
            if(value == null) return;
            if(this.tableStandard <> null && this.tableStandard <> 0){
                //set arrow if not set yet
                if(this.tableStandardArrow == null){
                	this.tableStandardArrow = this.tableStandard - value ;
                }
            }  else {
				this.tableStandard = value;
				setDaysElapsed(dt);
				this.statDay = stDay;
            }*/
        }
        // Feed data from oldest stat to newest
        public void setChickStandard(Decimal value, Decimal standardValue, Decimal statValue, String unit, Date dt, Decimal stDay){
        	if(chick.setStandard(value, standardValue, statValue, unit)){
				setDaysElapsed(dt);
				this.statDay = stDay;
        	}
        }
        // Feed data from oldest stat to newest
        public void setChickStandard(Decimal value, Date dt, Decimal stDay){
        	if(chick.setStandard(value, 0, 0, '')){
				setDaysElapsed(dt);
				this.statDay = stDay;
        	}
        	/*
            if(value == null) return;
            if(this.chickStandard <> null && this.chickStandard <> 0){
                //set arrow if not set yet
                if(this.chickStandardArrow == null){
                	this.chickStandardArrow = this.chickStandard - value ;
                }
            }  else {
				this.chickStandard = value;
				setDaysElapsed(dt);
				this.statDay = stDay;
            }*/
        }
        /*
		private void setArrowColor(String parameterName, Decimal parameterValue){
			String colorValue;
			if(parameterValue < 95 && parameterValue != null) {
                colorValue = 'red';
            }
            else if(parameterValue >= 95 && parameterValue < 100) {
                colorValue = 'orange';
            }
            else if(parameterValue >= 100) {
                colorValue = 'green';
            }
            else {
                colorValue = 'black';
            }
            
            if(parameterName == 'tableStandard'){
            	this.tableStandardColor = colorValue;
            } else if(parameterName == 'chickStandard'){
            	this.chickStandardColor = colorValue;
            } 
		}
	*/
	}
}