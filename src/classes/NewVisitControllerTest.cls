@isTest
public class NewVisitControllerTest {
	@isTest
    public static void test1(){
        
        Account test = new Account(Name='test', Brojler__c=true);
        insert test;
        Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
        insert testC;
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
        insert farm;
        House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
        insert house;
        //dodaje jakas norme
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
        Standard_Stats__c stdSt = new Standard_Stats__c(Standard__c = std.id, Day__c=2, Daily_water_consumption__c=33, 
                                                      Daily_feed_consumed__c=16, Avg_body_weight__c =50  );
        insert stdSt;
        
        Date day = System.today()-2;
        Flock__c flock = new Flock__c( House__c = house.id, 
                                      Birds_at_arrival__c  = 5000,
                                      Chick_weight_at_arrival__c = 40,
                                        Species__c='Broiler',
                                      Genetic_Line__c = 'Ross 308',
                                      Date_of_arrival__c = day);
        insert flock;
    
        List<House__c> houses = NewVisitController.getHouses(farm.id);
        System.assertEquals(1, houses.size() ) ;
        
        System.assertEquals(farm.id, NewVisitController.getFarms(test.id)[0].id );
        
        Visit__c visit = new Visit__c(Farm__c = farm.id);
        NewVisitController.saveVisitApex(farm.id, visit, houses, 'testing');
        
        System.assertEquals(1,[SELECT count() FROM House_Visit__c]);
        
        System.assertEquals('Account',NewVisitController.getObjectName(test.id) );
    }
}