@isTest
public class PanelFermWrapperControllerTest {

    static testMethod void test1(){
    Account test = new Account(Name='test', Brojler__c=true);
    insert test;
        Account test2 = new Account(Name='test2', Brojler__c=true);
    insert test2;
    Contact testC = new Contact(AccountId = test.id,LastName = 'tester');
        insert testC;
        Contact testM = new Contact(AccountId = test2.id,LastName = 'tester2');
        insert testM;
    Farm__c farm = new Farm__c(Manager__c = testM.id,Farm_Owner_Account__c = test.id,Postal_Code__c = '123-123');
    insert farm;
        PanelFermWrapperController.getAccount();
        PanelFermWrapperController.getFarms(test.id);
        
    }
    
    static testMethod void test11(){
    Account test = new Account(Name='test', Brojler__c=true);
    insert test;
        Account test2 = new Account(Name='test2',Brojler__c=true);
    insert test2;
    Contact testC = new Contact(AccountId = test.id,LastName = 'tester');
        insert testC;
        Contact testM = new Contact(AccountId = test2.id,LastName = 'tester2');
        insert testM;
    Farm__c farm = new Farm__c(Manager__c = testC.id,Farm_Owner_Account__c = test.id,Postal_Code__c = '123-123');
    insert farm;
        PAnelFermWrapperController.getAccount();
        PanelFermWrapperController.getFarms(test.id);
        
    }
    static testMethod void test2(){
    Account test = new Account(Name='test', Brojler__c=true);
    insert test;
    Contact testC = new Contact(AccountId = test.id,LastName = 'tester');
        insert testC;
    Farm__c farm = new Farm__c(Manager__c = testC.id,Farm_Owner_Account__c = test.id,Postal_Code__c = '123-123');
    insert farm;
        
        PanelFermWrapperController.getFarms(test.id);
        PanelFermWrapperController.getAccount();
    }
    
    static testMethod void test3(){
        
         //Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
         Profile p = [SELECT Id FROM Profile WHERE UserType = 'Standard' AND PermissionsAuthorApex = true LIMIT 1 ]; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='1991standarduser@testorg.com');
        
        system.runAs(u){
    Account test = new Account(Name='test', Brojler__c=true);
    insert test;
    Contact testC = new Contact(AccountId = test.id,LastName = 'tester');
        insert testC;
    Farm__c farm = new Farm__c(Manager__c = testC.id,Farm_Owner_Account__c = test.id,Postal_Code__c = '123-123');
    insert farm;
        
        PanelFermWrapperController.getFarms(test.id);
        
        House__c house = new House__c(House_name__c = 'house',Farm__c = farm.id,House_area__c = 59);
        insert house;
        
        
        PanelFermWrapperController.selectFarm(farm.id);
        
        }
	    }

    
    static testMethod void test4(){
         //Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
         Profile p = [SELECT Id FROM Profile WHERE UserType = 'Standard' AND PermissionsAuthorApex = true LIMIT 1 ]; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='1991standarduser@testorg.com');
        
        system.runAs(u){
    Account test = new Account(Name='test', Brojler__c=true);
    insert test;
    Contact testC = new Contact(AccountId = test.id,LastName = 'tester');
        insert testC;
    Farm__c farm = new Farm__c(Manager__c = testC.id,Farm_Owner_Account__c = test.id,Postal_Code__c = '123-123');
    insert farm;
        
        PanelFermWrapperController.getFarms(test.id);
        
        House__c house = new House__c(House_name__c = 'house',Farm__c = farm.id,House_area__c = 59);
        insert house;
        
        Standard__c st = new Standard__c(Genetic_Line__c = 'Cobb 500', Effective_Date__c = date.today()-1,Termination_Date__c = date.today()+1, Chick_weight_at_arrival__c = 15);
        
        insert st;
        
        for(integer i=1; i<32; i++)
        {
          Standard_stats__c sts = new Standard_stats__c(Standard__c = st.id, Day__c = i, Daily_water_consumption__c = 10, Daily_feed_consumed__c = 15, Avg_body_weight__c = 30);
        
        insert sts;  
        }
      
        
        Flock__c flock = new Flock__c(Standard__c = st.id,House__c = house.id,Flock_number__c = 'flock',Date_of_arrival__c = date.today()-1,Birds_at_arrival__c=10000,Date_of_removal__c = date.today()+1,Genetic_line__c = 'Cobb 500', Species__c='Broiler');
		insert flock;
        
        system.debug('Flock status: '+flock.Status__c);
        
        Flock_stats__c fstats = new Flock_stats__c(Flock__c = flock.id,Daily_dead__c = 5, Daily_selection__c = 3, Avg_body_weight__c = 50, Daily_feed_consumed__c = 25, Daily_water_consumption__c = 15, Date__c = date.today());
        insert fstats;
        PanelFermWrapperController.selectFarm(farm.id);
        
        
	    }
    }
}