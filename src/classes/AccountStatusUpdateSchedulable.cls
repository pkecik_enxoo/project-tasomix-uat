/* @author Kacper Augustyniak 2017-12-11
 * 
 * class that allows for AccountStatusUpdateBatch to be scheduled
 */ 
global class AccountStatusUpdateSchedulable implements Schedulable {
    global void execute(SchedulableContext sc){
        Id batchJobId = Database.executeBatch(new AccountStatusUpdateBatch(), 100);
    }
}