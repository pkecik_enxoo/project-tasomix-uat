public class ComparePanelSelectParams {
    @AuraEnabled
	public String param {get;set;}
	@AuraEnabled
	public String accId {get;set;}
    @AuraEnabled
	public String farmId {get;set;}
    @AuraEnabled
	public String houseId {get;set;}
	@AuraEnabled
	public String flockId {get;set;}
	@AuraEnabled
    public Boolean chickStandard {get;set;}
    @AuraEnabled
    public Boolean tableStandard {get;set;}
}