/* @author Kacper Augustyniak 2017-12-11
 * 
 * class that takes all accounts with every flock inactive for at least 183 days and 
 * marks them as inactive (Lost)
 */ 
global class AccountStatusUpdateBatch implements Database.Batchable<sObject>{
    private Integer noOfInactiveDays = 183;
    
    global Database.QueryLocator start(Database.BatchableContext BC){//Date_of_Latest_Removal__c
        Date inactiveDate = System.Today().addDays(-noOfInactiveDays);
        return Database.getQueryLocator([SELECT id, name, Status__c  FROM Account Where 
                                         Status__c <> 'Lost' AND //( Status__c = 'Active' OR Status__c = null ) AND 
                                         id IN (SELECT Farm_Owner_Account__c FROM Farm__c WHERE Date_of_Latest_Removal__c <:inactiveDate) AND
                                         id NOT IN (SELECT Farm_Owner_Account__c FROM Farm__c WHERE Number_of_Active_Flocks__c > 0 OR Date_of_Latest_Removal__c >=:inactiveDate)
                                        ]);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope)  {
        for(Account a : scope)
        {
            System.debug(a.name);  
            a.Status__c = 'Lost';
        }
        update scope;
    }  
    
    global void finish(Database.BatchableContext BC) {
    }
}