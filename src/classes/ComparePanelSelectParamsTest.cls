@isTest
public class ComparePanelSelectParamsTest {
    @isTest
    public static void checkClass()
    {
        ComparePanelSelectParams par = new ComparePanelSelectParams();
        par.param = 'cos';
        system.assertEquals('cos', par.param);
        
        par.accId = 'cos';
        system.assertEquals('cos', par.accid);
        
        par.farmId = 'cos';
        system.assertEquals('cos', par.farmId);
        
        par.houseId= 'cos';
        system.assertEquals('cos', par.houseId);
        
        par.flockId = 'cos';
        system.assertEquals('cos', par.flockId);
        
        par.chickStandard = false;
        system.assertEquals(false, par.chickStandard);
        
        par.tableStandard = false;
        system.assertEquals(false, par.tableStandard);
        
    }

}