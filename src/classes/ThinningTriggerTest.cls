@isTest
public class ThinningTriggerTest {
    	@testSetup
		static void insertFarm()
        {
            Account test = new Account(Name='test', Brojler__c=true);
    		insert test;
    		Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
       	 	insert testC;
            Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
    		insert farm;
            House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
            insert house;
            //dodaje jakas norme
            Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
            insert std;
            Date day = System.today();
            Flock__c flock = new Flock__c( House__c = house.id, 
                                          Birds_at_arrival__c  = 5000,
                                          Chick_weight_at_arrival__c = 40,
                                            Species__c='Broiler',
                                          Genetic_Line__c = 'Ross 308',
                                          Date_of_arrival__c = day);
            insert flock;
        }
    
    	@isTest
    	public static void checkNewThinnig()
        {
			System.debug('Test nowa ubiorka ');

            Flock__c flock = [Select id from Flock__c limit 1];
            Date day = System.today()-1;
			Thinning__c thg = new Thinning__c(Flock__c=flock.id,  Birds_thinned_at_farm__c = 1000, Date__c = day);
            try{
            	insert thg;
            }
            catch(Exception e)
            {
                system.debug('error ' + e.getMessage());

            }
            
            flock = [Select id, status__c, Current_number_of_birds__c from Flock__c limit 1];
            
            System.assertEquals('Active', flock.Status__c);
        }
    
        @isTest
    	public static void checkClosingThinnig()
        {
            System.debug('Test zamykania wstawienia ');
            Flock__c flock = [Select id from Flock__c limit 1];
            Date day = System.today()-1;
			Thinning__c thg = new Thinning__c(Flock__c=flock.id,  Birds_thinned_at_farm__c = 2500, Date__c = day);
            Thinning__c thg2 = new Thinning__c(Flock__c=flock.id,  Birds_thinned_at_farm__c = 2000, Date__c = day);
         	List<Thinning__c> thgList = new List<Thinning__c>();
            thgList.add(thg);
            thgList.add(thg2);
 
            try{
            	insert thgList;
                thg.Birds_thinned_at_farm__c = 2900;
                update thg;
                delete thg;
            }
            catch(Exception e)
            {
                system.debug('error ' + e.getMessage());
            }
            
            flock = [Select id, status__c, Current_number_of_birds__c from Flock__c limit 1];
            
            System.assertEquals('Inactive', flock.Status__c);
        }
    
}