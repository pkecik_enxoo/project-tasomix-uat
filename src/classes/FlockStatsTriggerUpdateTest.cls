@IsTest
public class FlockStatsTriggerUpdateTest {
	@testSetup
    public static void insertFarm()
    {
        Account test = new Account(Name='test', Brojler__c=true);
        insert test;
        Contact testC = new Contact(AccountId = test.id, LastName = 'testFlock');
        insert testC;
        Farm__c farm = new Farm__c(Manager__c = testC.id, Farm_Owner_Account__c = test.id, Postal_Code__c = '123-123');
        insert farm;
        House__c house = new House__c(Farm__c = farm.id, House_Name__c = 'Kurnik 1', House_area__c = 2000);
        insert house;
        //dodaje jakas norme
        Standard__c std = new Standard__c(Chick_weight_at_arrival__c  = 42, Genetic_Line__c ='Ross 308');
        insert std;
        Standard_Stats__c stdSt = new Standard_Stats__c(Standard__c = std.id, Day__c=2, Daily_water_consumption__c=33, 
                                                      Daily_feed_consumed__c=16, Avg_body_weight__c =50  );
        insert stdSt;
        Standard_Stats__c stdSt2 = new Standard_Stats__c(Standard__c = std.id, Day__c=3, Daily_water_consumption__c=33, 
                                                      Daily_feed_consumed__c=16, Avg_body_weight__c =50  );
        insert stdSt2;
        
        Standard_Stats__c stdSt3 = new Standard_Stats__c(Standard__c = std.id, Day__c=4, Daily_water_consumption__c=33, 
                                                      Daily_feed_consumed__c=16, Avg_body_weight__c =50  );
        insert stdSt3;
        
        Date day = System.today()-2;
        Flock__c flock = new Flock__c( House__c = house.id, 
                                      Birds_at_arrival__c  = 5000,
                                      Chick_weight_at_arrival__c = 40,
                                        Species__c='Broiler',
                                      Genetic_Line__c = 'Ross 308',
                                      Date_of_arrival__c = day);
        insert flock;
    }
    
    @isTest
    public static void checkNewStat()
    {
        System.debug('Test nowa statystyka');
        
        Flock__c flock = [Select id from Flock__c limit 1];
        Date day = System.today();
        
        Flock_stats__c flSt = new Flock_stats__c(Flock__c=flock.id, Date__c = day); 
        try{
            insert flSt;
        }
        catch(Exception e)
        {
            system.debug('error ' + e.getMessage());
            
        }
        
        Flock_stats__c flSt2 = new Flock_stats__c(Flock__c=flock.id, Date__c = date.today()+1,Daily_dead__c=10,Daily_selection__c=20); 
        try{
            insert flSt2;
        }
        catch(Exception e)
        {
            system.debug('error ' + e.getMessage());
            
        }
        
        system.debug('TEST: '+flst2.id);
        
        flSt = [Select id, Standard_Line_Id__c  from Flock_stats__c where id=:flSt.id ];
        Standard_Stats__c stdSt = [select id from Standard_Stats__c limit 1];
        System.assertEquals( stdSt.id, flSt.Standard_Line_Id__c );
        
        flSt2 = [Select id, Standard_Line_Id__c,Daily_dead__c  from Flock_stats__c where id=:flSt2.id];
        flSt2.Daily_dead__c = 50;
        update flSt2;
        flst.Daily_selection__c = 10;
        update flst;
    }
}