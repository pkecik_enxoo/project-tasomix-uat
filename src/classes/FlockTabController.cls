public with sharing class FlockTabController {

	@AuraEnabled
    public static Account[] getAccounts(){
        List<Account> options = ClientUtils.getAccount();
        return options;
    }
    
    // pobieranie listy ferm dla accounta
    @AuraEnabled
    public static Farm__c[] getFarms(String accId){
        List<Farm__c> options = ClientUtils.getFarms(accId);
        return options;
    }

    // pobieranie listy kurnikow dla wybranej fermy
    @AuraEnabled
    public static House__c[] getHouses(string farmId){
        List<House__c> options =  ClientUtils.getHouses(farmId);   
        return options;
    }
    
    // pobieram pickliste 
    @AuraEnabled
    public static String  getCodePicklist(String objName, String fieldName){
        Schema.DescribeFieldResult fieldResult = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap().get(fieldName).getDescribe(); 
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        return JSON.serialize(ple); 
    } 
    
	// pobieranie listy wstawien dla okreslonego kurnika    
    @AuraEnabled
    public static Flock__c[] getFlocks(String houseId, Boolean showActive, Boolean showClosed, String farmId, String orderByField,
            String orderByDirection, String hatcheryName, Boolean showRearing, Boolean showFattening, Boolean showBroiler){
        //sanitize user-supplied input
        if(!String.isBlank(orderByField)){
        	orderByField = String.escapeSingleQuotes(orderByField);
        }
        if(String.isBlank(orderByDirection) || (orderByDirection != 'DESC' && orderByDirection != 'ASC' ) ){
            orderByDirection = 'DESC';
        }
        if(orderByDirection == 'DESC'){
            orderByDirection = 'DESC NULLS last';
        }
        
        System.debug('Wyszukuje wstawienia dla kurnika: ' + houseId);
        List<Flock__c> options = new List<Flock__c>();
        if(houseId!= null && houseId != '')
        {
            List<String> lStatuses = new List<String>();
            List<String> types = new List<String>();
            List<String> species = new List<String>();
            
            if(showActive==true)
            	lStatuses.add('Active');
            
            if(showClosed==true)
            	lStatuses.add('Inactive');

            if(showRearing)
                types.add('Rearing');

            if(showFattening)
                types.add('Fattening');

            if(showBroiler)
                species.add('Broiler');
            
            List<Flock__c> flockList = null;
            String query = 'Select House__r.House_Name__c, House__r.Farm__r.Name, House__r.Farm__r.Farm_Owner_Account__r.Name, id, ' +
                    'Flock_number__c, Hatchery__c, Name, Last_Feed_Weight_Ratio_Standard__c, Date_of_arrival__c, Genetic_Line__c, ' +
                    'toLabel(Type__c), Standard_weight_for_avg_age__c, IsTerminated__c, Avg_age__c, Avg_body_weight__c, Total_feed_for_FCR__c,' +
                    'Feed_Left_in_Silo__c, Total_feed_consumed__c, FCR__c, EWW__c, Total_dead_percent__c, Total_dead__c from Flock__c '+
                'where Status__c in :lStatuses';

            if(!types.isEmpty())
                query += ' and Type__c in :types';

            if(!species.isEmpty())
                query += ' and Species__c in :species';

            String customSort = '';
            if(!String.isBlank(orderByField)) {
                customSort = orderByField+ ' '+orderByDirection+', ';
                System.debug('Sorting by '+customSort);
            }
            
            // zawęź wyszukiwanie o wylęgarnię
            if(String.isNotBlank(hatcheryName)){
            	String sanitizedHatcheryName = String.escapeSingleQuotes(hatcheryName);
            	query += ' and Hatchery__c = :sanitizedHatcheryName ';
            }
             
            if(houseId=='all' && farmId!=null && farmId!='') {
                /*
                flockList =[Select House__r.House_Name__c, id, Flock_number__c, Name, Date_of_arrival__c, Genetic_Line__c, Standard_weight_for_avg_age__c, Avg_age__c, Avg_body_weight__c, Total_feed_for_FCR__c, Total_feed_consumed__c, FCR__c, EWW__c, Total_dead_percent__c, Total_dead__c from Flock__c 
                            where House__r.Farm__c =:farmId and  Status__c in :lStatuses
                            order by House__r.Name ASC, Date_of_arrival__c  DESC];*/
            
                query += ' and House__r.Farm__c =:farmId order by '+ customSort +' House__r.Name ASC, Date_of_arrival__c  DESC';
        	} else {  
                /* flockList = [Select House__r.House_Name__c, id, Flock_number__c, Name, Date_of_arrival__c, Genetic_Line__c, Standard_weight_for_avg_age__c, Avg_age__c, Avg_body_weight__c, Total_feed_for_FCR__c, Total_feed_consumed__c, FCR__c, EWW__c, Total_dead_percent__c, Total_dead__c from Flock__c 
                             where House__c=:houseId and Status__c in :lStatuses
                             order by Date_of_arrival__c  DESC]; */
                query += ' and House__c =:houseId order by '+ customSort +'Date_of_arrival__c  DESC';
            }

            flockList = database.query(query);
            for(Flock__c flock : flockList)
            {
                if(flock.Flock_number__c != null)
                    options.add(flock);    	
            }
        }

        System.debug('XX flock ' + options);
        
        return options;
    }

}