/*
 	* @author Maria Raszka 2017-03-20
 	* Przypisywanie Flock statsom, Standard statsów na podstawie pola Standard
	* 0. Pobieram liste statsow z bazy i sprawdzam czy dla danego dnia sa inne wpisy - jesli sa to jest to blad
	* 1. jeśli jest insert albo Standard_assigned__c!= true
	* 2. Dla kazdego Flock stats wyszukuje odpowiedni Standard stats
	* 3. Jeśli nie znaleziono zgłaszam błąd
	* Przepisywanie na flock ostatniej (najbardziej aktualnej) wagi, normy wagowej i dnia pomiaru
	* 1. System pobiera floki i podany ostatnid zien pomiaru 
	* 2. Dla kazdego flok stata jesli dzien z floka jest mniejszy od dnia ze stata, flok jest aktualizowany
	* 3. Zapis flokow 
 	*/

trigger FlockStatsTrigger on Flock_stats__c (before insert, before update) {
    
    List<String> allFlockIdList = new List<String>();
    List<Double> dayList = new List<Double>();
    for(Flock_stats__c flStat : Trigger.new)
    {
        allFlockIdList.add(flStat.Flock__c);
        dayList.add(flStat.Day__c);
    }
    
    // pobieram liste flok statow z bazy 
    List<Flock_stats__c> allFlockList = [Select id, flock__c, date__c from Flock_stats__c where flock__c in : allFlockIdList];
  
    for(Flock_stats__c newFlStat : Trigger.new)
    {
        system.debug('FlockStatsTrigger - Sprawdzam czy wprowadzono juz dane dla dnia  ' +  newFlStat.date__c);
        // porownuje z lista z bazy 
        for(Flock_stats__c dbFlStat : allFlockList)
        {
            //system.debug('FlockStatsTrigger - porownuje daty ' + newFlStat.date__c + ' db: '+ dbFlStat.date__c);
            if(newFlStat.flock__c == dbFlStat.flock__c && newFlStat.date__c == dbFlStat.date__c)
            {
                if(Trigger.isInsert)            
                {
                    system.debug('Wprowadzono już wpis dla dnia ' + newFlStat.date__c );
                    newFlStat.addError('Wprowadzono już wpis dla dnia ' + newFlStat.date__c + ' nie można doać drugiego, zaktualizuj stary wpis.'); 
                }
                if(newFlStat.id != dbFlStat.id)
                {
                    system.debug('Wprowadzono już wpis dla dnia ' + newFlStat.date__c );
                    newFlStat.addError('Wprowadzono już wpis dla dnia ' + newFlStat.date__c + ' nie można doać drugiego, zaktualizuj stary wpis.'); 
                }
            }
        }  
    }
    
    List<Flock__c> flockList = [Select id, Standard__c, Last_weighting_day__c, Last_weight__c, Last_table_weight_std__c, Last_Feed_Weight_Ratio_Standard__c from Flock__c where id in : allFlockIdList];
    List<String> stdIdList = new List<String>();
    for(Flock__c fl : flockList)
    {
        stdIdList.add(fl.Standard__c);
    }
    system.debug('FlockStatsTrigger - Liczba norm ' + stdIdList.size());
    List<Standard_Stats__c> stdStatList = [Select Day__c, Standard__c, Avg_body_weight__c  from Standard_Stats__c where Standard__c in : stdIdList and day__c in : dayList] ;
    Map<Id, Flock__c> flToUpdate = new Map<Id, Flock__c>();   
    for(Flock_stats__c flStat : Trigger.new)
    {
        String stdId = null;
        Flock__c flock = null;
        // sprawdzam norme
        for(Flock__c fl: flockList)
        {
            if(flStat.Flock__c == fl.id)
            {
                stdId = fl.Standard__c;
                flock = fl;
                break;
            }
        }
        // 1. jeśli jest insert albo Standard_assigned__c!= true
        if(Trigger.isInsert || flStat.Standard_assigned__c == false)
        {
        	boolean stdFound = false;
           
            if(stdId == null)
            {
                flStat.addError('Norma nie jest porzypisana do Wstawienia : skontaktuj się z administratorem.');
            	continue;
            }   
            system.debug('FlockStatsTrigger - wyszukuje norme dla dnia ' + flStat.Day__c);
            //2. Dla kazdego Flock stats wyszukuje odpowiedni Standard stats
            for(Standard_Stats__c stdStat : stdStatList)
            {
                if(stdStat.Standard__c == stdId && flStat.Day__c == stdStat.Day__c)
                {
                    flStat.Standard_Line_Id__c = stdStat.id;
                    flStat.Standard_assigned__c = true;
                    stdFound = true;
                    break;
                }
            }
            
            if(!stdFound)
            {
                 system.debug('FlockStatsTrigger - nie odnaleziono normy dla dnia ' + flStat.Day__c);
                 flStat.addError('Nie odnaleziono Statystyk Normy dla dnia '+flStat.Day__c+': skontaktuj się z administratorem.');
            }
        }
        
        
        //* Przepisywanie na flock ostatniej (najbardziej aktualnej) wagi, normy wagowej i dnia pomiaru
        //* 1. System pobiera floki i podany ostatnid zien pomiaru 
        //* 2. Dla kazdego flok stata jesli dzien z floka jest mniejszy od dnia ze stata, flok jest aktualizowany
        //* 3. Zapis flokow 
        system.debug('FlockStatsTrigger - Sprawdzam warunki dla floka, dzien stata '+ flStat.Day__c);
        system.debug('FlockStatsTrigger '+ flStat.Day__c);
		if((flock.Last_weighting_day__c == null || flStat.Day__c>=flock.Last_weighting_day__c ) &&  flStat.Avg_body_weight__c>0 )
        {
            // wyszukuje standard
            Standard_Stats__c foundStdStat = null;
            for(Standard_Stats__c stdStat : stdStatList)
            {
                if(stdStat.Standard__c == stdId && flStat.Day__c == stdStat.Day__c)
                {
                    foundStdStat = stdStat;
                    break;
                }
            }
            if(foundStdStat!=null &&  foundStdStat.Avg_body_weight__c!=0)
                  flock.Last_table_weight_std__c =  100*flStat.Avg_body_weight__c / foundStdStat.Avg_body_weight__c;
            // przypisuję dane i dodaje floka do aktualizacji
           flock.Last_weight__c = flStat.Avg_body_weight__c;
           flock.Last_weighting_day__c = flStat.Day__c;
           flToUpdate.put(flock.Id, flock);
        }
        //Przepisz najnowsza woda/pasza na flock
        if(flStat.Feed_Weight_Ratio_Standard__c <> null){
        	flock.Last_Feed_Weight_Ratio_Standard__c = flStat.Feed_Weight_Ratio_Standard__c;
           	flToUpdate.put(flock.Id, flock);
        }
        
    }
    
    if(flToUpdate.size()>0)
    {
        Set<Id> flId = flToUpdate.keySet();
        List<Flock__c> flList = new List<Flock__c>();
        for(Id singleId : flId)
        {
            flList.add(flToUpdate.get(singleId));
        }
        update flList;
    }
}