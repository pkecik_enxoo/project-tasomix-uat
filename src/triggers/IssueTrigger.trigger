trigger IssueTrigger on Issue__c (before insert, before update) {
 
    if(trigger.isInsert || trigger.isUpdate){
        // link all new notes to open visit
        Visit__c openVisit = VisitHelper.getCurrentUserOngoingVisit();
        
        Set<id> accIds= new Set<id> ();
        Set<id> farmIds= new Set<id> ();
        Set<id> houseIds= new Set<id> ();
        Set<id> flockIds= new Set<id> ();
        for(Issue__c issue: trigger.new){             
            accIds.add(issue.Farm_Owner_Account__c  );
            farmIds.add(issue.Farm__c );
            houseIds.add(issue.House__c );
            flockIds.add(issue.Flock__c );
        }
        
        VisitHelper vH = new VisitHelper(accIds, farmIds, houseIds, flockIds );
        
        for(Issue__c issue :trigger.new){
            
            // fill lookup fields based on already filled lookups
            vH.fillEmptyFields(issue);
            
            if(openVisit <> null ){
                // if there is ongoing visit and visit on issue is blank link issue to visit
                if(issue.Visit__c == null){
                    issue.Visit__c = openVisit.id;
                }
                // if there is ongoing visit and customer on issue is blank link issue to customer from visit
                if(issue.Farm_Owner_Account__c  == null){
                    issue.Farm_Owner_Account__c  = openVisit.Farm_Owner_Account__c ;
                }
                // if there is ongoing visit and FARM on issue is blank link issue to FARM from visit
                if(issue.Farm__c == null){
                    issue.Farm__c = openVisit.Farm__c;
                }
                /*
                // if there is ongoing visit and House on issue is blank link issue to House from visit
                if(issue.House__c == null){
                    issue.House__c = openVisit. House__c;
                }
                // if there is ongoing visit and FLOCK on issue is blank link issue to FLOCK from visit
                if(issue.Flock__c == null){
                    issue.Flock__c = openVisit. Flock__c;
                }
				*/
            }
        }
    }
}