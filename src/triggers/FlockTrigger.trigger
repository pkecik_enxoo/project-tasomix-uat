/**
 	* @author Maria Raszka 2017-03-20
 	* Przypisywanie Wstawienia do Normy na podstawie rasy
	* Dla każdego wstawienia:
	* 1. jeśli jest insert albo zmieniono rasę 
	* 2. Wyszukuję aktywną (na dzień dzisiejszy) normę dla danej Rasy 
	* 3. Przypisuję normę do Wstawienia
	* 4. Jeśli norma nie zostanie znaleziona zgłaszam błąd
	* 5. Jeśli był to update a zmianie uległa norma i są dzienne statystyki, to aktualizuję ich przypisanie do normy
	* 6. Jeśli jest to insert nadaję nr wstawienia
	***  Wyszukuję ostatnie wstawienie z Kurnika, sprawdzam jego date, i nadaję koljeny nr 
 	*/

trigger FlockTrigger on Flock__c (before insert, before update) {
    
    // Lista pomocnicza dla nazwania wstawienia
    List<Flock__c> flockList = null;
    List<Flock__c> flockIsUpdateList = new List<Flock__c>();
    List<Id> standardIds = new List<Id>();
    List<Standard__c> standardList = new List<Standard__c>();
    Map<Flock__c,List<Standard_stats__c>> flockMap = new Map<Flock__c,List<Standard_stats__c>>();
    
    //List of Houses to set value of Spieces and Type based on value of this fields on Flock
    List<String> housesToSetSpecies = new List<String>();
    List<Flock__c> flocksToUpdateHouses = new List<Flock__c>();
    Map<Id, House__c> parentHouses = new Map<Id, House__c>();

    // Calculates week of removal 
    for(Flock__c flock : Trigger.new)
    {
        flock.Calendar_Week_of_removal__c = DateHelper.GetIsoWeekString(flock.Date_of_removal__c);
    }
    
    if(Trigger.isInsert)
    {
        List<String> houseList = new List<String>();
		for(Flock__c flock : Trigger.new)
        {
            flocksToUpdateHouses.add(flock);
            houseList.add(flock.House__c);
        }
        
        housesToSetSpecies.addAll(houseList);
        
        flockList = [Select House__c, Flock_number__c, Date_of_arrival__c from Flock__c where House__c in : houseList order by House__c, Date_of_arrival__c DESC];
    }
    List<Flock_Stats__c> fStsToUpdate = new List<Flock_Stats__c>();
    
    List<Standard__c> stList = [select name, Genetic_Line__c from Standard__c where status__c= 'Active'];
    for(Flock__c flock : Trigger.new)
    {

        //1.jeśli jest insert albo zmieniono rasę
        if( Trigger.isInsert || (Trigger.oldmap.get(flock.id).Genetic_Line__c != flock.Genetic_Line__c) )
        {	
            boolean stFound = false;
            //2. Wyszukuję aktywną (na dzień dzisiejszy) normę dla danej Rasy 
            for(Standard__c std : stList)
            {
                if(flock.Genetic_Line__c == std.Genetic_Line__c)
                {
                    //3. Przypisuję normę do Wstawienia
                    flock.Standard__c = std.id;
                    stFound = true;
                    break;
                }
            }    
            //4. Jeśli norma nie zostanie znaleziona zgłaszam błąd
            if(!stFound)
            {
                flock.addError('Brak normy dla rasy : flock.Genetic_Line__c, skontaktuj się z administratorem');
            }
        }
        
        // 5. Jesli jest to update i zmieniono normę i są codzienne statystyki, aktualizuję ich listę
 		if( Trigger.isUpdate && (Trigger.oldmap.get(flock.id).Standard__c != flock.Standard__c) )
        {
            // sytuacja wyjątkowa!!! norma nie powinna być zmieniana - pojedyncze przypadki
   			// Pobieram listę pomocniczą z lista statystyk dziennych 
   			List<Flock_stats__c> flStatList = [Select Standard_Line_Id__c, Standard_assigned__c, Flock__c, Flock__r.Standard__c from Flock_stats__c where flock__c =: flock.id ];
			for(Flock_stats__c flStat : flStatList )
            {
                flStat.Standard_assigned__c = false; 
                fStsToUpdate.add(flStat);
            }
        }
        
         
        //6. Jeśli jest to insert nadaję nr wstawienia
         if(Trigger.isInsert)
    	{
            String biggestNumber = '';
            for(Flock__c lastFlock: flockList)
            {
                if(lastFlock.House__c == flock.House__c && lastFlock.Date_of_arrival__c.Year() == flock.Date_of_arrival__c.Year())
                {
                    biggestNumber = lastFlock.Flock_number__c;
                    break;
                }
            } 
            String newNumber = '';
            Integer tmpIndex = 1;
            if(biggestNumber!='')
            {
                try
                {
                    String tmp = biggestNumber.split('-')[0];
                    tmpIndex = Integer.valueOf(tmp) +1;
                }
                catch(Exception e)
                {
                    System.debug('Error '+e.getMessage());
                }

            }  
            newNumber = tmpIndex +'-' + flock.Date_of_arrival__c.Year();
            flock.Flock_number__c = newNumber;
        }
    }

    // aktualizuje statystyki stada
    if(fStsToUpdate.size()>0){
        map <id, Flock_Stats__c> fStsToUpdateMap = new map <id, Flock_Stats__c>(fStsToUpdate); 
        FlockStatsHelper.forceFutureStandardMatch(fStsToUpdateMap.keySet());
    }
       
    
    
    if( Trigger.isUpdate )
        {
            // Generuję listę flocków u których zmieniła się wartość Avg_age__c
            for(Flock__c flockUpdate : Trigger.new)
            {
                
                if(Trigger.oldmap.get(flockUpdate.id).Avg_age__c != flockUpdate.Avg_age__c 
                   || Trigger.oldmap.get(flockUpdate.id).Avg_body_weight__c != flockUpdate.Avg_body_weight__c
                   || Trigger.oldmap.get(flockUpdate.id).Standard__c != flockUpdate.Standard__c)
                {
                    flockIsUpdateList.add(flockUpdate);
                    standardIds.add(flockUpdate.Standard__c);
                }
                
                //Check if species or type have been changed
                if(Trigger.oldmap.get(flockUpdate.id).Species__c != flockUpdate.Species__c 
                   || Trigger.oldmap.get(flockUpdate.id).Type__c != flockUpdate.Type__c ) {
                   
                       flocksToUpdateHouses.add(flockUpdate);
                       housesToSetSpecies.add(flockUpdate.House__c);
                }
            }

            //Sprawdzam czy rozmiar listy jest większy od 0 a następnie generuję mapę flocków i standard statów powiązanych ze standardem na flocku
            system.debug('Flock Trigger list size: '+flockIsUpdateList.size());
            if(flockIsUpdateList.size() > 0)
            {
                standardList = [Select id,(select id,Avg_body_weight__c,Day__c from Standard_stats__r) from Standard__c where id in: standardIds];

                for(FLock__c flockListItem : flockIsUpdateList)
                {
                    for(Standard__c standardListItem : standardList)
                    {
                        if(flockListItem.Standard__c == standardListItem.Id)
                        {
                            flockMap.put(flockListItem, standardListItem.Standard_stats__r);
                        }
                    }
                }

                //Iteruję po mapie oraz zaokrąglam średni wiek by określić dzień z jakim będę porównywał standard staty
                for(Flock__c flockToUpdate : flockMap.keySet())
                {

                    Long flockAgeDayLong = (flockToUpdate.Avg_age__c).round(System.RoundingMode.HALF_EVEN);
                    integer flockAgeDay = flockAgeDayLong.intValue();

                    for(Standard_stats__c standardStats : flockMap.get(flockToUpdate))
                    {
                        system.debug('Flock Trigger flock avg age day: '+flockAgeDay);
                        system.debug('Flock Trigger standardStats.Day__c: '+standardStats.Day__c);
                        flockToUpdate.Standard_weight_for_avg_age__c = null;
                        if( flockAgeDay == standardStats.Day__c && standardStats.Avg_body_weight__c > 0)
                        {
                            Decimal toRound = ((flockToUpdate.Avg_body_weight__c * 1000) / standardStats.Avg_body_weight__c)*100;    
							Decimal rounded = toRound.setScale(2);
                            flockToUpdate.Standard_weight_for_avg_age__c = rounded;
                            system.debug('Flock Trigger flockToUpdate.Avg_body_weight__c: '+flockToUpdate.Avg_body_weight__c);
                            system.debug('Flock Trigger standardStats.Avg_body_weight__c: '+standardStats.Avg_body_weight__c);
                            system.debug('Flock Trigger flock standard avg age number: '+flockToUpdate.Standard_weight_for_avg_age__c);
                            break;
                        }

                    }
                }
            }
        }
    
    //If there are any houses to update species or type we query houses to update and set Species__c and Type__c on house based on current active flock in that house
    if(!housesToSetSpecies.isEmpty()) {
        parentHouses = new Map<Id, House__c>([SELECT Id, Species__c, Type__c FROM House__c WHERE ID IN :housesToSetSpecies]);
        for(Flock__c flock : flocksToUpdateHouses) {
            parentHouses.get(flock.House__c).Species__c = flock.Species__c;
            parentHouses.get(flock.House__c).Type__c = flock.Type__c;
        }
        
        update parentHouses.values();
    }
   
}