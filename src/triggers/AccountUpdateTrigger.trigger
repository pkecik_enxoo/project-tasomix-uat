trigger AccountUpdateTrigger on Account (after update) {
if(Trigger.isUpdate)
    {
        List<Account> accountsChanged = new List<Account>();
        for(Account accChanged : Trigger.new)
        {
            if(Trigger.oldMap.get(accChanged.Id).OwnerId != Trigger.newMap.get(accChanged.Id).OwnerId)
            {
                accountsChanged.add(accChanged);
            }
        }
        
        List<Farm__c> farmList = new List<Farm__c>([select id from Farm__c where Farm_Owner_Account__c in: accountsChanged]);

        List<Farm__Share> sharesToDelete = [SELECT Id 
                                                FROM Farm__Share 
                                                WHERE ParentId IN :farmList
                                                AND RowCause = 'OwnerAccess__c'];
        if(!sharesToDelete.isEmpty()){
            Database.Delete(sharesToDelete, false);
        }



           // Create a new list of sharing objects for Job
        List<Farm__Share> farmShrs  = new List<Farm__Share>();
        
        // Declare variables for recruiting and hiring manager sharing
        Farm__Share accownerShr;

        List<Farm__c> farms = new List<Farm__c>([Select id,name,Farm_Owner_Account__r.OwnerId from Farm__c where id in: farmList]);
        for(Farm__c farm : farms){
            // Instantiate the sharing objects
            accownerShr = new Farm__Share();
            
            // Set the ID of record being shared
            accownerShr.ParentId = farm.Id;
 
            
            // Set the ID of user or group being granted access
            accownerShr.UserOrGroupId = farm.Farm_Owner_Account__r.OwnerId;
            system.debug('FarmTrigger owner id: '+farm.Farm_Owner_Account__r.OwnerId);
     
            // Set the access level
            accownerShr.AccessLevel = 'Read';

            
            // Set the Apex sharing reason for hiring manager and recruiter
            accownerShr.RowCause = Schema.Farm__Share.RowCause.OwnerAccess__c;
 
            
            // Add objects to list for insert
            farmShrs.add(accownerShr);

        }
        
        // Insert sharing records and capture save result 
        // The false parameter allows for partial processing if multiple records are passed 
        // into the operation 
        Database.SaveResult[] lsr = Database.insert(farmShrs,false);
        
        // Create counter
        Integer i=0;
        
        // Process the save results
        for(Database.SaveResult sr : lsr){
            if(!sr.isSuccess()){
                // Get the first save result error
                Database.Error err = sr.getErrors()[0];
                
                // Check if the error is related to a trivial access level
                // Access levels equal or more permissive than the object's default 
                // access level are not allowed. 
                // These sharing records are not required and thus an insert exception is 
                // acceptable. 
                if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION &&  err.getMessage().contains('AccessLevel'))){
                    // Throw an error when the error is not related to trivial access level.
                    trigger.newMap.get(farmShrs[i].ParentId).addError('Unable to grant sharing access due to following exception: ' + err.getMessage());
                }
            }
            i++;
        }   



    }
    
}