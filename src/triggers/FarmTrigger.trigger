/* @author Jarosław Kołodziejczyk 2017-04-04
 * 1. Klasa słuząca do sharowania apexowego wybranych ferm
 * 2. Jeśli user jest właścicielem konta okreslonego na pol Farm_Owner_Account to automatycznie ma dostęp do takiej fermy.
*/

trigger FarmTrigger on Farm__c (after insert) {
    
    if(trigger.isInsert){

        // Create a new list of sharing objects for Job
        List<Farm__Share> farmShrs  = new List<Farm__Share>();
        
        // Declare variables for recruiting and hiring manager sharing
        Farm__Share accownerShr;

        List<Farm__c> farms = new List<Farm__c>([Select id,name,Farm_Owner_Account__r.OwnerId from Farm__c where id in: Trigger.new]);
        for(Farm__c farm : farms){
            // Instantiate the sharing objects
            accownerShr = new Farm__Share();
            
            // Set the ID of record being shared
            accownerShr.ParentId = farm.Id;
 
            
            // Set the ID of user or group being granted access
            accownerShr.UserOrGroupId = farm.Farm_Owner_Account__r.OwnerId;
            system.debug('FarmTrigger owner id: '+farm.Farm_Owner_Account__r.OwnerId);
     
            // Set the access level
            accownerShr.AccessLevel = 'read';

            
            // Set the Apex sharing reason for hiring manager and recruiter
            accownerShr.RowCause = Schema.Farm__Share.RowCause.OwnerAccess__c;
 
            
            // Add objects to list for insert
            farmShrs.add(accownerShr);

        }
        
        // Insert sharing records and capture save result 
        // The false parameter allows for partial processing if multiple records are passed 
        // into the operation 
        Database.SaveResult[] lsr = Database.insert(farmShrs,false);
        
        // Create counter
        Integer i=0;
        
        // Process the save results
        for(Database.SaveResult sr : lsr){
            if(!sr.isSuccess()){
                // Get the first save result error
                Database.Error err = sr.getErrors()[0];
                
                // Check if the error is related to a trivial access level
                // Access levels equal or more permissive than the object's default 
                // access level are not allowed. 
                // These sharing records are not required and thus an insert exception is 
                // acceptable. 
                if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION &&  err.getMessage().contains('AccessLevel'))){
                    // Throw an error when the error is not related to trivial access level.
                    trigger.newMap.get(farmShrs[i].ParentId).addError('Unable to grant sharing access due to following exception: ' + err.getMessage());
                }
            }
            i++;
        }   
    }
    
}