trigger VisitAfterTrigger on Visit__c (after insert, after update) {
    
    // validate only one open visit
    if(! VisitHelper.maxOneOngoingVisitPerUser() ){
        trigger.new[0].addError('Dozwolona jest tylko jedna rozpoczęta wizyta. Proszę zakończyć poprzednią wizytę.');
    }
}