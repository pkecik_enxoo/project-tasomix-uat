trigger VisitTrigger on Visit__c (before insert, before update) {
// dopisywanie farmy do konta
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate) ){
        Set<id> accIds= new Set<id> ();
        Set<id> farmIds= new Set<id> ();
        Set<id> houseIds= new Set<id> ();
        Set<id> flockIds= new Set<id> ();
        
        for(Visit__c visit: trigger.new){
            accIds.add(visit.Farm_Owner_Account__c  );
            farmIds.add(visit.Farm__c );
            //houseIds.add(visit. House__c );
            //flockIds.add(visit. Flock__c );
        }
        VisitHelper vH = new VisitHelper(accIds, farmIds, houseIds, flockIds );
        
        for(Visit__c visit: trigger.new){            
            vH.fillEmptyFields(visit);
        }
    }

}