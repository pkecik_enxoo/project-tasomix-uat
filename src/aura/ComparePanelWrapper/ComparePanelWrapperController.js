({
	
    doInit: function(component,event,helper) {
        console.log("Compare panel initialization" ); 
        var paramList = [{accId:'', farmId:'', houseId:'', flockId:'', param: 'waga'}];
    
        // parametry prze
        var newSelectParamsList = component.get("v.newSelectParamsList");
        if(newSelectParamsList!= undefined && newSelectParamsList.length>0)
        {
            console.log("new list" + newSelectParamsList); 
            //console.log("param: " + newSelectParamsList[0].param);   
            paramList = JSON.parse(newSelectParamsList);
        }
        
		component.set("v.selectParamsList", paramList); 
    },
    
    doAdd: function(component,event,helper) {
    	//console.log("doAdd" );
        var ismax = component.get("v.isMax");
        //console.log("is max? "+ ismax );
        var paramList = component.get("v.selectParamsList");
        if(paramList.length<10)
        {	
        	// copy last param
        	if(paramList.length){
        		paramList.push(  JSON.parse(JSON.stringify( paramList[paramList.length-1] ) )   );
        	} else {
        		paramList.push({ accId:'', farmId:'', houseId:'', flockId:'', param: 'waga'});
        	}
            //console.log("List after add size:"+ paramList.length);
            if(paramList.length>=10)
			   component.set("v.isMax", true);             
            component.set("v.selectParamsList", paramList);
        }
	},
 
    doRemove: function(component,event,helper) {
    	//console.log("doRemove" );
        var paramList = component.get("v.selectParamsList");
        if(paramList.length>1)
        {
            //console.log("List before rem:"+ JSON.stringify(paramList));
            paramList.pop(); 
            //console.log("List after rem size:"+ paramList.length);
            if(paramList.length<10)
				component.set("v.isMax", false);             
            component.set("v.selectParamsList", paramList);
        }
	},

    doCompare: function(component,event,helper) {
    	console.log("doCompare" );
        var paramList = component.get("v.selectParamsList");
        //console.log("List to compare:"+ JSON.stringify(paramList));
        helper.getDataSeries(component);

        var cmpTarget = component.find('comparePanel');
        $A.util.removeClass(cmpTarget, 'slds-hide');
         var cmpTarget = component.find('comparePanelTable');
        $A.util.addClass(cmpTarget, 'slds-hide');


        var cmpTarget = component.find('hideData');
        $A.util.addClass(cmpTarget, 'slds-hide');
	},

    doShowTable: function(component,event,helper) {
        var cmpTarget = component.find('comparePanel');
        $A.util.addClass(cmpTarget, 'slds-hide');

        var Data = component.get("v.dataSeries");

        
        var paramList = component.get("v.selectParamsList");
        console.log("List to compare:"+ JSON.stringify(paramList));
        helper.getDataSeries(component);
        
         var cmpTarget = component.find('comparePanelTable');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        
    },

    doHideTable: function(component,event,helper) {
        //console.log("doCompare" );
        var cmpTarget = component.find('comparePanel');
        $A.util.removeClass(cmpTarget, 'slds-hide');
         var cmpTarget = component.find('comparePanelTable');
        $A.util.addClass(cmpTarget, 'slds-hide');


        var cmpTarget = component.find('hideData');
        $A.util.addClass(cmpTarget, 'slds-hide');
    }

})