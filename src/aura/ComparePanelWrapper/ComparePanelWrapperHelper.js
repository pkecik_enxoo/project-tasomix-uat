({    
    getDataSeries: function(component) {
        
        var action = component.get("c.getRemoteSeries");
        var paramList = component.get("v.selectParamsList");
        action.setParams({ 
                    "params" : JSON.stringify(paramList) 
                });
        action.setStorable({
            "ignoreExisting": "true"
        });
        action.setCallback(this, function(response) {
            var jsonData = response.getReturnValue();
            var data = JSON.parse(jsonData);
            var state = response.getState();
            var errors = action.getError();
            if (errors!= 0 && errors.length>0) {
                console.log("error " + errors[0].message);
            }
            if(data != null){
                //console.log("data format" + JSON.stringify(data));
                component.set("v.dataSeries", data);
            }           
        });
        $A.enqueueAction(action);
    },
})