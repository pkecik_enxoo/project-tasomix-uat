({
    panelInit: function(component,event,helper) {
    	
    	helper.doGetQfgView(component);
        var paramList = [{accId:'', farmId:'', houseId:'', flockId:''}];
        component.set("v.selectParams", paramList);
        helper.doSearchObjects(component, "c.getAccounts", null, null, "v.accList",  "v.accId");
        
        //pobieram liste dla kodow
        helper.doGetCodes(component, "c.getCodePicklist", "v.codeList");
        helper.doGetCodes(component, "c.getCodeDeliveredPicklist", "v.codeDeliveredList");
    },
    
	accIdChange: function(component, event, helper){
        //console.log("accIdChange" );     
        helper.doSearchObjects(component, "c.getFarms", "accId", "v.accId", "v.farmList",  "v.farmId"); 
    	// ustawiam wartosc zmiennej w obiekcie
        helper.doSetObj(component, "accId", "v.accId");
    },
    
    farmIdChange: function(component, event, helper){
        //console.log("farmIdChange" );
        helper.doSearchObjects(component, "c.getHouses", "farmId", "v.farmId", "v.houseList",  "v.houseId"); 
	    // ustawiam obiekt search
        helper.doSetObj(component, "farmId", "v.farmId");
    },
    
    houseIdChange: function(component, event, helper){
        //console.log("houseIdChange" );
        helper.doSearchObjects(component, "c.getFlocks", "houseId", "v.houseId", "v.flockList",  "v.flockId"); 
		// ustawiam obiekt search
        helper.doSetObj(component, "houseId", "v.houseId");
    },
    
    flockIdChange: function(component, event, helper){
        //console.log("flockIdChange" );
        var spinner = component.find('mySpinner');   
        $A.util.removeClass(spinner, "slds-hide");

        component.set("v.loadingData", "true");
        
        // ustawiam obiekt search
        helper.doSetObj(component, "flockId", "v.flockId");
    	helper.doSearchObjects(component, "c.getFlockStats", "flockId", "v.flockId", "v.flockStatsList");
	},

    // najpierw zapisuje dane pozniej je pobieram
	saveRecords: function(component, event, helper){
       
        var spinner = component.find('mySpinner');
        $A.util.removeClass(spinner, "slds-hide");
        component.set("v.loadingData", "true");
        
        var New = component.get("v.flockStatsList");
        //var Old = component.get("v.flsListOld");
        
        New = JSON.stringify(New);
        
        //console.log(New);
        
        var action = component.get("c.saveRecordsApex");
        
        action.setParams({ 
            'flNew' : New
        });
        action.setCallback(this, function(response) {
            var data = response.getReturnValue();
            var state = response.getState();
            var errors = action.getError();
            let message = "";
            let messageType = "success";
            if (state === "SUCCESS") {
	        	message = "Zapisano";
		    }
		    else if (state === "ERROR") {
		        if (errors && Array.isArray(errors) && errors.length > 0) {
		        	message = errors[0].message;
		        	messageType = "error";
		        	console.log("error " + message);
		        }
		    }
		    else {
		        message = "Wystąpił nieobsługiwany błąd";
		        messageType = "error";
		    }
			helper.showSomeToast('', message, messageType);
		    
            if (errors!= 0 && errors.length>0) {
                console.log("error " + errors[0].message);
            }
        });
        $A.enqueueAction(action);

        helper.doSearchObjects(component, "c.getFlockStats", "flockId", "v.flockId", "v.flockStatsList");   
	}
})