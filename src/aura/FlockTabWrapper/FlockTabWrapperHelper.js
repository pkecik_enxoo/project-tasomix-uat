({
	flipSortingOrder : function(cmp) {
		var order = cmp.get("v.orderByDirection");
        if(order == "ASC"){
            order = "DESC";
        } else {
            order = "ASC";
        }
        cmp.set("v.orderByDirection",order);
	}
})