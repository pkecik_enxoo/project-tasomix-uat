({
    // Metoda wyszukująca odpowiednie fermy,kurniki,flocki i flock staty, przy pomocy selectedValue(wybrana ferma) przekazywanego z aurowego page'a jako atrybut
    // następnie przekazuje wyszukane dane jako lista customowych obiektów
    selectFerm : function(component, event, helper)
    {
        var action = component.get("c.selectFarm");
        
        action.setStorable({
            "ignoreExisting": "true"
        });
        
        action.setParams({  
            "farmId" : component.get("v.selectedValue")
        })
        
        action.setCallback(this, function(response) 
        {
            var data = response.getReturnValue();
            
            //console.log("Data: " + data);
            
            var state = response.getState();
            var errors = response.getError();
            
            if (errors) 
            {
                if (errors[0] && errors[0].message) 
                {
                    console.log("Error message: " + errors[0].message);
                }
            } 
            else 
            {
                console.log("Unknown error");
            }
            
            //console.log("Failed with state: " + state );
            component.set("v.dataList", data);
        });
        $A.enqueueAction(action);
    },
    
})