({
    doInit: function(component,event,helper) {
        console.log("doInit" );
        helper.doGetHatchery(component);
        var paramList = [{accId:'', farmId:'', houseId:'', flockId:''}];
        component.set("v.selectParams", paramList);
        helper.doSearchObjects(component, "c.getAccounts", null, null, "v.accList",  "v.accId"); 
    },
    
	accIdChange: function(component, event, helper){
        console.log("accIdChange" );     
        helper.doSearchObjects(component, "c.getFarms", "accId", "v.accId", "v.farmList",  "v.farmId"); 
    	// ustawiam wartosc zmiennej w obiekcie
        helper.doSetObj(component, "accId", "v.accId");
    },
    
    farmIdChange: function(component, event, helper){
        console.log("farmIdChange" );
        helper.doGetHouses(component);
        //helper.doSearchObjects(component, "c.getHouses", "farmId", "v.farmId", "v.houseList",  "v.houseId"); 
	    // ustawiam obiekt search
        helper.doSetObj(component, "farmId", "v.farmId");
        // wywoluje pobranie flokow (bo pierwsze na liście jest all)
        helper.doGetFlocks(component);
    },
    
    houseIdChange: function(component, event, helper){
        console.log("houseIdChange" );
        //wyszuje wszystkie wstawienia wg podanych parametrów
        helper.doGetFlocks(component); 
		// ustawiam obiekt search
        helper.doSetObj(component, "houseId", "v.houseId");
    },
    
    showActiveChange: function(component, event, helper){
       // wyszukiwanie flokow aktywnych
       helper.doGetFlocks(component);
 	},
    
    hatcheryChange: function(component, event, helper){
       // wyszukiwanie flokow z wylęganią
       helper.doGetFlocks(component);
 	},

    showTypeChange: function(component, event, helper){
        helper.doGetFlocks(component);
    },

    showSpeciesChange: function(component, event, helper){
        helper.doGetFlocks(component);
    },
    
    getSortedFlocks: function(component, event, helper){
      // gets custom sorted flocks 
      var params = event.getParam('arguments');
        if (params) {
            var orderByField = params.orderByField;
            var orderByDirection = params.orderByDirection;
       		helper.doGetFlocks(component, orderByField, orderByDirection);
        }
    },
    
    createFlock : function (component, event, helper) {
        if(!component.get("v.houseId") || component.get("v.houseId") === 'all'){
            helper.showToast('warning', null , 'Nie wskazano kurnika');
            return;
        }
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Flock__c",
            "defaultFieldValues": {
                'House__c' : component.get("v.houseId")
            }
        });
        createRecordEvent.fire();
    },
    
    downloadTable : function(cmp, event, helper) {
        event.preventDefault();
		var accounts = cmp.get("v.accList");
        var accId = cmp.get("v.accId");
        var accName;
        for (var i=0, iLen=accounts.length; i<iLen; i++) {
            if (accounts[i].Id == accId) accName = accounts[i].Name;
        }
        var farms = cmp.get("v.farmList");
        var farmId = cmp.get("v.farmId");
        var farmName;
        for (var i=0, iLen=farms.length; i<iLen; i++) {
            if (farms[i].Id == farmId) farmName = farms[i].Name;
        }
        var houses = cmp.get("v.houseList");
        var houseId = cmp.get("v.houseId");
        var houseName;
        for (var i=0, iLen=houses.length; i<iLen; i++) {
            if (houses[i].Id == houseId) houseName = houses[i].House_Name__c;
        }
        
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('flock_tab_table');
        var table_html = encodeURI(table_div.outerHTML.replace(/↓/g,"").replace(/↑/g,"").replace(/&darr;/g,"").replace(/&uarr;/g,"").replace(/&nbsp; %/g,""));
        var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
        tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
        tab_text = tab_text + '<x:Name>Wstawienia</x:Name>';
        tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
        tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
        tab_text = tab_text + table_html;
        tab_text = tab_text + '</body></html>';

		var tab_name = 'Wstawienia ' + accName + '-'+ farmName + ' - Kurnik ' + houseName +  '.xls';
		        
        var a = document.createElement('a');
        a.href = data_type + ', ' + tab_text;
        a.download = tab_name;
        a.click();
    },
    
})