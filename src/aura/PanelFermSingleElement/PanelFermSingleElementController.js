({
	myAction : function(component, event, helper) {
		
	},
    
	getWeightParams : function(component,event,helper)
    {
        helper.navigateToCmpPanel(component, "waga");
    },
    
    backWeightBg : function(component,event,helper)
    {
  		component.set("v.weightStyle", '');
    },
    
    changeWeightBg : function(component,event,helper)
    {
    	component.set("v.weightStyle", 'background-color: rgb(247, 249, 251)');
    },

    getWaterConsumptionParams : function(component,event,helper)
    {
        helper.navigateToCmpPanel(component, "woda");
    },
    
    backWaterBg : function(component,event,helper)
    {
  		component.set("v.waterConsumptionStyle", '');
    },
    
    changeWaterBg : function(component,event,helper)
    {
    	component.set("v.waterConsumptionStyle", 'background-color: rgb(247, 249, 251)');
    },

    getFeedConsumptionParams : function(component,event,helper)
    {
		helper.navigateToCmpPanel(component, "pasza");
    },
    
    backFeedBg : function(component,event,helper)
    {
  		component.set("v.feedConsumptionStyle", '');
    },
    
    changeFeedBg : function(component,event,helper)
    {
    	component.set("v.feedConsumptionStyle", 'background-color: rgb(247, 249, 251)');
    },
    
    getDailyDeadParams : function(component,event,helper)
    {
        helper.navigateToCmpPanel(component, "upadki");
    },
    
    backDeadBg : function(component,event,helper)
    {
  		component.set("v.dailyDeadStyle", '');
    },
    
    changeDeadBg : function(component,event,helper)
    {
    	component.set("v.dailyDeadStyle", 'background-color: rgb(247, 249, 251)');
    },
    getWaterFeedParams : function(component,event,helper)
    {
		helper.navigateToCmpPanel(component, "woda_pasza");
    },
    
    backWaterFeedBg : function(component,event,helper)
    {
  		component.set("v.waterFeedStyle", '');
    },
    
    changeWaterFeedBg : function(component,event,helper)
    {
    	component.set("v.waterFeedStyle", 'background-color: rgb(247, 249, 251)');
    },
    navigateToHouse : function (component,event,helper) {
        helper.navigateToRecord(component.get("v.data.house.Id") );
    },
    navigateToFlock : function (component,event,helper) {
        helper.navigateToRecord(component.get("v.data.flock.Id") );
    }
})