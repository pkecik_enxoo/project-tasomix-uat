({
	navigateToCmpPanel : function(component, param) {
        var data = component.get("v.data");
        var accId = component.get("v.accId");
        var farmId = component.get("v.farmId");

        if(data.house!= undefined && data.flock!=undefined)
        {   
            var paramList = [{"param" : param, "accId" : accId, "farmId" : farmId, "houseId" : data.house.Id, "flockId" : data.flock.Id, "chickStandard": false, "tableStandard": true}];
            console.log('redirect with data '+ JSON.stringify(paramList));
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:ComparePanelWrapper",
                componentAttributes: {
                    "newSelectParamsList" : JSON.stringify(paramList),
                    "selectFirst": false
                }
            });
            evt.fire(); 
            
            var evt = $A.get("e.c:PanelFermNavigate");
            evt.setParams({ 
                "newSelectParamsList": JSON.stringify(paramList),
                "selectFirst": false
            });
            evt.fire();
            
            /*
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/porownywarka",
                "newSelectParamsList" : JSON.stringify(paramList),
                "selectFirst": false
            });
            urlEvent.fire();
             */
        }
    },
    navigateToRecord : function (objectId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": objectId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    }
})