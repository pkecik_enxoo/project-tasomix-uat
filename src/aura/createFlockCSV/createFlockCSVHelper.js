({
	gotoURL : function (component) {
        var flockId = component.get("v.recordId"); 
        var urlEvent = $A.get("e.force:navigateToURL");
        var urlLink = 'apex/createFlockCSV?recordId='+flockId;
        console.log(urlLink);
        
        urlEvent.setParams({
          "url": "/apex/createFlockCSV?recordId="+flockId
        });
        urlEvent.fire();
    }
})