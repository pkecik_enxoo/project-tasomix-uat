({
	rowInit : function(component, event, helper) {
		
        var data =  component.get("v.flockStatsRow");    
           
        if(!('DayHidden__c' in data))
            data["DayHidden__c"] = data.Day__c;

        component.set("v.flockStatsRow", data);    
        // ustawiam formularz
        // loadingData
        component.set("v.Day", data.DayHidden__c); 
        var d = new Date(data.Date__c);
        // format DD-MM-RR
        var s_date = d.toISOString().slice(8,10) + d.toISOString().slice(4,8) + d.toISOString().slice(2,4);
   		component.set("v.Date",s_date); 
     
        component.set("v.Daily_dead", ((data.Daily_dead__c == undefined || data.Daily_dead__c == null )? "" :  data.Daily_dead__c)); 
        component.set("v.Daily_selection", data.Daily_selection__c); 
        component.set("v.Daily_water_consumption", data.Daily_water_consumption__c); 
        component.set("v.Daily_feed_consumed", data.Daily_feed_consumed__c); 
        component.set("v.Avg_body_weight", data.Avg_body_weight__c); 
        component.set("v.Number_of_weighings", data.Number_of_weighings__c); 
        component.set("v.Feed_delivery_from_farm_to_house", data.Feed_delivery_from_farm_to_house__c); 
        component.set("v.Feed_code", data.Feed_code__c); 
        component.set("v.Feed_code_delivered", data.Feed_code_delivered__c); 
        component.set("v.Treatment", data.Treatment__c); 
        component.set("v.Formulations", data.Formulations__c); 
        component.set("v.Note", data.Note__c); 
        
    },
    
    dailyDeadChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        {
            helper.doSetObj(component, "Daily_dead");  
        }         
	},
    
    dailySelectionChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false"){
        	helper.doSetObj(component, "Daily_selection");
        }           
	},
    
    dailyFeedChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") {
        	helper.doSetObj(component, "Daily_feed_consumed"); 
        }         
	},
    dailyWaterChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        	helper.doSetObj(component, "Daily_water_consumption");          
	},

    bodyWeightChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        	helper.doSetObj(component, "Avg_body_weight");          
	},

    NumOfWeighChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        	helper.doSetObj(component, "Number_of_weighings");          
	},

    feedDeliveredChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        	helper.doSetObj(component, "Feed_delivery_from_farm_to_house");          
	},

    feedCodeChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        	helper.doSetObj(component, "Feed_code");          
	},
    
    feedCodeDeliveredChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        	helper.doSetObj(component, "Feed_code_delivered");          
	},
    

    treatmentChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        	helper.doSetObj(component, "Treatment");          
	},
    
    noteChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        	helper.doSetObj(component, "Note");          
	},
    
    setCode : function(component, event, helper)
    {
     	var selected = component.find("selectCode").get("v.value");
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        	component.set("v.Feed_code", selected);
	},
    
    setCodeDelivered : function(component, event, helper)
    {
     	var selected = component.find("selectCodeDelivered").get("v.value");
        var loadingData = component.get("v.loadingData");
        
        if(loadingData=="false") 
        	component.set("v.Feed_code_delivered", selected);
	},
    

    formulationsChange: function(component, event, helper)
    {
        var loadingData = component.get("v.loadingData");
        if(loadingData=="false") 
        	helper.doSetObj(component, "Formulations");          
	},
	
	
	
})