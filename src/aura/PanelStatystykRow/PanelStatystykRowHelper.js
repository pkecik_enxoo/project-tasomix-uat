({
    doSetObj : function (component, param_name)
    {
        var flockStatsRow = component.get("v.flockStatsRow");
        var tmpVal = component.get("v."+param_name);
        flockStatsRow[param_name+"__c"] = tmpVal;
        flockStatsRow["HasChanged__c"] = true;
        console.log("Zmiana "+param_name);
        component.set("v.flockStatsRow", flockStatsRow);
    }
})