({
	setChartFileName : function(component ){
    	let allSeries =  component.get("v.series");
        if(!allSeries[0] || !allSeries[0].label){
    		return;
    	}
		let dateNow = new Date();
		let dateNowString = dateNow.toISOString();
		dateNowString = dateNowString.replace("T", " ");
		dateNowString = dateNowString.substring(0, dateNowString.indexOf('.'));
		
    	let fileName = allSeries[0].label+' '+dateNowString+'.jpg';
    	component.set("v.chartFileName",fileName);
	
	},
	
	downloadChart : function(component, event ){
	
		var canvas = component.find("chart").getElement();

		var dt = canvas.toDataURL('image/jpeg');
		
		event.currentTarget.href = dt;
	
	},
	// color different graphs
	colorAllSeries : function(component, allSeries) {

		var colors = component.get("v.defaultColors");
		if(colors){
			colors = JSON.parse(JSON.stringify(colors));
		} else {
	         colors = [{ "bgc": "rgba(0, 0, 0,0.1)", "bdc": "rgba(0, 0, 0,1)"},
                      { "bgc": "rgba(138, 141, 142,0.1)", "bdc": "rgba(138, 141, 142,1)"},
                      { "bgc": "rgba(8, 127, 27,0.1)", "bdc": "rgba(58, 127, 27,1)"},
                      { "bgc": "rgba(127, 8, 70,0.1)", "bdc": "rgba(127, 8, 70,1)"},
            		  { "bgc": "rgba(255, 73, 251,0.1)", "bdc": "rgba(255, 73, 251,1)"},
            		  { "bgc": "rgba(249, 131, 29,0.1)", "bdc": "rgba(249, 131, 29,1)"},
              		  { "bgc": "rgba(39, 29, 249,0.1)", "bdc": "rgba(39, 29, 249,1)"},
            		  { "bgc": "rgba(172, 29, 249,0.1)", "bdc": "rgba(172, 29, 249,1)"},
                      { "bgc": "rgba(53, 224, 255,0.1)", "bdc": "rgba(53, 224, 255,1)"},
                      { "bgc": "rgba(43, 249, 29,0.1)", "bdc": "rgba(43, 249, 29,1)"}
                      ];
        }
       
        var colors_pisklak = [{ "bgc": "rgba(249,246,29,0.1)", "bdc": "rgba(249,246,29,1)"}];
        
        var colors_table = component.get("v.defaultColorsTable");
        if(colors_table){
        	colors_table = JSON.parse(JSON.stringify(colors_table));
        } else {
        	colors_table=[{ "bgc": "rgba(239,9,9,0.1)", "bdc": "rgba(239,9,9,1)"}];
        }
        
        
        
        for(var i=0; i<allSeries.length;i++) {
                    
	        var dataset = allSeries[i];
	        var label = allSeries[i].label;
	        
	        
	        
	        if(label.indexOf(' pisklak:')>-1)
	        {
	            if(colors_pisklak.length>0 && colors.length)
	            {
	            	dataset.backgroundColor = colors[colors.length-1].bgc;
	            	dataset.borderColor = colors[colors.length-1].bdc; 
	            	dataset.borderDash = [10,5];
	            	/*/
	            	dataset.backgroundColor = colors_pisklak[0].bgc;
	            	dataset.borderColor = colors_pisklak[0].bdc; 
	            	 */   
	            }
	            dataset.pointRadius  = 1;
	            dataset.pointBorderWidth = 1;                    
	        }
	        else if(label.indexOf(' tabela ')>-1)
	        {
	            if(colors_table.length>0)
	            {
	            	dataset.backgroundColor = colors_table[0].bgc;
	            	dataset.borderColor = colors_table[0].bdc;
	            }
	            dataset.pointRadius  = 1;
	            dataset.pointBorderWidth = 1;         
	        }
	        else if(colors.length>0)
	        {
	            //console.log('ustawiam kolor ' + colors[colors.length-1].bgc);
	            dataset.backgroundColor = colors[colors.length-1].bgc;
	            dataset.borderColor = colors[colors.length-1].bdc;
	            dataset.fill = false;
	        	colors.pop();
	        }
	        
	        var chartType = component.get("v.chartType");
			if(chartType==='bar'){
	           dataset.borderWidth  = 3;
	        }
	        dataset.pointBackgroundColor = "#fff";
	        dataset.pointHoverBorderWidth = 2;
	        
		}
		
		return allSeries;
	},
	
	changeYAxis : function(allSeries){
		    var yAxisID = [];
            for(var i=0; i<allSeries.length;i++)
            {
                if(allSeries[i].yAxisID!=undefined)
                    yAxisID.push(allSeries[i].yAxisID);
            }
            // ustawiam osie y
            var isRight = 0;
        	for(var i=0; i<window.myChart.options.scales.yAxes.length; i++)
            {	
                //console.log('zmieniam os y ' + window.myChart.options.scales.yAxes[i].id);
                window.myChart.options.scales.yAxes[i].display = false;
                
                for(var j=0;j<yAxisID.length;j++)
                {
                    if(yAxisID[j]==window.myChart.options.scales.yAxes[i].id &&  window.myChart.options.scales.yAxes[i].display==false )
                    {
                        window.myChart.options.scales.yAxes[i].display =true;
                        window.myChart.options.scales.yAxes[i].position = (isRight==1?'right':'left');
                        isRight=1-isRight;
                    }
                }
            }
	},
	
	drawChart : function(component ){
    	var allSeries =  component.get("v.series");
	
		
    	myChart.destroy();
        var dataOptions = this.getOptions(component);
	
  		var ctx = component.find("chart").getElement();  
        var allSeries =  component.get("v.series"); 
    	var labelsArray = [];
		for (let i = 0; i <= this.getLongestSeriesLength(allSeries) ; i++) {
		    labelsArray.push(i.toString());
		}
		var dataSets = this.parseAllSeries(component, allSeries);
		dataSets = this.colorAllSeries(component, allSeries);
		var chartType = component.get("v.chartType");
        
		window.myChart = new Chart(ctx, {
		    type: chartType,
		    data: {
		        labels: labelsArray,
		        datasets: dataSets
		    },
		    options: dataOptions
		});
		this.changeYAxis(allSeries);
		
		// force update
		window.myChart.update();  
	},
	
	getLongestSeriesLength : function( allSeries) {
		let maxLength = 0;
		for (let i = 0; i < allSeries.length; i++) {
			let dataSet = allSeries[i];
            if(!dataSet.data.length){continue;}
            
			let lastDay = dataSet.data[dataSet.data.length-1].x;
			if( lastDay > maxLength ){
				maxLength = lastDay; 
			}
		}
		return maxLength;
	},
	// modify data for bar graph i bar graf
	parseAllSeries : function(component, allSeries) {
		var chartType = component.get("v.chartType");
		if(chartType==='line'){
			return allSeries;
		} else if(chartType==='bar'){
			for (let i = 0; i < allSeries.length; i++) {
			
				let dataSet = allSeries[i];
            	if(!dataSet.data.length){continue;}
				//console.log(dataSet);
				//console.log('dataSet');
				let barDataArray = [];
				let lastElement = dataSet.data[dataSet.data.length-1].x;
				for (let j = 0; j < lastElement; j++) {
					barDataArray.push('-');
				}
				for (let j = 0; j < dataSet.data.length; j++) {
					barDataArray.splice(dataSet.data[j].x, 0, dataSet.data[j].y);
				}
				//delete barDataArray;
				allSeries[i].data = barDataArray;
				//console.log('barDataArray');
				//console.log(barDataArray);
			}
			return allSeries;
		}
	},

    getOptions: function(component) {

        var chartType = component.get("v.chartType");
        var xAxesSetup = [];
        if (chartType === 'barr') {
            
        } else if (chartType === 'line') {
        	xAxesSetup = [{
                        type: 'linear',
                        position: 'bottom',
                        scaleLabel: {
                            display: true,
                            labelString: 'Dzień chowu'
                        },
                        ticks: {
                            beginAtZero: true
                        }
                    }];
                          
        }
        
            return {
                chartArea: {
                    backgroundColor: 'rgba(236, 236, 236, 1)'
                },
                scales: {
                    xAxes: xAxesSetup,
                    yAxes: [{
                        "id": "y-waga",
                        "scaleLabel": {
                            "display": true,
                            "labelString": 'Waga [g]'
                        },
                        "ticks": {
                            "beginAtZero": true
                        }
                    }, {
                        "id": "y-woda",
                        "scaleLabel": {
                            "display": true,
                            "labelString": 'Spożycie wody [ml]'
                        },
                        "ticks": {
                            "beginAtZero": true
                        },
                        display: false
                    }, {
                        "id": "y-pasza",
                        "scaleLabel": {
                            "display": true,
                            "labelString": 'Spożycie paszy [g]'
                        },
                        "ticks": {
                            "beginAtZero": true
                        },
                        display: false
                    }, {
                        "id": "y-upadki",
                        "scaleLabel": {
                            "display": true,
                            "labelString": 'Upadki dzienne'
                        },
                        "ticks": {
                            "beginAtZero": true
                        },
                        display: false
                    }, {
                        "id": "y-woda_pasza",
                        "scaleLabel": {
                            "display": true,
                            "labelString": 'Woda/pasza'
                        },
                        "ticks": {
                            //"max": 4,
                            "beginAtZero": true
                        },
                        display: false
                    }]
                }
            }
        }
    

})