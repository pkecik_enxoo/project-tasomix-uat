({
    //Sprawdzam czy przekazany został id accounta a następnie wywołuję metodę Apexową wraz z przekazanym parametrem jako account
    //
    doSearchFarms : function(component, event, helper)
    {
        var nullChecker = component.get("v.accId");
        
        if(nullChecker != null)
        {
            var action = component.get("c.getFarms");
            
            action.setStorable({
                "ignoreExisting": "true"
            });
            
            action.setParams({ 
                "accId" : component.get("v.accId") 
            })
            
            action.setCallback(this, function(response) {
                var data = response.getReturnValue();
                var state = response.getState();
                var errors = action.getError();
                
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        cmp.set("v.message", errors[0].message);
                    }
                }
                
                if(data != null)
                {
                    //console.log("List size: " + JSON.stringify(data));
                    component.set("v.farmList", data);
                    //console.log("Id: " + JSON.stringify(data[0]));
                    helper.setSelectedValue(component,event,data[0].Id);
                }
            });
            $A.enqueueAction(action);
        } 
    },
    
    // Po wywołaniu panelu wywołuję metodę apexową pobierającą dostępne konta (które są przypisane do przynajmniej jednej fermy), 
    // a następnie przy pomocy helperowej metody wyszukuję dostępne fermy, oraz określam defaultowo wybrane wartości
    //
    doInit: function(cmp,event,helper) 
    {

        var spinner = cmp.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");

        console.log("INIT!!!" );
        var action = cmp.get("c.getAccount");
        
        action.setStorable({
            "ignoreExisting": "true"
        });
        
        action.setCallback(this, function(response) 
        {
            var acc = response.getReturnValue();
            
            if(acc.length){
                cmp.set("v.accList",acc);
                cmp.set("v.defaultAcc",acc[0].Id);
                //cmp.set("v.accId",acc[0].Id);
                cmp.find("selectacc").set("v.value", acc[0].Id);
                console.log("ACCID before init method: "+acc[0].Id);
                
                //helper.doSearchFarms(cmp,event, acc[0].Id);
            } else {
                cmp.set("v.accList",[{'sobjectType':'Account','Name':'-- Nie znaleziono kont z fermami --'}]);
            }
            
            $A.util.toggleClass(spinner, "slds-hide");
           // helper.doSearchFarms(cmp,event,acc[0].Id);
            // Set the attribute value.
        });
        $A.enqueueAction(action);
    },
   
})