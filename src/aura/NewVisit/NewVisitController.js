({
    doInit : function(component, event, helper) {
    	var getObjName = component.get("c.getObjectName");
        getObjName.setParams({
            "objId": component.get("v.recordId")
        });
        getObjName.setCallback(this, function(response) {
            var data = response.getReturnValue();
            var state = response.getState();
            var errors = getObjName.getError();
            if (errors!= 0 && errors.length>0) {
                console.log("error " + errors[0].message);
            }
            if(data != null){
            	component.set("v.objectType",data);
            	if(data === "Account"){
            		component.set("v.objectType",data);
            		helper.doSearchObjects(component, "c.getFarms", "accountId", "v.recordId", "v.farms", "v.selectedFarmId"); 
	        	} else if (data === "Farm__c"){
            		component.set("v.selectedFarmId",component.get("v.recordId") );
        			helper.doSearchObjects(component, "c.getHouses", "farmId", "v.recordId", "v.houses"); 
            	}
                
            }           
        });
        $A.enqueueAction(getObjName);
        
    },
    
    saveVisit : function(component, event, helper) {
        helper.saveVisit(component);
    },
    
    allHousesSelectedChange : function(component, event, helper) {
        let selected = component.get("v.allHousesSelected");
        if(selected){
        	helper.selectAllHouses(component, true) ;
        } else {
        	helper.selectAllHouses(component, false) ;
        }
    },
    
    houseSelected : function(component, event, helper) {
        let selected = component.get("v.allHousesSelected");
        component.set("v.allHousesSelected", false);
        
    },
    
    farmSelected : function(component, event, helper) {  
    	helper.doSearchObjects(component, "c.getHouses", "farmId", "v.selectedFarmId", "v.houses"); 
    },
})