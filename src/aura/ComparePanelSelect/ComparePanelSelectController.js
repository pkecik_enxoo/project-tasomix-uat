({
    doInit: function(component,event,helper) {
        //console.log("doInit" );
        var selectParams = component.get("v.selectParams") ;
        
        if(selectParams )
        {
//            component.set("v.accId", selectParams.accId );
//            component.set("v.farmId", selectParams.farmId );
//            component.set("v.houseId", selectParams.houseId );
//            component.set("v.flockId", selectParams.flockId );
//            component.set("v.param", selectParams.param );
            component.set("v.tableSelect", selectParams.tableStandard );
            component.set("v.chickSelect", selectParams.chickStandard );
        }
        
        helper.doSearchObjects(component, "c.getAccounts", null, null, "v.accList",  "v.accId"); 
        //console.log("selectParams "+ JSON.stringify(selectParams));
        //console.log("selectFirst "+  component.get("v.selectFirst"));
        //helper.doSetObj(component, "param", "v.param");
           
    },

	accIdChange: function(component, event, helper){   
        helper.doSearchObjects(component, "c.getFarms", "accId", "v.accId", "v.farmList",  "v.farmId"); 
    	// ustawiam wartosc zmiennej w obiekcie
        helper.doSetObj(component, "accId", "v.accId");
    },
    
    farmIdChange: function(component, event, helper){
        //console.log("farmIdChange" );
        helper.doSearchObjects(component, "c.getHouses", "farmId", "v.farmId", "v.houseList",  "v.houseId"); 
	    // ustawiam obiekt search
        helper.doSetObj(component, "farmId", "v.farmId");
    },
    
    houseIdChange: function(component, event, helper){
        //console.log("houseIdChange" );
        helper.doSearchObjects(component, "c.getFlocks", "houseId", "v.houseId", "v.flockList",  "v.flockId"); 
		// ustawiam obiekt search
        helper.doSetObj(component, "houseId", "v.houseId");
    },
    
    flockIdChange: function(component, event, helper){
        //console.log("flockIdChange" );
        //ustawiam obiekt search
    	helper.doSetObj(component, "flockId", "v.flockId");
	},
                        
	chickSelectChange: function(component, event, helper){
        //console.log("chickSelectChange" );
        //ustawiam obiekt search
    	helper.doSetObj(component, "chickStandard", "v.chickSelect");     
 	},

    tableSelectChange: function(component, event, helper){
        //console.log("tableSelectChange" );
        //ustawiam obiekt search
    	helper.doSetObj(component, "tableStandard", "v.tableSelect");     
 	},

    paramChange: function(component, event, helper){
        //console.log("paramChange" ); 
    	helper.doSetObj(component, "param", "v.param");         
    }
})